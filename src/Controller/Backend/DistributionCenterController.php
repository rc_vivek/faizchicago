<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * DistributionCenter Controller
 *
 * @property \App\Model\Table\DistributionCenterTable $DistributionCenter
 */
class DistributionCenterController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $distributionCenter = $this->paginate($this->DistributionCenter);

        $this->set(compact('distributionCenter'));
        $this->set('_serialize', ['distributionCenter']);
    }

    /**
     * View method
     *
     * @param string|null $id Distribution Center id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $distributionCenter = $this->DistributionCenter->get($id, [
            'contain' => []
        ]);

        $this->set('distributionCenter', $distributionCenter);
        $this->set('_serialize', ['distributionCenter']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $distributionCenter = $this->DistributionCenter->newEntity();
        if ($this->request->is('post')) {
            $distributionCenter = $this->DistributionCenter->patchEntity($distributionCenter, $this->request->data);
            if ($this->DistributionCenter->save($distributionCenter)) {
                $this->Flash->success(__('The distribution center has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The distribution center could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('distributionCenter'));
        $this->set('_serialize', ['distributionCenter']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Distribution Center id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $distributionCenter = $this->DistributionCenter->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $distributionCenter = $this->DistributionCenter->patchEntity($distributionCenter, $this->request->data);
            if ($this->DistributionCenter->save($distributionCenter)) {
                $this->Flash->success(__('The distribution center has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The distribution center could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('distributionCenter'));
        $this->set('_serialize', ['distributionCenter']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Distribution Center id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distributionCenter = $this->DistributionCenter->get($id);
        if ($this->DistributionCenter->delete($distributionCenter)) {
            $this->Flash->success(__('The distribution center has been deleted.'));
        } else {
            $this->Flash->error(__('The distribution center could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
