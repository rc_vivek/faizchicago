<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * DriverInfo Controller
 *
 * @property \App\Model\Table\DriverInfoTable $DriverInfo
 */
class DriverInfoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $driverInfo = $this->paginate($this->DriverInfo);

        $this->set(compact('driverInfo'));
        $this->set('_serialize', ['driverInfo']);
    }

    /**
     * View method
     *
     * @param string|null $id Driver Info id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $driverInfo = $this->DriverInfo->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('driverInfo', $driverInfo);
        $this->set('_serialize', ['driverInfo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driverInfo = $this->DriverInfo->newEntity();
        if ($this->request->is('post')) {
            $driverInfo = $this->DriverInfo->patchEntity($driverInfo, $this->request->data);
            if ($this->DriverInfo->save($driverInfo)) {
                $this->Flash->success(__('The driver info has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The driver info could not be saved. Please, try again.'));
            }
        }
        $users = $this->DriverInfo->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverInfo', 'users'));
        $this->set('_serialize', ['driverInfo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Driver Info id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $driverInfo = $this->DriverInfo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $driverInfo = $this->DriverInfo->patchEntity($driverInfo, $this->request->data);
            if ($this->DriverInfo->save($driverInfo)) {
                $this->Flash->success(__('The driver info has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The driver info could not be saved. Please, try again.'));
            }
        }
        $users = $this->DriverInfo->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverInfo', 'users'));
        $this->set('_serialize', ['driverInfo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Driver Info id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driverInfo = $this->DriverInfo->get($id);
        if ($this->DriverInfo->delete($driverInfo)) {
            $this->Flash->success(__('The driver info has been deleted.'));
        } else {
            $this->Flash->error(__('The driver info could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
