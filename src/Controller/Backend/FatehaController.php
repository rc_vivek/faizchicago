<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * Fateha Controller
 *
 * @property \App\Model\Table\FatehaTable $Fateha
 */
class FatehaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
       /* $this->paginate = [
            'contain' => ['Users']
        ];
        $fateha = $this->paginate($this->Fateha);

        $this->set(compact('fateha'));
        $this->set('_serialize', ['fateha']);*/
    	
    	$fatehaList = $this->Fateha->find('all', array('contain' => ['Users'], 'order' => ['Fateha.Id '=> 'DESC']));
    	$this->set('fatehaList', $fatehaList);
    }

    /**
     * View method
     *
     * @param string|null $id Fateha id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fateha = $this->Fateha->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('fateha', $fateha);
        $this->set('_serialize', ['fateha']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fateha = $this->Fateha->newEntity();
        if ($this->request->is('post')) {
            $fateha = $this->Fateha->patchEntity($fateha, $this->request->data);
            if ($this->Fateha->save($fateha)) {
                $this->Flash->success(__('The fateha has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fateha could not be saved. Please, try again.'));
            }
        }
        $users = $this->Fateha->Users->find('list', ['limit' => 200]);
        $this->set(compact('fateha', 'users'));
        $this->set('_serialize', ['fateha']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fateha id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fateha = $this->Fateha->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fateha = $this->Fateha->patchEntity($fateha, $this->request->data);
            if ($this->Fateha->save($fateha)) {
                $this->Flash->success(__('The fateha has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fateha could not be saved. Please, try again.'));
            }
        }
        $users = $this->Fateha->Users->find('list', ['limit' => 200]);
        $this->set(compact('fateha', 'users'));
        $this->set('_serialize', ['fateha']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fateha id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fateha = $this->Fateha->get($id);
        if ($this->Fateha->delete($fateha)) {
            $this->Flash->success(__('The fateha has been deleted.'));
        } else {
            $this->Flash->error(__('The fateha could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
