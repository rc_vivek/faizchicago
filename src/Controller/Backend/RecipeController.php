<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Recipe Controller
 *
 * @property \App\Model\Table\RecipeTable $Recipe
 */
class RecipeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        /*$recipe = $this->paginate($this->Recipe);

        $this->set(compact('recipe'));
        $this->set('_serialize', ['recipe']);*/
    	
    	$recipesList = $this->Recipe->find('all',  ['order' => ['Recipe.id' => 'DESC']]);
    	$this->set('recipesList', $recipesList);
    }

    /**
     * View method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recipe = $this->Recipe->get($id, [
            'contain' => []
        ]);

        $this->set('recipe', $recipe);
        $this->set('_serialize', ['recipe']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $recipe = $this->Recipe->newEntity();
        if ($this->request->is('post')) {
            $recipe = $this->Recipe->patchEntity($recipe, $this->request->data);
            if ($this->Recipe->save($recipe)) {
                $this->Flash->success(__('The recipe has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recipe could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('recipe'));
        $this->set('_serialize', ['recipe']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recipe = $this->Recipe->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $recipe = $this->Recipe->patchEntity($recipe, $this->request->data);
            if ($this->Recipe->save($recipe)) {
                $this->Flash->success(__('The recipe has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recipe could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('recipe'));
        $this->set('_serialize', ['recipe']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Recipe id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $recipe = $this->Recipe->get($id);
        if ($this->Recipe->delete($recipe)) {
            $this->Flash->success(__('The recipe has been deleted.'));
        } else {
            $this->Flash->error(__('The recipe could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function sendRecipe() {
    	$this->loadModel('Caterer');
    	if ($this->request->is('post')) {
    		$caterer  =  $this->request->data['caterer_id'];
    		$recipeIds  =  $this->request->data['recipe_name'];
    		
    		$caterer = $this->Caterer->find()
    		->select(['email_address'])
    		->where(['Caterer.id' => $caterer])->first();
    		
    		$recipes = $this->Recipe->find()
    		->select(['recipe_name', 'recipe_description'])
    		->where(['Recipe.id IN' => $recipeIds]);
    		
    		$msg = '<p>';
    	 	foreach ($recipes as $recipe) { 
    			 $msg .=  "<b> Recipe Name : </b>". $recipe->recipe_name."<br>";
    			 $msg.= "<b> Description : </b>".$recipe->recipe_description."<br><br>";
    		} 
    		$msg.= '</p>';
    		
    	 
    	    $catererEmailAddress = $caterer->email_address;
    	    $emailSub = 'Recipe Details';
    		if ($catererEmailAddress != '')
    		{
    			$email = new Email('default');
    			$email->from([EMAIL_SENDER => 'Faizchicago'])
    			->emailFormat('both')
    			->to($catererEmailAddress)
    			->subject($emailSub)
    			->send($msg);
    			
    			$this->Flash->success(__('Recipe Email has been sent successfully.'));
    			$this->redirect(['action' => 'sendRecipe']);
    		}
    		else {
    			$this->Flash->error(__('Caterer email address is missing. Please add email address first.'));
    		} 
    			
    	}
    	
    	$caterer = $this->Caterer->find('list', ['limit' => 200, 'order' => array('Caterer.name' => 'ASC') ]);
    	$recipe = $this->Recipe->find('list', ['limit' => 200, 'order' => array('Recipe.recipe_name' => 'ASC') ]);
    	$this->set('caterer', $caterer);
    	$this->set('recipe', $recipe);
    	
    }
}
