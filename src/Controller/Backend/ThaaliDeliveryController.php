<?php
namespace App\Controller\Backend;

use App\Controller\AppController;

/**
 * ThaaliDelivery Controller
 *
 * @property \App\Model\Table\ThaaliDeliveryTable $ThaaliDelivery
 */
class ThaaliDeliveryController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Thaali', 'DistributionCenter']
        ];
        $thaaliDelivery = $this->paginate($this->ThaaliDelivery);

        $this->set(compact('thaaliDelivery'));
        $this->set('_serialize', ['thaaliDelivery']);
    }

    /**
     * View method
     *
     * @param string|null $id Thaali Delivery id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $thaaliDelivery = $this->ThaaliDelivery->get($id, [
            'contain' => ['Users', 'Thaali', 'DistributionCenter']
        ]);

        $this->set('thaaliDelivery', $thaaliDelivery);
        $this->set('_serialize', ['thaaliDelivery']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $thaaliDelivery = $this->ThaaliDelivery->newEntity();
        if ($this->request->is('post')) {
            $thaaliDelivery = $this->ThaaliDelivery->patchEntity($thaaliDelivery, $this->request->data);
            if ($this->ThaaliDelivery->save($thaaliDelivery)) {
                $this->Flash->success(__('The thaali delivery has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali delivery could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliDelivery->Users->find('list', ['limit' => 200]);
        $thaali = $this->ThaaliDelivery->Thaali->find('list', ['limit' => 200]);
        $distributionCenter = $this->ThaaliDelivery->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('thaaliDelivery', 'users', 'thaali', 'distributionCenter'));
        $this->set('_serialize', ['thaaliDelivery']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Thaali Delivery id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $thaaliDelivery = $this->ThaaliDelivery->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $thaaliDelivery = $this->ThaaliDelivery->patchEntity($thaaliDelivery, $this->request->data);
            if ($this->ThaaliDelivery->save($thaaliDelivery)) {
                $this->Flash->success(__('The thaali delivery has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali delivery could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliDelivery->Users->find('list', ['limit' => 200]);
        $thaali = $this->ThaaliDelivery->Thaali->find('list', ['limit' => 200]);
        $distributionCenter = $this->ThaaliDelivery->DistributionCenter->find('list', ['limit' => 200]);
        $this->set(compact('thaaliDelivery', 'users', 'thaali', 'distributionCenter'));
        $this->set('_serialize', ['thaaliDelivery']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Thaali Delivery id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $thaaliDelivery = $this->ThaaliDelivery->get($id);
        if ($this->ThaaliDelivery->delete($thaaliDelivery)) {
            $this->Flash->success(__('The thaali delivery has been deleted.'));
        } else {
            $this->Flash->error(__('The thaali delivery could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
