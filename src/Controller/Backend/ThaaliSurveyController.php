<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
/**
 * ThaaliSurvey Controller
 *
 * @property \App\Model\Table\ThaaliSurveyTable $ThaaliSurvey */
class ThaaliSurveyController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
      /*  $this->paginate = [
            'contain' => ['Users', 'Thaalis']
        ];
        $thaaliSurvey = $this->paginate($this->ThaaliSurvey);

        $this->set(compact('thaaliSurvey'));
        $this->set('_serialize', ['thaaliSurvey']);*/
    	
    	$thaaliSurvey = $this->thaaliSurvey->find('all', array('contain' => ['Users','Thaalis']));
    	$this->set('users', $thaaliSurvey);
    }

    /**
     * View method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view()
    {
          $data = array();
		  $fromDate = date('Y-m-d') ;
          $toDate =  date('Y-m-d') ;
		 
		  if ($this->request->is('post')) {
			$fromDate = $this->request->data['dtfrom'];
			$toDate = $this->request->data['dtto'];
		  }
		 
		  $connection = ConnectionManager::get('default');
		  
		  /*$data = $connection->execute("select ts.rating as rating , ts.thaali_taste as taste,ts.thaali_qty  as qty,ts.created as created_date,
									  (select concat(first_name,' ',middle_name,' ',last_name) from users where id = ts.user_id) as user_name,
									  (select menu_item from thaali where id = ts.thaali_id ) as menu_item,
		  							  (select menu_date from thaali where id = ts.thaali_id ) as thaali_date 
									  from thaali_survey as ts WHERE date(ts.created) >= '".$fromDate."'and date(ts.created) <= '".$toDate."'")->fetchAll('assoc');
		  */
		  $data = $connection->execute("select ts.is_oil as oil , ts.thaali_taste as taste,ts.thaali_qty  as qty,ts.created as created_date,
									   concat(u.first_name,' ',u.middle_name,' ',u.last_name) as user_name, t.menu_item, t.menu_date as thaali_date
		  		                       FROM thaali_survey as ts LEFT JOIN
		  		                       thaali t 
		  		                       ON ts.thaali_id = t.id LEFT JOIN
		  		                       users u
		  							   ON ts.user_id = u.id  
		  		                       WHERE date(t.menu_date) >= '".$fromDate."'and date(t.menu_date) <= '".$toDate."'")->fetchAll('assoc');
		  
		  
		 $this->set('survey', $data);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
   /*  public function add()
    {
        $thaaliSurvey = $this->ThaaliSurvey->newEntity();
        if ($this->request->is('post')) {
            $thaaliSurvey = $this->ThaaliSurvey->patchEntity($thaaliSurvey, $this->request->data);
            if ($this->ThaaliSurvey->save($thaaliSurvey)) {
                $this->Flash->success(__('The thaali survey has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali survey could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliSurvey->Users->find('list', ['limit' => 200]);
        $thaalis = $this->ThaaliSurvey->Thaalis->find('list', ['limit' => 200]);
        $this->set(compact('thaaliSurvey', 'users', 'thaalis'));
        $this->set('_serialize', ['thaaliSurvey']);
    } */

    /**
     * Edit method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /* public function edit($id = null)
    {
        $thaaliSurvey = $this->ThaaliSurvey->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $thaaliSurvey = $this->ThaaliSurvey->patchEntity($thaaliSurvey, $this->request->data);
            if ($this->ThaaliSurvey->save($thaaliSurvey)) {
                $this->Flash->success(__('The thaali survey has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The thaali survey could not be saved. Please, try again.'));
            }
        }
        $users = $this->ThaaliSurvey->Users->find('list', ['limit' => 200]);
        $thaalis = $this->ThaaliSurvey->Thaalis->find('list', ['limit' => 200]);
        $this->set(compact('thaaliSurvey', 'users', 'thaalis'));
        $this->set('_serialize', ['thaaliSurvey']);
    } */

    /**
     * Delete method
     *
     * @param string|null $id Thaali Survey id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
   /*  public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $thaaliSurvey = $this->ThaaliSurvey->get($id);
        if ($this->ThaaliSurvey->delete($thaaliSurvey)) {
            $this->Flash->success(__('The thaali survey has been deleted.'));
        } else {
            $this->Flash->error(__('The thaali survey could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    } */
}
