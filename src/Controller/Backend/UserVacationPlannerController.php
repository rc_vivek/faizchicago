<?php
namespace App\Controller\Backend;

use App\Controller\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;
/**
 * UserVacationPlanner Controller
 *
 * @property \App\Model\Table\UserVacationPlannerTable $UserVacationPlanner
 */
class UserVacationPlannerController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($userId = null)
    {
       /* $this->paginate = [
            'contain' => ['Users']
        ];
        $userVacationPlanner = $this->paginate($this->UserVacationPlanner);

        $this->set(compact('userVacationPlanner'));
        $this->set('_serialize', ['userVacationPlanner']);*/
    	$reason = array('Out of town' => 'Out of town', 'Going for vacation' => 'Going for vacation', 'Other' => 'Other');
    	$this->set('reason', $reason);
    	$this->set('userId', $userId);
    	$userVacationPlanner = $this->UserVacationPlanner->newEntity();
    	$this->loadModel('ThaaliDelivery');
    	//$noDays = USER_THAALI_UPDATE_DAYS;
    	$noDays = 1;
    	$now = Time::now();
    	$min = $now->modify("+$noDays days")->i18nFormat('MM/dd/yyyy');
    	$this->set('user_id', $userId);
    	$this->set('min', $min);
    	if ($this->request->is('post')) {
    		$startDate = $this->request->data['start_date'];
    		$endDate = $this->request->data['end_date'];
    	
    		$vacationCount = $this->UserVacationPlanner->find()
    		->where(['user_id' =>  $userId , 'OR' => [['start_date <=' => $startDate, 'start_date >=' => $endDate], ['end_date >=' => $startDate, 'end_date <=' => $endDate]]])->count();
    	
    		//Checking data existance in vacation table
    		if ($vacationCount == 0 ) {
    			$now = Time::now();
    			$minDate = $now->modify("+$noDays days")->i18nFormat('yyyy-MM-dd');
    	
    			if ($minDate <= $startDate ) {
    				$dates = array();
    				$current = strtotime($startDate);
    				$last = strtotime($endDate);
    				 
    				while( $current <= $last ) {
    	
    					// foreach($dates as $vdate) {
    	
    					$userVacationPlanner = $this->UserVacationPlanner->newEntity();
    					$userVacationPlanner->user_id = $this->request->data['user_id'];
    					$userVacationPlanner->title = $this->request->data['title'];
    					$userVacationPlanner->driver_notes = $this->request->data['driver_notes'];
    					$userVacationPlanner->start_date = $current ;
    					$userVacationPlanner->end_date = $current;
    					 
    					if ($this->UserVacationPlanner->save($userVacationPlanner)) {
    						//Update User Thaali order status
    						$query = $this->ThaaliDelivery->query();
    						$query->update()
    						->set(['order_status' => '3'])
    						->where(['delivery_date' =>  $current,   'user_id' => $userId, 'order_status !=' => '1' ])
    						->execute();
    						//->where(['delivery_date >=' =>  $startDate, 'delivery_date <=' =>  $endDate,  'user_id' => $userId ])
    						// End of Update User Thaali order status
    						$dates[] = date('Y-m-d', $current);
    						$current = strtotime('+1 day', $current);
    					}else $this->Flash->error(__('There is the problem while adding vacation details.'));
    				}
    				 
    				$this->Flash->success(__('Vacation planner has been added succesfully.'));
    				return $this->redirect(['controller' => 'user_vacation_planner', 'action' => 'index', $userId]);
    				/*} else {
    				 $this->Flash->error(__('Adding/modifing vacation information will be considered valid only if the event is scheduled for after '. $min));
    				}*/
    			}
    			else {
    				$this->Flash->error(__("The minimum date should be" .$minDate."'"));
    			}
    		}
    		else {
    			$this->Flash->error(__('The selected date is already added in user Vacation Planner list.'));
    		}
    			
    	}
    	
    /*	$users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
    	$this->set(compact('userVacationPlanner', 'users'));
    	$this->set('_serialize', ['userVacationPlanner']);
    	*/
    	//Getting vacation List
    	
    	$userVacationPlannerList = $this->UserVacationPlanner->find('all', [
    			'contain' => ['Users'],
    			'conditions' => ['user_id  ' => $userId]
    	]);
    	
    	$this->set('userVacationPlannerList', $userVacationPlannerList);
    	$this->set('_serialize', ['userVacationPlannerList']);
    	
    	 
    }

    /**
     * View method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userVacationPlanner = $this->UserVacationPlanner->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userVacationPlanner', $userVacationPlanner);
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    
    
   public function add($userId = null)
    {
    	$reason = array('Out of town' => 'Out of town', 'Going for vacation' => 'Going for vacation', 'Other' => 'Other');
    	$this->set('reason', $reason);
    	$this->set('userId', $userId);
        $userVacationPlanner = $this->UserVacationPlanner->newEntity();
        $this->loadModel('ThaaliDelivery');
        //$noDays = USER_THAALI_UPDATE_DAYS;
        $noDays = 1;
        $now = Time::now();
        $min = $now->modify("+$noDays days")->i18nFormat('MM/dd/yyyy');
        $this->set('user_id', $userId);
        $this->set('min', $min);
        if ($this->request->is('post')) {
        	 $startDate = $this->request->data['start_date'];
        	 $endDate = $this->request->data['end_date'];
    		
    		$vacationCount = $this->UserVacationPlanner->find()
    		->where(['user_id' =>  $userId , 'OR' => [['start_date <=' => $startDate, 'start_date >=' => $endDate], ['end_date >=' => $startDate, 'end_date <=' => $endDate]]])->count();
    		
    		//Checking data existance in vacation table 
    		if ($vacationCount == 0 ) {
    			$now = Time::now();
        		$minDate = $now->modify("+$noDays days")->i18nFormat('yyyy-MM-dd'); 
	        	
		        	if ($minDate <= $startDate ) {
		        		$dates = array();
		        		$current = strtotime($startDate);
		        		$last = strtotime($endDate);
		        		 
		        		while( $current <= $last ) {
		        			 
		        		 // foreach($dates as $vdate) {
		        				
		        			$userVacationPlanner = $this->UserVacationPlanner->newEntity();
		        			$userVacationPlanner->user_id = $this->request->data['user_id'];
		        			$userVacationPlanner->title = $this->request->data['title'];
		        			$userVacationPlanner->driver_notes = $this->request->data['driver_notes'];
		        			$userVacationPlanner->start_date = $current ;
		        			$userVacationPlanner->end_date = $current;
		         
				            if ($this->UserVacationPlanner->save($userVacationPlanner)) {
				                //Update User Thaali order status 
					       		$query = $this->ThaaliDelivery->query();
					       		$query->update()
					       		->set(['order_status' => '3'])
					       		->where(['delivery_date' =>  $current,   'user_id' => $userId, 'order_status !=' => '1' ])
					       		->execute();
					       		//->where(['delivery_date >=' =>  $startDate, 'delivery_date <=' =>  $endDate,  'user_id' => $userId ])
					       		// End of Update User Thaali order status
					       		$dates[] = date('Y-m-d', $current);
					       		$current = strtotime('+1 day', $current);
				            }else $this->Flash->error(__('There is the problem while adding vacation details.'));
	            		 }  
	            		
	            		$this->Flash->success(__('Vacation planner has been added succesfully.')); 
     					return $this->redirect(['controller' => 'user_vacation_planner', 'action' => 'add', $userId]);
		        	/*} else {
			        	 	$this->Flash->error(__('Adding/modifing vacation information will be considered valid only if the event is scheduled for after '. $min));
			            }*/
    		}
    		else { 
    			$this->Flash->error(__("The minimum date should be" .$minDate."'")); 
    			}
    		}
    			else {
    				$this->Flash->error(__('The selected date is already added in user Vacation Planner list.'));
    			} 
    		   
        }
        
        $users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
        $this->set(compact('userVacationPlanner', 'users'));
        $this->set('_serialize', ['userVacationPlanner']);
        
        //Getting vacation List
        
        $userVacationPlannerList = $this->UserVacationPlanner->find('all', [
        		'contain' => ['Users'],
        		'conditions' => ['user_id  ' => $userId]
        ]);
        
        $this->set('userVacationPlannerList', $userVacationPlannerList);
        $this->set('_serialize', ['userVacationPlannerList']);
        
         
    }
 
    /**
     * Edit method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userVacationPlanner = $this->UserVacationPlanner->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userVacationPlanner = $this->UserVacationPlanner->patchEntity($userVacationPlanner, $this->request->data);
            if ($this->UserVacationPlanner->save($userVacationPlanner)) {
                $this->Flash->success(__('The user vacation planner has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user vacation planner could not be saved. Please, try again.'));
            }
        }
        $users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
        $this->set(compact('userVacationPlanner', 'users'));
        $this->set('_serialize', ['userVacationPlanner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Vacation Planner id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $userId)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userVacationPlanner = $this->UserVacationPlanner->get($id);
        
        $userId     = $userVacationPlanner->user_id;
        $thaaliDate = $userVacationPlanner->start_date;
        $this->loadModel('ThaaliDelivery');
        
        $query = $this->ThaaliDelivery->query();
    						$query->update()
    						->set(['order_status' => '0'])
    						->where(['delivery_date' =>  $thaaliDate,   'user_id' => $userId, 'order_status !=' => '1' ])
    						->execute();
        
        if ($this->UserVacationPlanner->delete($userVacationPlanner)) {
            $this->Flash->success(__('The user vacation planner has been deleted.'));
        } else {
            $this->Flash->error(__('The user vacation planner could not be deleted. Please, try again.'));
        }

       
       return $this->redirect(['controller' => 'user_vacation_planner', 'action' => 'index', $userId]);
    }
}
