<?php
namespace App\Controller\Customer;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use PhpParser\Node\Stmt\Else_;
use App\Model\Entity\UserThaaliInfo;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\View\Helper\FormHelper;
use Cake\Mailer\Email;
use Twilio\autoload;
use Twilio\Rest\Client;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	var $role = array('Role');
	
	
    public function beforeFilter(Event $event) {
    	$this->Auth->allow(['registration', 'forgotPassword', 'getThaaliMenu']);
    	$this->viewBuilder()->layout('customer');
    }

    public function index()	{
       return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    }
    
    public function login()	{
    	$this->viewBuilder()->layout('customer-login');
    	$user = $this->Auth->identify();
    	
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	
    	if($id > 0) {
    		return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    	}
    	//For get this week

    	//windows
    	$today = date("Y-m-d");
        $number = date('N', strtotime($today));
    	
    	if ($number != 6 ) { 
    	$weekStart = date('y-m-d', strtotime("monday this week"));
    	$weekEnd = date('y-m-d', strtotime("friday this week"));
    	
    	$start = date('m/d/y', strtotime("monday this week"));
    	$end = date('m/d/y', strtotime("friday this week"));
    	
    	$nextWeek = date('y-m-d', strtotime("monday next week"));
    	$prevWeek = date('y-m-d', strtotime("monday last week"));
    	
    	}
    	else {
    		$weekStart = date('y-m-d', strtotime("monday next week"));
    		$weekEnd = date('y-m-d', strtotime("friday next week"));
    		 
    		$start = date('m/d/y', strtotime("monday next week"));
    		$end = date('m/d/y', strtotime("friday next week"));
    		 
    		$nextWeek = strtotime($today);
    		$nextWeek =  strtotime('+9 day', $nextWeek);
    		$nextWeek = date('y-m-d', $nextWeek);
    		
    		$prevWeek = strtotime($today);
    		$prevWeek = strtotime('-5 day', $prevWeek);
    		$prevWeek = date('y-m-d', $prevWeek);
    		 
    	}
    	//linux
    	
    	/*$weekstart = date('m/d/Y', strtotime("sunday this week"));
    	$weekend = date('m/d/Y', strtotime("next saturday"));
    	*/
    	
    	$this->set('weekstart', $weekStart);
    	$this->set('weekend', $weekEnd);
    	$this->set('nextWeek', $nextWeek);
    	$this->set('prevWeek', $prevWeek);
    	$this->set('start', $start);
    	$this->set('end', $end);
    	
    	
    	$thisWeekCalendar = $this->__getThisWeekCalandar($weekStart, $weekEnd);
    	$this->set('thisWeekCalendar', $thisWeekCalendar);
    	if ($this->request->is('post')) {
    		$user = $this->Auth->identify();
    		//if ($user['user_role'] > '2') {
    			if ($user['status'] == '1'){
    				$this->Auth->setUser($user);
    				return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    			}
    			else if ($user['status'] == '0')
    				$this->Flash->error(__('Your account is not activated, try again'));
    			else if ($user['status'] == '2')
    				$this->Flash->error(__('Your account has been disabled'));
    			else
    				$this->Flash->error(__('Invalid username or password, try again'));
    		/*} else {
    			$this->Flash->error(__('Invalid username or password, try again'));
    		}*/
    	}
    }
    
    //Customer LogOut
    public function logout() {
    	$this->Flash->success('You are logged out');
    	return $this->redirect($this->Auth->logout());
    }
    
    //Customer Change Password
    public function changePassword() {
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	$user = $this->Users->get($id, [
    			'contain' => []
    	]);
    	 
    	if ($this->request->is(['patch', 'post', 'put'])) {
    		$user = $this->Users->patchEntity($user, $this->request->data);
    		if ($this->Users->save($user)) {
    			$this->Flash->success(__('Your password has been successfully updated !.'));
    			return $this->redirect(['action' => 'changePassword']);
    		}
    		else {
    			$this->Flash->error(__('Password user could not be saved. Please, try again !.'));
    		}
    	}
    	 
    	$this->set(compact('user'));
    	$this->set('_serialize', ['user']);
    }
    
    public function __resetPassword ($userId) {
    
    	if (empty($userId)) {
    		return null;
    	}
    	// Generate a random string 100 chars in length.
    	$hash = $this->__rand_string( 5 );
    	$user = $this->Users->get($userId);
    
    	$user->password = $hash;
    	$user->modifiedated_at     = date('Y-m-d H:i:s');
    	$to	=  $user->email_address;
    	if ($this->Users->save($user)) {
    		$msg = 'Salaam, <br/><br/> We have reset your password as per your request. Your new password is <b>'.$hash.'</b><br/><br/>Shukran';
    		$sub = 'Faizchicago - Password Reset';
    		$email = new Email('default');
    		$email->from([EMAIL_SENDER => 'Faizchicago'])
    		->emailFormat('both')
    		->to($to)
    		->subject($sub)
    		->send($msg);
    
    
    		 $this->Flash->success(__('Your password has been reset and new password sent to your registered email address.'));
    		//return $this->redirect(['action' => 'index']);
    	}
    	else {
    		$this->Flash->error(__('Password could not be disable. Please, try again.'));
    	}
    }
    
    public function forgotPassword() {
    	$this->viewBuilder()->layout('customer-login');
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	 
    	if($id > 0) {
    		return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    	}
    	if ($this->request->is('post')) {
    		$ejamaat_id = $this->request->data('ejamaat_id');
    		$email = $this->request->data('email_address');
    		$userCount = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->count();
    		 
    		if ($userCount == 1) {
    			$userDetails = $this->Users->find()->where(['ejamaatid ' => $ejamaat_id, 'email_address' => $email])->first();
    			$userStatus = $userDetails->status;
    			switch ($userStatus)	{
    
    				case '0' :
    					$this->Flash->error(__('Your account is not activated, try again'));
    					break;
    				case '1' :
    					$this->__resetPassword($userDetails->id);
    					break;
    				case '2' :
    					$this->Flash->error(__('Your account has been disabled'));
    					break;
    				case '3' :
    					$this->Flash->error(__('Your account has been Trashed'));
    					break;
    				case '4' :
    					$this->Flash->error(__('Your account has been Trashed'));
    					break;
    			}
    		}
    		else
    			$this->Flash->error(__('Invalid Ejamaat Id or Email address, try again'));
    		 
    	}
    }
    
    function __rand_string( $length ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$str ='';
    	$size = strlen( $chars );
    	for( $i = 0; $i < $length; $i++ ) {
    		$str .= $chars[ rand( 0, $size - 1 ) ];
    	}
    
    	return $str;
    }
     
    public function registration()	{
    	//$this->viewBuilder()->layout('customer-login');
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	 
    	if($id > 0) {
    		return $this->redirect(['controller' => 'Users', 'action' => 'home']);
    	}
        $states = $this->Users->States->find('list', ['limit' => 200]);
    	$this->set('states', $states);
    	$mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
        $this->set('mobileCarriers', $mobileCarriers);
    	
         $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
         $this->set('thaaliSize', $thaaliSize);
         
         $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
         $this->set('deliveryMethod', $deliveryMethod);
		 
		 $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
         $this->set('days', $days);
		
         $users  = $this->Users->find('list',[
         		'keyField' => 'id',
         		'valueField' => function ($e) {
         			return $e->full_name;
         		}, 'conditions' => ['user_role in (3,5)', 'status' => '1']]);
          
         $this->set('driverlist', $users);
         
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) { 
            $user = $this->Users->patchEntity($user, $this->request->data);
			$user->registration_type = 2;
			$user->status = 0;
		    $user->user_role = 4;
		//if ($this->data['User']['password'] == $this->Auth->password($this->data['User']['confirm_password']))
		//{
			if ($this->Users->save($user)) {
				
				//Insert Thaali
				$UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();			
				$UserThaaliInfo->user_id = $user->id; 
				$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
				$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
				$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
				$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
				$UserThaaliInfo->delivery_sms_notification 		=  $this->request->data['delivery_sms_notification'];
				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
				
				$userName =   $user->full_name;
				$newDriverId = $this->request->data['drivers'];
				if ($this->request->data['delivery_method'] == '2') {
					$msg = 'Salaam, <br/><br/>'.ucwords($userName). " has selected you as a driver for his Thaali deliveries.<br/><br/>";
					$msg.= "Contact Number is ".$user->mobile_phone. " ".$user->home_phone."<br/><br/>";
					$msg.= "<br/><br/>Shukran";
					$this->__updateUserDriverMapping ($user->id, $msg,  $newDriverId );
				}
				switch ($this->request->data['thaali_size']) {
					case '0': $size = 'None';break;
					case '1': $size =  'Small (1-2 servings)'; break;
					case '2': $size =  'Medium (3-4 Servings)';break;
					case '3': $size =  'Large (5-6 Servings)';break;
					case '4': $size =  'X-Small (Salawat)';break;
					case '5': $size =  'X-Large';break;
				}
				
				switch ($this->request->data['delivery_method']) {
					case '1': $deliveryMethod = 'Pick Up'; break;
					case '2': $deliveryMethod = 'Delivery';break;
				}
				
				
				$st = $this->Users->States->find()
				->select('abbrev')
				->where(['id' => $this->request->data['user_state']]);
				 
				
				foreach($st as $s):
				$userState = $s->abbrev;
				endforeach;
				
				$distributionInfo = $this->Users->UserDistributionMapping->newEntity();
				$distributionInfo->user_id 		= $user->id;
				$distributionInfo->distribution_id 		= 1;
				$this->Users->UserDistributionMapping->save($distributionInfo);
				
				$msg = "Dear Admin, <br/>A new user has registered for Faiz Chicago.<br/>";
				$msg = "Name: ".$this->request->data['first_name']." ". $this->request->data['middle_name']." ".$this->request->data['last_name']."<br/>";
				$msg .= "<b>Address</b>: "."<br/>";
				$msg .=  $this->request->data['address']."<br/>";
				$msg .=  $this->request->data['city'].", ";
				$msg .=  $userState." - ";
				$msg .=  $this->request->data['zipcode']."<br/><br/>";
				$msg .=  "<b>Contact Info</b>"."<br/>";
				$msg .=  "Email: ".$this->request->data['email_address']."<br/>";
				$msg .=  "Home Phone: ".$this->request->data['home_phone']."<br/>";
				$msg .=  "Mobile Phone: ".$this->request->data['mobile_phone']."<br/>";
				$msg .=  "Thaali Size: ".$size."<br/>";
				$msg .=  "Delivery Method: ".$deliveryMethod."<br/>";
				
				/*New User Registration*/
				$customerEmail = $this->request->data['email_address'];
				/*$customerContent = "Dear ".$this->request->data['first_name']." ". $this->request->data['middle_name']." ".$this->request->data['last_name'].",<br/>";
				$customerContent .= "<br/>";
				$customerContent .= "Thank you for registaring with us. Admin will contact you soon.";
				*/
				$customerContent = "Salaam,<br/><br/>
				<p>Mubarak.  You have enrolled to receive the barakat of Faiz al Mawaid al Burhaniyah.  
				Please login to your account on faizchicago.com and update your thaali calendar as needed.
				The login is your household head ejamaat number.
				Please login and change your user password as soon as possible. Changes to your thaali size or days
				needs to be made at least 7 days in advance.  Thaali menu is displayed for the entire month so you are 
				able to login and make your changes for the entire month in advance.
				</p>
				
				<p>
				Your thaali will start soon.If you have requested your thaali to be delivered please contact the following individuals for delivery options:<br/> <br/>
				Abdullah Bhai Ghatila - Bolingbrook <br/>
				Mustansir Cash - Downtown, North Chicago, Skokie <br/>
				Farida Bhen Kapadia - Naperville and Aurora <br/>
				Anwar Bhai Tapal - Darien, Lemont, Downers Grove, Bur Ridge, Hinsdale <br/>
				</p>
				
				<p>
				If you have opted to pick up your thaali from Masjid al Badri then your thaali will be available to pick up after 3pm in the hallway next to the men's shoe room.  If you have any questions/comments or need 
				help navigating the website please contact chicagomawaid@gmail.com and a commiittee member will 
				contact you.
				
				</p>
				
				
				Shukran<br/>
				Faiz al Mawaid al Burhaniya Chicago
				";
				//$ccAddr  = explode(',', REGISTRATION_EMAIL_CC_ADDR);
				$ccAddr  = REGISTRATION_EMAIL_CC_ADDR;
				 
				$email = new Email('default');
				$email->from([EMAIL_SENDER => 'Faizchicago'])
				->emailFormat('both')
				->to(ADMIN_EMAIL_ADDR)
				->addBcc($ccAddr)
				->subject('Faizchicago - New User Registration')
				->send($msg);
				
				/*Sending Email To Cusotmer*/
				$email->from([EMAIL_SENDER => 'Faizchicago'])
				->emailFormat('both')
				->to($customerEmail)
				->subject('Faizchicago - Account Registration')
				->send($customerContent);
				
				//return $this->redirect(['action' => 'index']);
				$this->Flash->success(__('You have been registered successfully.'));
				return $this->redirect(['controller' => 'Users', 'action' => 'login']);
			} else {
				$this->Flash->error(__('There is a problem in user registration. Please, try again.'));
			}
		//}	
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
   }

   
    public function home() {
    	$user = $this->Auth->identify();
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$role = $user_data['user_role'];
    	$this->set('role', $role);
    	// Getting Notification
    	$this->loadModel('Notification');
    	$edate = date('Y-m-d');
    	$notification = $this->Notification->find()
    	->select('details')
    	->where(['start_date <=' => $edate, 'end_date >=' => $edate]);
    	$info = '';
    
    	foreach ($notification as $not):
    	$info .= $not->details." ";
    	endforeach;
    	$this->set('info', $info);
    }
    
    
    
    public function myaccount() {
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('UserPayment');
    	$this->loadModel('Miqat');
    	
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$this->set('noDays', $noDays);
    	
    	$user = $this->Users->get($id, [
    			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
    	]);
    	  
    	// Get Assigned Driver Name
    	$query = $this->Users->UserDriverMapping->find()
    	 
    	->contain([
    			'Users' => function ($q) use($id) {
    				return $q
    				 
    				->where(['UserDriverMapping.user_id' => $id]);
    			}
    	]);
    	if ($query->count() > 0 ) {
    		foreach ($query as $driver) {
    			$driverId = $driver->driver_id;
    		}
    	
    		if ($driverId > 0) {
    			$query = $this->Users->get($driverId);
    		}
    		$query = $query->toArray();
    		$this->set('driverName', $query['first_name']." ".$query['middle_name']." ".$query['last_name']);
    	}
    	else
    		$this->set('driverName', '');
    	
    	 
    	if(!empty($user->user_thaali_info) > 0 ) {
    		foreach ($user->user_thaali_info as $thaaliInfo) {  
    			$this->set('thaali_size', $thaaliInfo->thaali_size);
    			$this->set('delivery_method',$thaaliInfo->delivery_method);
    			$this->set('driver_notes', $thaaliInfo->driver_notes);
    			$this->set('active_days', $thaaliInfo->active_days);
    			$this->set('delivery_sms_notification', $thaaliInfo->delivery_sms_notification);
    		}
    	}
    	else
    	{
    		$this->set('thaali_size','');
    		$this->set('delivery_method','');
    		$this->set('driver_notes', '');
    		$this->set('active_days', '');
    		$this->set('delivery_sms_notification', '');
    	}
    	$thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
    	$this->set('thaaliSize', $thaaliSize);
    	 
    	$deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
    	$this->set('deliveryMethod', $deliveryMethod);
    	
    	// Household Delivery Statistics
    	 
    	//For get this week
    	 
    	$sdate = date('y-m-d', strtotime("monday this week"));
    	//$edate = date('y-m-d', strtotime("saturday this week"));
    	$edate = date('y-m-d');
    	$wedate = date('y-m-d', strtotime("friday this week"));
    	//For get this month
    	 
    	$m_sdate = date('y-m-01');
    	$m_edate = date("y-m-t");
    	 
    	//For get this year
    	$y_sdate = date('y-01-01');
    	$y_edate = date('Y-12-31');
    	$weekstart = date('m/d/Y', strtotime("monday this week"));
    	$weekend = date('m/d/Y', strtotime("friday this week"));
    	$this->set('weekstart', $weekstart);
    	$this->set('weekend', $weekend);
    	// to get the count of today's delivery
    	$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
    	$todaysDelivery = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now,  'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
    	])->count();
    	$this->set('todaysDelivery', $todaysDelivery);
    	 
    	// to get the count of this week delivery
    	$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',  'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
    	])->count();
    	//debug($thisWeekDelivery);
    	$this->set('thisWeekDelivery', $thisWeekDelivery);
    	 
    	// to get the count of this month delivery
    	$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0',  'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
    	])->count();
    	$this->set('thisMonthDelivery', $thisMonthDelivery);
    	 
    	// to get the count of this year delivery
    	$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
    			'conditions' => array( ' ThaaliDelivery.user_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0',   'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
    	])->count();
    	$this->set('thisYearDelivery', $thisYearDelivery);
    	 
    	 
    	// count by this week
    	 
    	$query = $this->ThaaliDelivery->find();
    	$weekDelivery  = $query
    	->select([
    			'count' => $query->func()->count('id'),
    			'day' => 'dayname(delivery_date)'
    	])
    	->where(['user_id' => $id,   'delivery_date >=' => $sdate, 'delivery_date <=' => $wedate, 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']],  'OR' => [['ThaaliDelivery.delivery_type' => '1'], ['ThaaliDelivery.delivery_type' => '2']]])
    	->group('delivery_date');
    	 //debug($weekDelivery);
    	 
    	 $res    = array( 'Monday'=> '0', 'Tuesday'=> '0', 'Wednesday'=> '0', 'Thursday'=> '0', 'Friday'=> '0');
    	  
    	 foreach ($weekDelivery as $day):
    	  	 $res[$day['day']] =  $day['count'];
    	 endforeach;
    	 
    	 $this->set('weekDelivery', $res);
    	 $this->set('user', $user);
    	 $this->set('_serialize', ['user']);
     }
     
     public function menus($month=null, $year=null) {
     	
     	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	//$noDays = USER_THAALI_UPDATE_DAYS;
    	//$this->set('noDays', $noDays);
		$user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
        ]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	 switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     }
     
     public function changesize() {
     	$noDays = USER_THAALI_UPDATE_DAYS;
     	$now = Time::now();
     	$min = $now->modify("+$noDays days")->i18nFormat('MM/dd/yyyy');
     	$this->set('min', $min);
     	 
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	//$noDays = USER_THAALI_UPDATE_DAYS;
     	$user = $this->Users->get($id, [
     			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     	]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     }
     
     public function vacation() {
     	$this->loadModel('ThaaliDelivery');
     	$this->loadModel('UserVacationPlanner');
     	$userVacationPlanner = $this->UserVacationPlanner->newEntity();
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$userId = $user_data['id'];
     	$noDays = USER_VACATION_DATE_CONSTRAINTS;
     	$now = Time::now();
     	$min = $now->modify("+$noDays days")->i18nFormat('MM/dd/yyyy');
     	$this->set('user_id', $userId);
     	$this->set('min', $min);
     	
     	$reason = array('Out of town' => 'Out of town', 'Going for vacation' => 'Going for vacation', 'Other' => 'Other');
     	$this->set('reason', $reason);
     	
     	if ($this->request->is('post')) {
     		$startDate = $this->request->data['start_date'];
     		$endDate = $this->request->data['end_date'];
     
     	    $vacationCount = $this->UserVacationPlanner->find()
     		->where(['user_id' =>  $userId , 'OR' => [['start_date <=' => $startDate, 'start_date >=' => $endDate], ['end_date >=' => $startDate, 'end_date <=' => $endDate]]])->count();
     		 
     		 if ($vacationCount == 0 ) {
     			$now = Time::now();
     			$minDate =  $now->modify("+$noDays days")->i18nFormat('MM/dd/yyyy');
     			 
     			if ($minDate <= $startDate ) {
     				$dates = array();
     				$current = strtotime($startDate);
     				$last = strtotime($endDate);
     				
     				while( $current <= $last ) {
     				
     				 // foreach($dates as $vdate) {
     				 	
     				 	$userVacationPlanner = $this->UserVacationPlanner->newEntity();
     				 	$userVacationPlanner->user_id = $this->request->data['user_id']; 
     				 	$userVacationPlanner->title = $this->request->data['title']; 
     				 	$userVacationPlanner->driver_notes = $this->request->data['driver_notes']; 
     				 	$userVacationPlanner->start_date = $current ;
     				 	$userVacationPlanner->end_date = $current; 
     				 	//$this->$userVacationPlanner->save($userVacationPlanner);
     			 
     		        if ($this->UserVacationPlanner->save($userVacationPlanner)) { 
     					
     					//Update User Thaali order status
     					$query = $this->ThaaliDelivery->query();
     					$query->update()
     					->set(['order_status' => '3'])
     					->where(['delivery_date' =>  $current, 'user_id' => $userId, 'order_status !=' => '1'  ])
     					->execute();
     					//->where(['delivery_date >=' =>  $startDate, 'delivery_date <=' =>  $endDate,  'user_id' => $userId ])
     					// End of Update User Thaali order status
     					$dates[] = date('Y-m-d', $current);
     					$current = strtotime('+1 day', $current);
     				} else $this->Flash->error(__('There is the problem while adding vacation details.'));
     			  }
     			//}
     			
     			$this->Flash->success(__('Vacation planner has been added succesfully.')); 
     			return $this->redirect(['controller' => 'users', 'action' => 'vacation']);
     			}
     			else {
     				$this->Flash->error(__('Adding/modifing vacation information will be considered valid only if the event is scheduled for after '. $min));
     			}
     		}
     		else {
     			$this->Flash->error(__('The selected date is already added in your Vacation Planner list.'));
     		}
     	}
     	$users = $this->UserVacationPlanner->Users->find('list', ['limit' => 200]);
     	$this->set(compact('userVacationPlanner', 'users'));
     	$this->set('_serialize', ['userVacationPlanner']);
     	
     	//Getting vacation List
     	$now = Time::now();
     	$userVacationPlannerList = $this->UserVacationPlanner->find('all', [
     			'contain' => ['Users'],
     			'conditions' => ['user_id  ' => $userId, 'start_date >=' => $now]
     	]);
     	 
     	$this->set('userVacationPlannerList', $userVacationPlannerList);
     	$this->set('_serialize', ['userVacationPlannerList']);
     	
     }
     
     
     public function __checkUserVacationDetails($userId, $menuDate) {
     	$this->loadModel('UserVacationPlanner');
     	$userVacation = $this->UserVacationPlanner->find()
     	->select('id')->
     	where(['start_date <=' => $menuDate, 'end_date >=' => $menuDate, 'user_id' => $userId])->count();
     	if ($userVacation == 0)
     		return '0';
     	else return '3';
     }
     
     public function survey() {
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	//$noDays = USER_THAALI_UPDATE_DAYS;
     	$user = $this->Users->get($id, [
     			'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     	]);
     	
     	if(!empty($user->user_thaali_info) > 0 ) {
     		foreach ($user->user_thaali_info as $thaaliInfo) {
     			$thaali_size = $thaaliInfo->thaali_size;
     			$delivery_method = $thaaliInfo->delivery_method;
     			$driver_notes =  $thaaliInfo->driver_notes;
     		}
     	}
     	switch ($thaali_size) {
     		case '0': $thaali_size =  'None';break;
     		case '1': $thaali_size =  'Small (1-2 servings)'; break;
     		case '2': $thaali_size =  'Medium (3-4 Servings)';break;
     		case '3': $thaali_size =  'Large (5-6 Servings)';break;
     		case '4': $thaali_size =  'X-Small (Salawat)';break;
     		case '5': $thaali_size =  'X-Large';break;
     	}
     	
     	switch ($delivery_method) {
     		case '1': $delivery_method = 'Pick Up'; break;
     		case '2': $delivery_method = 'Delivery';break;
     	}
     	
     	$this->set('thaali_size', $thaali_size);
     	$this->set('delivery_method',$delivery_method);
     	$this->set('driver_notes', $driver_notes);
     	
     }
     public function getSurveyinfo()
	 {
		$monthstart = '2017-01-01';//start Date
    	$monthend =   '2017-05-31';//end Date
    	$thaaliList = array();
    	$this->loadModel('Thaali');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('Miqat');
		$this->loadModel('ThaaliSurvey');
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    
    	 // User Thaali delivery information
    	$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    	->select(['id', 'delivery_date', 'ThaaliInfo.menu_item',  'ThaaliDelivery.thaali_size',  'CatererInfo.name','order_status','thaali_id','ThaaliSurvey.is_oil'])
    	->join([
    			't' => [
    					'table' => 'thaali',
    					'alias' => 'ThaaliInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    					'fields' => ['ThaaliInfo.menu_item']
    					 
    			],
    			'c' => [
    					'table' => 'caterer',
    					'alias' => 'CatererInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    					'fields' => ['CatererInfo.name']
    			],
				's' => [
    					'table' => 'thaali_survey',
    					'alias' => 'ThaaliSurvey',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliSurvey.thaali_id = ThaaliInfo.id AND ThaaliSurvey.user_id ='.$id,
    					'fields' => ['ThaaliSurvey.is_oil']
    			]
    	])
    	->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
    	->order(['delivery_date' => 'DESC']);
    
    	$calandar = array();
    
    	 
    	foreach($thaaliDeleiveryInfo as $thaali):
    	switch ($thaali['thaali_size']) {
    		case '0': $thaali_size =  'None';break;
    		case '1': $thaali_size =  'Small Thaali'; break;
    		case '2': $thaali_size =  'Medium Thaali';break;
    		case '3': $thaali_size =  'Large Thaali';break;
    		case '4': $thaali_size =  'X-Small Thaali)';break;
    		case '5': $thaali_size =  'X-Large Thaali';break;
    	}
    	$res = array();
    	$res['id'] = $thaali['id'];
    	if($thaali['order_status']== 1 && $thaali['ThaaliSurvey']['is_oil'] == '' && $thaali['delivery_date'] <= date('Y-m-d') ){  
			$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<a href="javascript:thaaliReview('.$thaali['thaali_id'].')"><div class="fc-edit-content" data-toggle="modal" data-target="#review_dialog"><span>Add Review</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div></a>';
    	}
		else if($thaali['order_status']== 0 )
    	{
    		$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'];
    	}
		else if($thaali['order_status']== 1 && $thaali['ThaaliSurvey']['is_oil'] != ''){  
			$res['title'] =  ucwords($thaali['ThaaliInfo']['menu_item'])."  <hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<div class="fc-edit-content"><span>Review Added</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div>';
    	}
    	
    	$res['start'] = $thaali['delivery_date']->format('Y-m-d');
    	$res['cssclass'] = 'blue';
    	$res['allDay'] = false;
    	array_push($calandar, $res);
    	endforeach;
        
    	echo json_encode($calandar);
    	exit(); 
	 }
     public function drivers() {
     
     	$driverDetails = $this->paginate = [
     			'contain' => ['States'],
     			'conditions' => ['Users.status ' => '1', 'OR' => [['user_role' => '3' ], ['user_role' => '5']]] ,
     			'order' => ['Users.created' => 'DESC']
     	];
     	$driverDetails = $this->paginate($this->Users);
     	$this->set('driverDetails', $driverDetails);
     }
      
     
     public function contactus() {
     	$this->loadModel('ContactFeeds');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     
     	 
     
     	if ($this->request->is('post')) {
     		$contactFeed = $this->ContactFeeds->newEntity();
     		$contactFeed->user_id = $id;
     		$contactFeed->message = $this->request->data['msg'];
     		$to =CONTACT_US_RECEIVER;
     		$msg = "The user has submitted for the below request.</br></br>";
     		$msg .= "Name: ".$user_data['first_name']." ". $user_data['middle_name']." ".$user_data['last_name']."<br/>";
     		$msg .=  "Email: ".$user_data['email_address']."<br/>";
     		$msg .=  "Home Phone: ".$user_data['home_phone']."<br/>";
     		$msg .=  "Mobile Phone: ".$user_data['mobile_phone']."<br/><br/>";
     		$msg .=  "Message: ".$this->request->data['msg']."<br/>";
     		 
     		if ($this->ContactFeeds->save($contactFeed)) {
     			$this->Flash->success(__('Your message has been submitted successfully.'));
     			$email = new Email('default');
     			$email->from([EMAIL_SENDER => 'Faizchicago'])
     			->emailFormat('both')
     			->to($to)
     			->subject('Faizchicago - Contact Us')
     			->send($msg);
     
     			return $this->redirect(['action' => 'contactus']);
     		} else {
     			$this->Flash->error(__('There is a problem while sending your message.'));
     		}
     	}
     
     }
     
     public function deliveries() {
     	$this->loadModel('ThaaliDelivery');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     	$fromDate = date('Y-m-d') ;
     	$toDate =  date('Y-m-d') ;
     	if ($this->request->is('post')) {
     		$fromDate = $this->request->data['dtfrom'];
     		$toDate = $this->request->data['dtto'];
     	
     	}
     	// Driver Thaali delivery information
     	$driverDeleiveryInfo = $this->ThaaliDelivery->find('all')
     	->hydrate(false)
     	->select(['ThaaliDelivery.id', 'ThaaliDelivery.order_status', 'delivery_date', 'delivery_type', 'delivery_notes', 'thaali_size', 'Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode', 'States.abbrev'])
     	->join([
     
     			'c' => [
     					'table' => 'users',
     					'alias' => 'Users',
     					'type' => 'LEFT ',
     					'conditions' => 'Users.id = ThaaliDelivery.user_id',
     					'fields' => ['Users.first_name', 'Users.middle_name', 'Users.last_name', 'Users.mobile_phone', 'Users.address', 'Users.city', 'Users.zipcode']
     			],
     			's' => [
     					'table' => 'states',
     					'alias' => 'States',
     					'type' => 'LEFT ',
     					'conditions' => 'States.id = Users.user_state',
     					'fields' => ['States.abbrev']
     
     			],
     	])
     	->where([' ThaaliDelivery.driver_id' => $id, ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $fromDate, 'ThaaliDelivery.delivery_date <=' => $toDate, 'ThaaliDelivery.thaali_size !=' => '0', 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
     	->order(['delivery_date' => 'DESC']);
     	
     	//debug($driverDeleiveryInfo);
     	$this->set('driverDeleiveryInfo', $driverDeleiveryInfo);
     	$this->set('fromdate',$fromDate);
     	$this->set('todate',$toDate);
    }
     
     public function deliveryStatistics() {
     	$this->loadModel('ThaaliDelivery');
     	$session = $this->request->session();
     	$user_data = $session->read('Auth.User');
     	$id = $user_data['id'];
     
     
     	// Driver Delivery Statistics
     
     	//For get this week
     
     	$sdate = date('y-m-d', strtotime("monday this week"));
     	//$edate = date('y-m-d', strtotime("saturday this week"));
     	$edate = date('y-m-d');
     
     	//For get this month
     
     	$m_sdate = date('y-m-01');
     	$m_edate = date("y-m-t");
     
     	//For get this year
     	$y_sdate = date('y-01-01');
     	$y_edate = date('Y-12-31');
     
     	// to get the count of today's delivery
     	$now = Time::now('America/Chicago')->i18nFormat('yyyy-MM-dd');
     	$todaysDelivery = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', 'ThaaliDelivery.delivery_date' => $now, ' ThaaliDelivery.delivery_type' => '2', 'OR' => [['ThaaliDelivery.order_status' => '0'], ['ThaaliDelivery.order_status' => '1']])
     	])->count();
     	$this->set('todaysDelivery', $todaysDelivery);
     
     	// to get the count of this week delivery
     	$thisWeekDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id ,  'ThaaliDelivery.thaali_size !=' => '0',' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $sdate, 'ThaaliDelivery.delivery_date <=' => $edate,    'ThaaliDelivery.order_status' => '1')
     	])->count();
     	//debug($thisWeekDelivery);
     	$this->set('thisWeekDelivery', $thisWeekDelivery);
     
     	// to get the count of this month delivery
     	$thisMonthDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $m_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,   'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('thisMonthDelivery', $thisMonthDelivery);
     
     	// to get the count of this year delivery
     	$thisYearDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2', 'ThaaliDelivery.delivery_date >=' => $y_sdate, 'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('thisYearDelivery', $thisYearDelivery);
     
     	// to get the count of All time delivery
     	$allTimeDelivery  = $this->ThaaliDelivery->find('all', [
     			'conditions' => array( ' ThaaliDelivery.driver_id' => $id,  'ThaaliDelivery.thaali_size !=' => '0', ' ThaaliDelivery.delivery_type' => '2',  'ThaaliDelivery.delivery_date <=' => $edate,  'ThaaliDelivery.order_status' => '1')
     	])->count();
     	$this->set('allTimeDelivery', $allTimeDelivery);
     	// End of Driver Delivery Statistics
     
     	// count by month
     
     	$query = $this->ThaaliDelivery->find();
     	$monthsDelivery  = $query
     	->select([
     			'count' => $query->func()->count('id'),
     			'mon' => 'MONTH(delivery_date)'
     	])
     	->where(['driver_id' => $id,  'delivery_type' => '2', 'delivery_date >=' => $y_sdate, 'delivery_date <=' => $y_edate,  'order_status' => '1'])
     	->group('mon');
     
     	$res    = array(1 => '0', 2 => '0', 3 => '0', 4 => '0', 5 => '0', 6 => '0', 7 => '0', 8 => '0', 9 => '0', 10 => '0', 11 => '0', 12 => '0');
     
     	foreach ($monthsDelivery as $monthsDelivery):
     	$res[$monthsDelivery['mon']] =  $monthsDelivery['count'];
     	endforeach;
     	 
     	$this->set('monthsDelivery', $res);
     
     }
     
     public function __getThisWeekCalandar($weekStart, $weekEnd) {
     	$thaaliDetails = $this->__getThisWeekThaali($weekStart, $weekEnd);
     	$miqatDetails = $this->__getThisWeekMiqat($weekStart, $weekEnd);

     	$result = array_merge($thaaliDetails, $miqatDetails);
     	if(count($result) > 0) {
        foreach ($result as $key => $row) {
       	 $mid[$key]  = $row['date'];
        }
        array_multisort($mid, SORT_ASC, $result);
     	}
      return $result;
     }
     
     
     
     public function __getThisWeekThaali($weekStart, $weekEnd) {
     // $weekstart = date('y-m-d', strtotime("monday this week"));
     //	$weekend =  date('y-m-d', strtotime("friday this week")); 
     	$thaaliList = array();
     	$this->loadModel('Thaali');
     	$thisWeekThaali = $query = $this->Thaali->find()
		->select(['Caterer.name','Thaali.menu_date', 'Thaali.menu_item'])
		->join([
				'c' => [
						'table' => 'caterer',
						'alias' => 'Caterer',
						'type' => 'LEFT',
						'conditions' => 'Thaali.caterer_id = Caterer.id',
						'fields' => ['Caterer.name']
				],
				 
		])
		->where([ 'date(Thaali.menu_date) >=' => $weekStart, 'date(Thaali.menu_date) <=' => $weekEnd ]);
		$thisWeekThaali = $thisWeekThaali->toArray();
		$res = array();
			foreach($thisWeekThaali as $thaali):
			$res['type'] = 'Thaali';
	     	$res['date'] = $thaali['menu_date']->format('Y-m-d');
	     	$res['title'] =  $thaali['menu_item'];
	     	$res['details'] = $thaali['Caterer']['name'];
	     	array_push($thaaliList, $res) ;
	    endforeach;
	   return $thaaliList;			     	 
     }
     
     public function __getThisWeekMiqat($weekStart, $weekEnd) {
     	$weekList = array();
     	$miqatList = array();
     	  $weekstart = strtotime($weekStart);
     
     //	$weekend =  date('y-m-d', strtotime("saturday this week"));
     	if (date('Y-m-d', $weekstart) == date('Y-m-d', time() - 7*24*3600)) {
     		// we are that day... => add one week
     		$weekStart += 7 * 24 * 3600;
     	}
     	
     	$currentDay = $weekstart;
     	for ($i = 0 ; $i < 5 ; $i++) {
     		array_push($weekList, date('Y-m-d', $currentDay));
     		$currentDay += 24 * 3600;
     	}
     	 
     	$this->loadModel('Miqat');
     	 foreach ($weekList as $mday): 
	     	$thisWeekMiqat = $query = $this->Miqat->find('all')
	     	->select(['event_title','start_date', 'end_date','details'])
	     	->where([ 'date(Miqat.start_date) <=' => $mday, 'date(Miqat.end_date) >=' => $mday ]);
	     	$res = array();
	     	foreach($thisWeekMiqat as $miqat):
	     	$res['type'] = 'Miqat';
	     	$res['date'] = $mday;
	     	$res['title'] = $miqat->event_title;
	     	$res['details'] = $miqat->details;
	     	array_push($miqatList, $res) ;
	     	endforeach;
	     	
     	endforeach; 
     	return $miqatList;
     }
    
	 

	public function edit()	{
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
        $user = $this->Users->get($id, [
            'contain' => ['DriverInfo', 'UserThaaliInfo']
        ]);
        
         $noDays = USER_THAALI_UPDATE_DAYS;
         $role = $this->Users->Role->find('list', ['limit' => 200]);
         $states = $this->Users->States->find('list', ['limit' => 200]);
         $userStatus = array('0' => 'Unapproved', '1' => 'active', '2' => 'Inactive', '3' => 'Trashed - Unapproved User', '4' => 'Trashed- Approved User');
         $this->set('userStatus', $userStatus);
         $mobileCarriers = $this->Users->MobileCarrierList->find('list', ['limit' => 200]);
         $this->set('mobileCarriers', $mobileCarriers);
          
         $thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
         $this->set('thaaliSize', $thaaliSize);
         
         $deliveryMethod = array('1' => 'Pick Up', '2' => 'Delivery');
         $this->set('deliveryMethod', $deliveryMethod); 
         
         $days = array('1' =>'Monday',  '2' =>  'Tuesday', '3' => 'Wednesday', '4' =>  'Thursday', '5' =>  'Friday');
         $this->set('days', $days);
         $users  = $this->Users->find('list',[
         		'keyField' => 'id',
         		'valueField' => function ($e) {
         			return $e->full_name;
         		}, 'conditions' => ['user_role in (3,5)', 'status' => '1']]);
         
         $this->set('driverlist', $users);
         
         // Get Assigned Driver id
         $driverId = 0;
         $query = $this->Users->UserDriverMapping->find()
         
         ->contain([
         		'Users' => function ($q) use($id) {
         			return $q
         
         			->where(['UserDriverMapping.user_id' => $id]);
         		}
         ]);
         
         if ($query->count() > 0 ) {
         	foreach ($query as $driver) {
         		$driverId = $driver->driver_id;
         	}
         }
         
         $this->set('driverId', $driverId);
         
         
         if (!$this->request->is(['post'])) {
         	$this->set('role', $role);
         	$this->set('role_id',$user->role);
         	$this->set('states', $states);
         	$this->set('state_id',$user->state);
         	$this->set('registration_type', $user->registration_type);
         	$this->set('mobile_carrier',$user->mobile_carrier);
         	$this->set('status', $user->status);
         }	
         
         if(!empty($user->user_thaali_info) > 0 ) {
        	foreach ($user->user_thaali_info as $thaaliInfo) {
        	$this->set('thaali_id', $thaaliInfo->id);
        	$this->set('thaali_size', $thaaliInfo->thaali_size);
        	$this->set('delivery_method',$thaaliInfo->delivery_method);
        	$this->set('driver_notes', $thaaliInfo->driver_notes);
        	$this->set('active_days', $thaaliInfo->active_days);
        	$this->set('delivery_sms_notification', $thaaliInfo->delivery_sms_notification);
        }	
        }
        else
        {
        	$this->set('thaali_size','');
        	$this->set('delivery_method','');
        	$this->set('driver_notes', '');
        	$this->set('active_days', '');
        	$this->set('delivery_sms_notification', '');
        }
         	
         	if ($this->request->is(['patch', 'post', 'put'])) {
         		$user = $this->Users->patchEntity($user, $this->request->data);
         		if ($this->Users->save($user)) {
         			 
	         		//Update User Thaali information
	         		 $UserThaaliInfo = $this->Users->UserThaaliInfo->newEntity();
	         	     $userName =   $user->full_name;		 
	         		 	if ($user->user_role != 3 )	{
		         			$UserThaaliInfo->id = $this->request->data['thaali_id'];
		         			$UserThaaliInfo->user_id = $user->id; 
		         			$UserThaaliInfo->thaali_size		= $this->request->data['thaali_size'];
		         	    	$UserThaaliInfo->delivery_method 	= $this->request->data['delivery_method'];
		         			$UserThaaliInfo->driver_notes  		= $this->request->data['driver_notes'];
		         			$UserThaaliInfo->active_days 		= implode(',', $this->request->data['active_days']);
		         			$UserThaaliInfo->delivery_sms_notification = $this->request->data['delivery_sms_notification'];
		         		    if($this->request->data['thaali_size'] != '') {
		         				$this->Users->UserThaaliInfo->save($UserThaaliInfo);
		         				$newDriverId = $this->request->data['drivers'];
		         				if ($this->request->data['delivery_method'] == '2' && $driverId != $newDriverId) {
		         					$msg = 'Salaam, <br/><br/>'.ucwords($userName). " has selected you as a driver for his Thaali deliveries.<br/><br/>";
									$msg.= "Contact Number is ".$user->mobile_phone. " ".$user->home_phone."<br/><br/>";
									$msg.= "<br/><br/>Shukran";
									$this->__updateUserDriverMapping ($user->id, $msg,  $newDriverId );
		         				}	
		         				$this->__updateUserThaaliDeliveryInfo ($user->id, $noDays, 'update' );
		         			}
		         		 }	 
		         		 //$this->Flash->success(__('The user details has been updated successfully.'));
         	  			 return $this->redirect(['action' => 'myaccount']);
         		} else {
         			$this->Flash->error(__('The user could not be saved. Please, try again.'));
         		}
         	}  	 
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
	
	
    
    public function getUserThaaliDeliveryById ($id = null) {
    	$this->viewBuilder()->layout('empty');
    	if ($this->request->is('ajax')) {
    
    		$id			= $this->request->data['id'];
    		$this->loadModel('ThaaliDelivery');
    		$thaaliSize = array('0' => 'None', '1' => 'Small (1-2 servings)', '2' => 'Medium (3-4 Servings)', '3' => 'Large (5-6 Servings)', '4' => 'X-Small (Salawat)', '5' => 'X-Large');
    		$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    		->hydrate(false)
    		->select(['id', 'delivery_date', 'delivery_type', 'thaali_size', 'delivery_notes', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
    		->join([
    				't' => [
    						'table' => 'thaali',
    						'alias' => 'ThaaliInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    						'fields' => ['ThaaliInfo.menu_item']
    
    				],
    				'c' => [
    						'table' => 'caterer',
    						'alias' => 'CatererInfo',
    						'type' => 'LEFT ',
    						'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    						'fields' => ['CatererInfo.name']
    				],
    		])
    		->where([' ThaaliDelivery.id' => $id] );
    		$this->set('thaaliDeleiveryInfo', $thaaliDeleiveryInfo);
    		$this->set('thaaliSize', $thaaliSize);
    	}
    }
    public function editThaaliDelivery() {
    	if ($this->request->is('ajax')) {
    		$this->autoRender = false;
    		 
    		$this->loadModel('ThaaliDelivery');
    		$id			= $this->request->data['delivery_id'];
    		$thaaliSize			= $this->request->data['thaali_size'];
    		
    		$query = $this->ThaaliDelivery->query();
    		$query->update()
    		->set(['thaali_size' => $thaaliSize] )
    		->where(['id ' => $id])
    		->execute();
    
    	}
    	$this->autoRender = false;
    }
    
    public function __updateUserThaaliDeliveryInfo ($userId, $days = null, $type= null) {
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('DriverReplacement');
    	 
    	// Update delivery details from next day onwards
    	//Getting user information
    	$query = $this->Users->find('all')
    	->hydrate(false)
    	->select(['Users.id', 'd.driver_id', 't.thaali_size', 'ud.distribution_id','t.delivery_method', 't.driver_notes', 't.active_days'])
    	->join([
    			't' => [
    					'table' => 'user_thaali_info',
    					'type' => 'RIGHT',
    					'conditions' =>  'Users.id = t.user_id',
    			],
    			'd' => [
    					'table' => 'user_driver_mapping ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = d.user_id',
    			],
    			'ud' => [
    					'table' => 'user_distribution_mapping  ',
    					'type' => 'LEFT ',
    					'conditions' => 'Users.id = ud.user_id',
    			],
    	])
    	->where([' Users.id' => $userId]);
    	 
    	foreach ($query as $info) {
    		$user_id =  $info['id'];
    		$driver_id = $info['d']['driver_id'];
    		$thaali_size = $info['t']['thaali_size'];
    		$distribution_id = $info['ud']['distribution_id'];
    		$delivery_type = $info['t']['delivery_method'];
    		$delivery_notes = $info['t']['driver_notes'];
    		$userWeekDays = $info['t']['active_days'];
    	}
    	 
    	if ($days > 0 )
    		$sdate = date('Y-m-d',  strtotime("+$days days"));
    	else
    	{
    		$sdate = date('Y-m-d',  strtotime("+1 days"));
    		$query = $this->ThaaliDelivery->query();
    		$query->update()
    		->set(['order_status' => '0'])
    		->where(['delivery_date >=' =>  $sdate, 'user_id' => $id ])
    		->execute();
    	}
    
    
    	// Getting Thaali information for the upcoming days
    	$connection = ConnectionManager::get('default');
    	$thaali = $connection->execute('SELECT id AS `id`,menu_date FROM thaali Thaali
								WHERE  Thaali.menu_date >="'.$sdate.'"  AND (DAYOFWEEK(Thaali.menu_date))-1 in ('.$userWeekDays.') ');
    	//Deleting old upcoming data thaali information from thaali delivery table
    	 
    
    	$query = $this->ThaaliDelivery->query();
    	$query->delete()
    	->where(['delivery_date >=' =>  $sdate, 'user_id' =>  $userId ])
    	->execute();
    	// end of thaali delivery deletion
    	 
    	 
    	//inserting thaali delivery information
    	foreach ($thaali as $info) {
    		$ThaaliDelivery = $this->ThaaliDelivery->newEntity();
    		$ThaaliDelivery->user_id =  $user_id ;
    		if ($delivery_type == 2)
    			$ThaaliDelivery->driver_id = $driver_id;
    		else 
    			$ThaaliDelivery->driver_id = 0;
    		
    		$ThaaliDelivery->thaali_size = $thaali_size;
    		$ThaaliDelivery->distribution_id = $distribution_id;
    		$ThaaliDelivery->delivery_type = $delivery_type;
    		$ThaaliDelivery->delivery_notes = $delivery_notes;
    		$ThaaliDelivery->order_status = $this->__checkUserVacationDetails($user_id, $info['menu_date']);
    		$ThaaliDelivery->thaali_id = $info['id'];
    		$ThaaliDelivery->delivery_date = $info['menu_date'];
    
    		// Checking driver replacment data
    		 if ($delivery_type == 2) { 
    		 		$driverData = $this->DriverReplacement->find()
    		                     ->select('replace_driver_id')->
    		                      where(['from_date <=' => $info['menu_date'], 'to_date >=' => $info['menu_date'], 'driver_id' => $driver_id])->order(['id' => 'DESC'])->first();
    	 
    		
    		if (count( $driverData) > 0 ) {
    		foreach ($driverData as $driver) {
    			$driverId = $driver->replace_driver_id;
    			$ThaaliDelivery->driver_id = $driverId;
    		}
    		 }
    		 }
    		$this->ThaaliDelivery->save($ThaaliDelivery);
    		
    	}
    	if ($days == 7)
    			$this->Flash->success(__('The user details has been saved.The Thaali changes will be effetive from after '.$days." days."));
    		else 
    			$this->Flash->success(__('The user details has been saved.'));
    
    }
    
	public function getCustomercalendar($monthstart = null,$monthend = null)  {
     $thaaliList = array();
     $this->loadModel('Thaali');
     $this->loadModel('ThaaliDelivery');
     $this->loadModel('UserVacationPlanner');
        $this->loadModel('Miqat');
     $session = $this->request->session();
     $user_data = $session->read('Auth.User');
     $id = $user_data['id'];
      
     $user = $this->Users->get($id, [
       'contain' => ['DriverInfo', 'LoginHistory', 'UserDriverMapping', 'Role', 'UserDistributionMapping', 'UserThaaliInfo', 'ThaaliDelivery', 'States', 'MobileCarrierList']
     ]);
     
     // User Thaali delivery information
     $thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
     ->select(['id', 'delivery_date', 'ThaaliInfo.menu_item', 'CatererInfo.name'])
     ->join([
       't' => [
         'table' => 'thaali',
         'alias' => 'ThaaliInfo',
         'type' => 'LEFT ',
         'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
         'fields' => ['ThaaliInfo.menu_item']
     
       ],
       'c' => [
         'table' => 'caterer',
         'alias' => 'CatererInfo',
         'type' => 'LEFT ',
         'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
         'fields' => ['CatererInfo.name']
       ],
     ])
     ->where([' ThaaliDelivery.user_id' => $id, 'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
     ->order(['delivery_date' => 'DESC']);

     $calandar = array();
    
     
     foreach($thaaliDeleiveryInfo as $thaali):  
      $res = array();
     $menu = strlen($thaali['ThaaliInfo']['menu_item']) > 155 ? substr($thaali['ThaaliInfo']['menu_item'],0,155)."..." : $thaali['ThaaliInfo']['menu_item'];
      
      $res['id'] = $thaali['id'];
      $res['title'] =  $menu."<hr /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'];
      $res['start'] = $thaali['delivery_date']->format('Y-m-d');
   	  $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;

     // Get Miqat list
     $miqat = $this->Miqat->find('all')
     ->select(['event_title','start_date', 'end_date', 'details'])
     ->where([' status' => '1'] )
     ->order(['start_date' => 'DESC']);
        
     //User Vacation list
     $userVacationPlanner = $this->UserVacationPlanner->find('all')
     ->select(['start_date', 'end_date'])
     ->where([' user_id' => $id] )
     ->order(['start_date' => 'DESC']);
 
        foreach($miqat as $mq):
         $res = array();
      $res['id'] = '';
      $res['title'] =  $mq['event_title']."<br/> ". $mq['details'];
      $res['start'] = $mq['start_date']->format('Y-m-d');
     // $res['end'] =  $mq['end_date']->format('Y-m-d');
     $edate = date('Y-m-d', strtotime($mq['end_date']. ' +1 day'));
      $res['end'] =  $edate; 
  	  $res['color'] = '#4ad04a';
	  $res['textColor'] = 'white';
      $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;
  //$calandar =  array_merge($calandar,$res );
     
   
     foreach($userVacationPlanner as $vacation):
      $res = array();
      $res['id'] = "";
   //  $res['title'] = "Vacation Period - ". $vacation['start_date']->format('m/d/Y'). "  ". $vacation['end_date']->format('m/d/Y') ;
      $res['title'] = "&nbsp;Vacation" ;
      $res['start'] = $vacation['start_date']->format('Y-m-d');
     // $res['end'] =  $vacation['end_date']->format('Y-m-d');
      $edate = date('Y-m-d', strtotime($vacation['end_date']. ' +1 day'));
      $res['end'] =  $edate;
      $res['color'] = '#f75b5b';
      $res['textColor'] = 'white';
      $res['allDay'] = true;
      array_push($calandar, $res);
     endforeach;

     echo json_encode($calandar);
     exit();
    }
    
    public function getCustomereditcalendar($monthstart = null,$monthend = null)  {
    //	$monthstart = '2017-01-01';//start Date
    //	$monthend =   '2017-05-31';//end Date
    	$thaaliList = array();
    	$this->loadModel('Thaali');
    	$this->loadModel('ThaaliDelivery');
    	$this->loadModel('UserVacationPlanner');
    	$this->loadModel('Miqat');
    	$noDays = USER_THAALI_UPDATE_DAYS;
    	$session = $this->request->session();
    	$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
    	$noDays = USER_THAALI_UPDATE_DAYS;
     	$now = Time::now();
     	$min = $now->modify("+$noDays days")->i18nFormat('yyyy/MM/dd');
    	$this->set('min', $min);
    	
    	 // User Thaali delivery information
    	$thaaliDeleiveryInfo = $this->ThaaliDelivery->find('all')
    	->select(['id', 'delivery_date', 'ThaaliInfo.menu_item', 'ThaaliDelivery.thaali_size',  'CatererInfo.name'])
    	->join([
    			't' => [
    					'table' => 'thaali',
    					'alias' => 'ThaaliInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'ThaaliInfo.id = ThaaliDelivery.thaali_id',
    					'fields' => ['ThaaliInfo.menu_item']
    					 
    			],
    			'c' => [
    					'table' => 'caterer',
    					'alias' => 'CatererInfo',
    					'type' => 'LEFT ',
    					'conditions' => 'CatererInfo.id = ThaaliInfo.caterer_id',
    					'fields' => ['CatererInfo.name']
    			],
    	])
    	//->where([' ThaaliDelivery.user_id' => $id, 'ThaaliDelivery.delivery_date >=' => $min,  'ThaaliDelivery.order_status' => '0' ] )
    	->where([' ThaaliDelivery.user_id' => $id,  'OR' => [['ThaaliDelivery.order_status' => 0], ['ThaaliDelivery.order_status' => 1]]] )
    	->order(['delivery_date' => 'DESC']);
    	
    	 
    	$calandar = array();
    
    	 
    	foreach($thaaliDeleiveryInfo as $thaali):
    	switch ($thaali['thaali_size']) {
    		case '0': $thaali_size =  'None';break;
    		case '1': $thaali_size =  'Small Thaali'; break;
    		case '2': $thaali_size =  'Medium Thaali';break;
    		case '3': $thaali_size =  'Large Thaali';break;
    		case '4': $thaali_size =  'X-Small Thaali)';break;
    		case '5': $thaali_size =  'X-Large Thaali';break;
    	}
    	$res = array();
    	$menu = strlen($thaali['ThaaliInfo']['menu_item']) > 155 ? substr($thaali['ThaaliInfo']['menu_item'],0,155)."..." : $thaali['ThaaliInfo']['menu_item'];
    	
    	$res['id'] = $thaali['id'];
    	if(date('Y-m-d', strtotime("+$noDays days")) <= date('Y-m-d', strtotime($thaali['delivery_date'])) ){  
    	$res['title'] =  $menu."  <br /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<a href="javascript:userEdit('.$thaali['id'].')"><div class="fc-edit-content"><i class="fa fa-truck fa-3 fa-flip-horizontal" aria-hidden="true"></i>&nbsp;<span>'.$thaali_size.'</span><i class="fa fa-chevron-right" aria-hidden="true"></i></div></a>';
    	}
    	else 
    	{
    		$res['title'] =  "<div class='fc-edit-content-1'>".$menu."  <br /> <b>Caterer</b>:".$thaali ['CatererInfo']['name'].'<div class="fc-edit-content-1"><i class="fa fa-truck fa-3 fa-flip-horizontal" aria-hidden="true"></i>&nbsp;<span>'.$thaali_size.'</span></div></div>';
    	//	$res['color'] = '#f4f0ef';
    	}
    	$res['start'] = $thaali['delivery_date']->format('Y-m-d');
    	//$res['cssclass'] = 'blue';
    	$res['allDay'] = false;
    	array_push($calandar, $res);
    	endforeach;
        
    	echo json_encode($calandar);
    	exit();
    }
    
	public function surveySubmit()
	{
		$session = $this->request->session();
		$user_data = $session->read('Auth.User');
    	$id = $user_data['id'];
		$this->loadModel('ThaaliSurvey');
		$surveyInfo = $this->ThaaliSurvey->newEntity();
		
		if ($this->request->is('post'))
		{
			$surveyInfo->user_id = $id;
			$surveyInfo->thaali_id = $this->request->data['thaali_id'];
			$surveyInfo->thaali_taste = $this->request->data['taste'];
			$surveyInfo->thaali_qty = $this->request->data['happy'];
			$surveyInfo->is_oil = $this->request->data['is_oil'];
			if($this->ThaaliSurvey->save($surveyInfo))
			{
				echo "<div class='alert alert-success' role='alert'>Thaali Survey Submitted Successfully.</div>";
			} 		
			else{
				echo "<div class='alert alert-danger' role='alert'>There is the problem while submitting Thaali Survey.</div>";
			}
		}
		exit;
	}
	
	public function deleteVacation() {
		$this->autoRender = false;
		$session = $this->request->session();
		$this->loadModel('UserVacationPlanner');
		$this->loadModel('ThaaliDelivery');
		
		$id			= $this->request->data['id'];
		//$id			= 120;
		$user_data = $session->read('Auth.User');
		$userId = $user_data['id'];
		
		//Getting date from vacation planner table based on the ID
		 $userVacationPlannerCount = $this->UserVacationPlanner->find('all', [
	   		'conditions' => array('id' => $id),
	     ])->count();
	   
	   
	    $userVacationPlanner = $this->UserVacationPlanner->get($id); 
	    if($userVacationPlannerCount == 1) {
	        $userId     = $userVacationPlanner->user_id;
	        $thaaliDate = $userVacationPlanner->start_date;
	        $query = $this->ThaaliDelivery->query();
	    						$query->update()
	    						->set(['order_status' => '0'])
	    						->where(['delivery_date' =>  $thaaliDate,   'user_id' => $userId, 'order_status !=' => '1' ])
	    						->execute();
	        
	        if ( $this->UserVacationPlanner->delete($userVacationPlanner)) {
	            echo "<div class='alert alert-success' role='alert'>Vacation  has been deleted.</div>";
	        } else {
	            echo "<div class='alert alert-danger' role='alert'>Vacation planner could not be deleted. Please, try again.</div>";
	            
	        } 
	    }
	    else {
	    	echo "<div class='alert alert-danger' role='alert'>Vacation details is not found in the records.</div>";
	    }
	    
	    exit;
	}
	
	
	public function fatehaSalwaat() {
		$this->loadModel('Fateha');
		$session = $this->request->session();
		$user_data = $session->read('Auth.User');
		$userId = $user_data['id'];
		$this->set('user_id', $userId);
		if ($this->request->is('post')) {
			$fateha = $this->Fateha->newEntity();
			$fateha->user_id = $this->request->data['user_id'];
			$fateha->items = $this->request->data['items'];
			$fateha->fateha_names = $this->request->data['fateha_names'];
			$fateha->fateha_date =  $this->request->data['fateha_date']; 
			$item = $this->request->data['items'];
			if ($item == 0)
				$item = 'Sweets';
		    else 
		    	$item = 'Fruits';
		    
			if($this->Fateha->save($fateha)){
				$this->Flash->success(__('Fateha/Salwaat request has been succesfully send.'));
				$to = FATEHA_RECEIVER;
				$msg = "The user has submitted for the below request.</br></br>";
				$msg .= "Name: ".$user_data['first_name']." ". $user_data['middle_name']." ".$user_data['last_name']."<br/>";
				$msg .=  "Email: ".$user_data['email_address']."<br/>";
				$msg .=  "Home Phone: ".$user_data['home_phone']."<br/>";
				$msg .=  "Mobile Phone: ".$user_data['mobile_phone']."<br/><br/>";
				$msg .=  "Fateha Date: ". $this->request->data['fateha_date']."<br/>";
				$msg .=  "Fateha names: ". $this->request->data['fateha_names']."<br/>";
				$msg .=  "Item: ". $item."<br/>";
				
				    $email = new Email('default');
					$email->from([EMAIL_SENDER => 'Faizchicago'])
					->emailFormat('both')
					->to($to)
					->subject('Faizchicago - Request for Fateha/Salwaat ')
					->send($msg);
					 
			}
			else 
				$this->Flash->error(__('There is the problem while sending Fateha/Salwaat request.'));
		}	
		     
	}
	
	public function updateDeliveryStatus() {
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			 
			$this->loadModel('ThaaliDelivery');
			$this->loadModel('UserThaaliInfo');
			$id			= $this->request->data['id'];
			 
			$query = $this->ThaaliDelivery->query();
			$query->update()
			->set(['order_status' => '1'] )
			->where(['id ' => $id])
			->execute();
	       
			//sending sms to user
			// Get User Id by Thaali-delivery ID
			$userDetails = $this->ThaaliDelivery->find()->where(['id ' => $id])->first();
			$userId = $userDetails->user_id;
			
			$userThaaliinfo = $this->UserThaaliInfo->find()->where(['user_id ' => $userId])->first();
			$delivery_sms_notification =  $userThaaliinfo->delivery_sms_notification;
			
			if ($delivery_sms_notification == 1){
				$users = $this->Users->find()->where(['id ' => $userId])->first();
			    $mobileNo = $users->mobile_phone;
			    $mobileNo = str_replace('-','',$mobileNo);
			    $mobileNo = str_replace('(','',$mobileNo);
			    $mobileNo = str_replace(')','',$mobileNo);
			  
			    if ($mobileNo != '') {
			    	$mobileNo = TWILIO_COUNTRY_CODE.$mobileNo;
			 		// Use the REST API Client to make requests to the Twilio REST API
					// Your Account SID and Auth Token from twilio.com/console
					$sid = TWILIO_SID;
					$token = TWILIO_TOKENS;
					$client = new Client($sid, $token);
			        $tdate = date('m/d/y');
					// Use the client to do fun stuff like send text messages!
					$client->messages->create(
						// the number you'd like to send the message to
						$mobileNo,
						array(
							// A Twilio phone number you purchased at twilio.com/console
							'from' => TWILIO_PHONE_NO,
							// the body of the text message you'd like to send
							'body' => 'Your '.$tdate. ' Thaali has been delivered - FaizChicago'
							)
				   );
				}
		 	}
		$this->autoRender = false;
	}
	exit;
 }	
 

 
  
 public function getThaaliMenu() {
 	$this->viewBuilder()->layout('empty');
 	//windows
 	 
 	$weekstart = $this->request->data['startDate'];
 	 
 	$weekend = strtotime($weekstart);
 	$weekend =  strtotime('+4 day', $weekend);
 	$weekend = date('y-m-d', $weekend);
 	
 	$start = strtotime($weekstart);
 	$end   = strtotime('+4 day', $start);
 	$start = date('m/d/y', $start); 
 	
 	$end =  date('m/d/y', $end);
 	 
 	
 	$nextWeek = strtotime($weekstart);
 	$nextWeek =  strtotime('+7 day', $nextWeek);
 	$nextWeek = date('y-m-d', $nextWeek);
 	
 	$prevWeek = strtotime($weekstart);
 	$prevWeek =  strtotime('-7 day', $prevWeek);
 	$prevWeek = date('y-m-d', $prevWeek);
 	
  
 	 
 	$this->set('weekstart', $weekstart);
 	$this->set('weekend', $weekend);
 	$this->set('nextWeek', $nextWeek);
 	$this->set('prevWeek', $prevWeek);
 	$this->set('start', $start);
 	$this->set('end', $end);
 	 
 	 
 	$thisWeekCalendar = $this->__getThisWeekCalandar($weekstart, $weekend);
 	$this->set('thisWeekCalendar', $thisWeekCalendar);
 	
 }
 
 public function __updateUserDriverMapping ($userId, $msg, $driverId ) {
 	 
 	$this->loadModel('UserDriverMapping');
 	 
 	$driverMapping = $this->UserDriverMapping->find('all', [
 			'conditions' => array( ' user_id' => $userId)
 	])->count();
 	 
 	if ($driverMapping == 1) {
 		 
 		$query = $this->UserDriverMapping->query();
 		$query->update()
 		->set(['driver_id' => $driverId])
 		->where(['user_id' => $userId ])
 		->execute();
 		 
 	}
 	else
 	{
 		$userDriver = $this->UserDriverMapping->newEntity();
 		$userDriver->user_id = $userId;
 		$userDriver->driver_id = $driverId;
 		$this->UserDriverMapping->save($userDriver);
 	}
 	
 	$user = $this->Users->get($driverId);
 	$to    = $user->email_address;
 	$sub = 'Faizchicago - Delivery Notification';
 	
 	$email = new Email('default');
 	$email->from([EMAIL_SENDER => 'Faizchicago'])
 	->emailFormat('both')
 	->to($to)
 	->subject($sub)
 	->send($msg);
 	 
 }
}