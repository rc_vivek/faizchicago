<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ThaaliDelivery Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $driver_id
 * @property int $thaali_id
 * @property string $thaali_size
 * @property int $distribution_id
 * @property \Cake\I18n\Time $delivery_date
 * @property string $delivery_type
 * @property string $delivery_notes
 * @property string $order_status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Thaali $thaali
 * @property \App\Model\Entity\DistributionCenter $distribution_center
 */
class ThaaliDelivery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
