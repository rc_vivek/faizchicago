<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DriverInfo Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DriverInfo get($primaryKey, $options = [])
 * @method \App\Model\Entity\DriverInfo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DriverInfo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DriverInfo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DriverInfo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DriverInfo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DriverInfo findOrCreate($search, callable $callback = null)
 */
class DriverInfoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('driver_info');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('insurance_provider', 'create')
            ->notEmpty('insurance_provider');

        $validator
            ->requirePresence('insurance_policy_no', 'create')
            ->notEmpty('insurance_policy_no');

        $validator
            ->date('insurance_expiry_date', ['ymd'])
            ->requirePresence('insurance_expiry_date', 'create')
            ->notEmpty('insurance_expiry_date');

        $validator
            ->requirePresence('license_number', 'create')
            ->notEmpty('license_number');

        $validator
            ->date('license_expiry_date', ['ymd'])
            ->requirePresence('license_expiry_date', 'create')
            ->notEmpty('license_expiry_date');

        $validator
            ->requirePresence('vehicle_make', 'create')
            ->notEmpty('vehicle_make');

        $validator
            ->requirePresence('vehicle_model', 'create')
            ->notEmpty('vehicle_model');

        $validator
            ->date('vehicle_year')
            ->requirePresence('vehicle_year', 'create')
            ->notEmpty('vehicle_year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
