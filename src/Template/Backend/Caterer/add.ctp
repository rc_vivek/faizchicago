<?php $this->assign('title', 'Add Caterer');?> 
	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Caterer', 'action' => 'index']);?>
    </div>
            </div><!-- /.box-header -->  
		<div class="">
         
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($caterer, ['id' => 'caterer', 'name' => 'caterer']) ?>
							    <fieldset>
							        <legend><?= __('Add Caterer') ?></legend>
							         <?php
							            echo $this->Form->input('name', [ 'class'=> 'form-control']);
							            echo $this->Form->input('email_address', [ 'class'=> 'form-control']);
							         ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 <script>
  $(document).ready(function() {
	  $("#caterer").validate({
	     rules: {
	         	name: {required:true},
	         	email_address: {required:true, email:true}
       },
        messages: {
        			name: { 
  			 				required: "Caterer Name is required."
  						  },
       email_address : {required: "Email Address is required."}
  		 }
	}); 
});
</script>
  