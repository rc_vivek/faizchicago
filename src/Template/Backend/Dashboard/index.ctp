<?php $this->assign('title', 'Dashboard');?>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
               <div class="box-body table-responsive">
                
		          <div class="col-lg-12">
			      <h2 class="text-left" style="font-size: 18px; font-weight: 600;"><?= __('Notifications') ?></h2>
			      <ul class="nav nav-tabs tab-active">
			        <li class="active">
			           <a data-toggle="tab" href="#home">Pending Users <?= ($recentUserCnt> 0) ? "(".$recentUserCnt.")":""; ?> </a>
			       </li>
			        <!--<li><a data-toggle="tab" href="#menu1">Thaali size change request</a></li>-->
			      </ul>
			      <div class="tab-content">
			        <div id="home" class="tab-pane fade in active tab-space">
			        <div class="table-responsive">
			          <table class="table edit-table">
			            <tbody>
			              <tr>
			                <th scope="col">Name</th>
			                <th scope="col">Ejamaat ID</th>
			                <th scope="col">Email </th>
			                <th scope="col">Registered On</th>
			                <th scope="col">Action</th>
			              </tr>
			              <?php foreach ($recentUser as $user): ?>
			                 <tr>
			                 	<td><?= h($user->full_name) ?></td>
			                 	<td><?= $user->ejamaatid ?></td>
			                 	<td><?= $user->email_address ?></td>
			                 	<td><?= $user->created->format('m/d/Y') ?></td>
			                 	<td>
			                 		<ul class="action">
			                    		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View', 'aria-hidden' => 'true']), ['controller' => 'users', 'action' => 'view', $user->id], ['escape' => false]) ?></li>
					                	<!-- <li><?= $this->Form->postLink(
	                                                 $this->Html->tag('i', '', ['class' => 'fa fa-check-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Approve']). "",
	                                                 ['controller' =>'users', 'action' => 'enable', $user->id],
	  												 ['escape'=>false, 'confirm' => __('Are you sure you want to approve this user?', $user->id)],
	                                                 ['class' => 'btn btn-mini']); ?></li>-->
					                	<li><?= $this->Form->postLink(
	                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete']). "",
	                                                 ['action' => 'delete', $user->id],
	  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)],
	                                                 ['class' => 'btn btn-mini']); ?></li>
			                  		</ul>
			                 	</td>
			                 </tr>
			              <?php endforeach; ?>
			              </tbody>
			          </table>
			        </div>
			        </div>
			        
			      </div>
			    </div>
      	        <div class="col-lg-12 top-spacing">
					  <div class="row">
					 <div class="col-lg-4">
						  <h4 class="text-left">Thaali Counts</h4>
						  <table class="table block-border">
							<thead>
							  <tr class="skyblue-bg">
								<th align="center">
								<div class="input-group form-group" style="margin-bottom:1px;">
				  <span class="input-group-addon" id="basic-addon1">
						<i class="fa fa-calendar"></i>
					</span> 
					<input type='text' name='menu_date' id='menu-date' class='datepicker form-control' value=<?= $currDate;?> >
				</div>
								</th>
								<th></th>
							  </tr>
							</thead>
							<tbody id = 'thaali-count'>
							<?php $total = 0 ;?>
							<?php foreach ($thaaliSizes as $key => $value) { ?>
							  <tr>
								<td align="left"><?= $value ?></td>
								<td align="right">
								 
								<?= (array_key_exists($key,$todaysThaali)) ? $todaysThaali[$key]:'0'; ?>
								<?php $total += @$todaysThaali[$key];?>
								</td>
							  </tr>
							<?php } ?>  
							 <tr>
							 	<td align="left">Total</td>
							 	<td align="right"><?= $total;?></td>
							 </tr>
							</tbody>
						  </table>
						</div>
						<div class="col-lg-4">
						  <h4 class="text-left">Registered Users</h4>
						  <table class="table block-border">
							<thead>
							  <tr class="skyblue-bg">
								<th>Period</th>
								<th>Users</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td align="left">Last 7 Days </td>
								<td align="left"><?= $thisWeekUser ?> </td>
							  </tr>
							  <tr>
								<td align="left">Last 14 Days</td>
								<td align="left"><?= $lastTwoWeeksUser ?></td>
							  </tr>
							  <tr>
								<td align="left">Last Month</td>
								<td align="left"><?= $lastMonthUser ?></td>
							  </tr>
							  <tr>
								<td align="left">All time</td>
								<td align="left"><?= $allUsers?></td>
							  </tr>
							  <tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
							  </tr>
							</tbody>
						  </table>
						</div>
						<div class="col-lg-4">
						  <h4 class="text-left">Delivered Household Tiffins</h4>
						  <table class="table block-border">
							<thead>
							  <tr class="skyblue-bg">
								<th>Status</th>
								<th>Household Tiffins</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td align="left">Today</td>
								<td align="left"><?= $todaysDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Week</td>
								<td align="left"><?= $thisWeekDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Month</td>
								<td align="left"><?= $thisMonthDelivery ?></td>
							  </tr>
							  <tr>
								<td align="left">This Year</td>
								<td align="left"><?= $thisYearDelivery ?></td>
							  </tr>
							   <tr>
								<td align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
							  </tr>
							</tbody>
						  </table>
						</div>
						 
						</div>
					</div>
  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
 
     
 <script>  
     $(document).ready(function() {
   	 $('#allUsers, #newUsers').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 6 }
        ],
        fixedColumns: true
   	 });
   	 
	
    
     $(function() {
     			
     				var dates = $("#menu-date").datepicker({
                                //           minDate:'-0y',
                                       // 	maxDate:'+0d',
                                       dateFormat: 'yy-mm-dd',
     					defaultDate: "+1w",
     					changeMonth: true,
     					numberOfMonths: 1,
     					onSelect: function( selectedDate ) {
     						var option = this.id == "menu-date" ? "minDate" : "maxDate",
     							instance = $( this ).data( "datepicker" ),
     							date = $.datepicker.parseDate(
     								instance.settings.dateFormat ||
     								$.datepicker._defaults.dateFormat,
     								selectedDate, instance.settings );
     						dates.not( this ).datepicker( "option", option, date );
     					}
     					
     				});
     			});
		
       $("#menu-date").datepicker({
	  onSelect: function(dateText) {
		 // var tdate = this.value;
		  //var tdate1 = tdate.replace(///g , "$$");
	    $.ajax({
		    type : 'POST',
		    url : '<?php echo $this->Url->build(array('controller'=>'dashboard','action'=>'getTodaysThaaliSizeByCount','_full' => true )); ?>',
			data : {"tdate":this.value},
		     success: function(response) {  
		    	 $('#thaali-count').html(response);
	    },
	      
		});
	  }
	});
     } );
      
</script>	
	