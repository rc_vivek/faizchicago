<?php $this->assign('title', 'Add Driver Replacement');?>
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'deliveries', 'action' => 'index']);?>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
						<legend><?= __('Add Driver Replacement') ?></legend>
						 <div class="alert alert-info">
          				  <?php echo "<strong>Info!</strong> Driver replacement will be effective from the selected date onwards.";?> 
          				 </div>
							  <?= $this->Form->create($driverReplacement, ['id' => 'driverReplacement', 'name' => 'driverReplacement']) ?>
							    <fieldset>
							         <?php
									      
									            echo $this->Form->input('check_permenant',['type'=>'checkbox','value'=>'1','label'=>'Replace Driver Permenantly','onchange'=>'disableToDate()','format' => array('before', 'input', 'between', 'label', 'after', 'error')]); 
											    echo $this->Form->input('driver_id', ['options' => $us, 'empty'=>'Select Driver', 'class'=> 'form-control']);
									            echo $this->Form->input('replace_driver_id', ['options' => $us, 'empty'=>'Select Driver', 'class'=> 'form-control']);
												echo $this->Form->input('from_date', ['class'=>'datepicker form-control','type'=> 'text']);
									            echo "<div id='chk'>".$this->Form->input('to_date', ['class'=>'datepicker form-control' ,'type'=> 'text'])."</div>";
												echo $this->Form->input('status', ['type' => 'hidden', 'value'=>'1', 'class'=> 'form-control']);
									        ?>
								 </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 <script>
  
  $(function() {
			
				var dates = $("#from-date, #to-date").datepicker({
                                        minDate:'-0y',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					//defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "from-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#driverReplacement").validate({
	     rules: {
	    	 driver_id: {required:true, },
	    	 replace_driver_id: {required:true, notEqual: "#driver-id"},
         	 start_date: {required:true,date: true},
         	 end_date: {
             	 required: function (element) {
                     if($("#check-permenant").is(':checked')){
                         var e = document.getElementById("myDropDown");
                         return e.options[e.selectedIndex].value=="" ;                            
                     }
                     else
                     {
                         return false;
                     }  
                  }  ,
             	  date:true}  
        },
        messages: {
        	driver_id: {required: "Driver Name field is required."},
  			replace_driver_id: { required: "Replaced Driver field is required."}, 
  			from_date : { required: "From date field is required."}, 
  			to_date : {required: "To date field is required."} 
		}
	}); 
});
</script>
  <script type="text/javascript">
function disableToDate()
{
    if($('#check-permenant').is(":checked"))   
        $("#to_date_id, #chk").hide();
    else
         $("#to_date_id, #chk").show();
}
</script>