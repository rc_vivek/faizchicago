	<section class="content">
	<div class="row">
	    <div class="col-lg-12">
		         <div class="row">
		          		<div class="col-lg-6">
		           			<h4 class="text-left">Distribution Center Calendar</h4>
		          		</div>
		          		<div class="col-lg-6">
		           			 
		          		</div>
	              	</div>
		              <div class="box">
			              <div class="box-body table-responsive">
							<table id="distributionCalendar" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="skyblue-bg">
										<th><?= 'day'?></th>
										<th><?= 'Distribution Center'?></th>
									</tr>
								</thead>					
								<tbody>
								 
										<tr>
											<td>Sunday 05/01/2016</td>
											<td>Masjid (139)
												Burhanuddin bhai Badri's residence (4)
												Shabbir bhai Talib's residence (1)
											</td>
										</tr>
										 <tr>
										    <td>Monday 06/01/2016</td>
											<td>Masjid (139)
												Burhanuddin bhai Badri's residence (4)
												Shabbir bhai Talib's residence (1)
										    </td>
										 </tr>
									 
							   </tbody>
							</table>
						</div>
					</div>
				</div>	
			 
	</div>

</section><!-- /.content -->
  
  <section class="content">
	<div class="row">
	    <div class="col-lg-12">
		      <div class="row">
		          <div class="col-lg-12">
		          	<div class="row">
	          		<div class="col-lg-6">
	           			<h4 class="text-left">Manage Distribution Centers</h4>
	          		</div>
	          	 
	          		<div class="col-lg-6 col-md-6 col-xs-6">
            		<a class="btn btn-primary center-block pull-right" data-placement='top' title= 'Manage Distribution Center' data-toggle="tooltip"  href="/backend/distribution_replacement">
            			<i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Replace Distribution Center</span></a>
            			<a class="btn btn-primary center-block pull-right" data-placement='top' title= 'Add New Distribution Center' data-toggle="tooltip"  href="/backend/distribution_center/add">
            			<i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add Distribution Center</span></a>
            			
            			
        			</div>
        			
	              </div>
		              <div class="box">
			              <div class="box-body table-responsive">
							<table id="allDistribution" class="table table-striped table-bordered" width="100%">
								<thead>
									<tr class="skyblue-bg">
										<th><?= 'name' ?></th>
										<th><?= 'phone' ?></th>
										<th><?= 'address' ?></th>
										<th class="actions"><?= __('Actions') ?></th>
									</tr>
								</thead>					
								<tbody>
									<?php foreach ($distributionCenter as $distributionCenter): ?>
										<tr>
											<!--<td><?= $this->Number->format($distributionCenter->id) ?></td>-->
											<td><?= h($distributionCenter->name) ?></td>
											<td><?= h($distributionCenter->phone) ?></td>
											<td><?= h($distributionCenter->address) ?></td>
											<td class="actions">
											<ul class="action">
					                    		<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'View','aria-hidden' => 'true']), ['controller' => 'distribution-center', 'action' => 'view', $distributionCenter->id], ['escape' => false]) ?></li>
							                	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit', $distributionCenter->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $distributionCenter->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $distributionCenter->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                  				</ul>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>	
				
			</div>
		</div>
	</div>
  </div>
</section><!-- /.content -->
  
      
 <script>  
     $(document).ready(function() {
    
   	 
   	  $('#allDistribution').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        "aoColumnDefs": [
         { "sWidth": "20%",  "aTargets": [0]},
         { "sWidth": "15%", "aTargets": [1]},
          { "sWidth": "50%", "aTargets": [2]},
           { "sWidth": "15%", "aTargets": [3]},
          ],
        "bSort" : false, 
         "lengthMenu": [[7, 14, 21, 28 -1], [7, 14, 21, 28, "All"]]
   	 });
	} );
</script>
 