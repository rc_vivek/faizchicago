  <?php   
     $sdate = date('Y-m-d', strtotime($distributionReplacement->from_date)); 
     $edate = date('Y-m-d', strtotime($distributionReplacement->to_date)); 
   ?>
   
   <div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/distribution_replacement/index" class="btn btn-primary" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							   <?= $this->Form->create($distributionReplacement, ['id' => 'distributionReplacement', 'name' => 'distributionReplacement']) ?> 
							    <fieldset>
							        <legend><?= __('Edit Distribution Replacement') ?></legend>
							         <?php
							            echo $this->Form->input('distribution_id', [ 'options' => $distributionCenter, 'class'=> 'form-control']);
							            echo $this->Form->input('replace_distribution_id', [ 'options' => $distributionCenter, 'class'=> 'form-control']);
							            echo $this->Form->input('from_date', ['value' => $sdate ,'class'=>'datepicker','type'=> 'text']);
							            echo $this->Form->input('to_date', ['value' => $edate ,'class'=>'datepicker','type'=> 'text']);
							            echo '</div>';
	       										$attributes['value'] =   $distributionReplacement->status; 
	       										echo "<div class='input'>";
	       										echo $this->Form->label('distributionReplacement.Status');
	       										echo "&nbsp;&nbsp;";
								            	echo $this->Form->radio('status', [ 
									            ['value' => '0', 'text' => 'Inactive', 'class'=> ''],
		       									['value' => '1', 'text' => 'Active', 'class'=> '']
	       									    ], $attributes);
	       									    echo '</div>'; 
							        ?>
								 </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 <script>
 
 $(function() {
			
				var dates = $("#from-date,#to-date").datepicker({
                                        minDate:'-0y',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "from-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			}); 
    $(document).ready(function() {
	  $("#distributionReplacement").validate({
	     rules: {
	         	distribution_id: {required:true, notEqual:"#replace-distribution-id"},
         		replace_distribution_id: {required:true, notEqual:"#distribution-id"},
         		from_date: {required:true,date: true},
         		to_date: {required:true, date:true}
        },
        messages: {
        			distribution_id: { 
  			 				required: "Distribution center is required."
  						  },
  					replace_distribution_id: { 
  			 				required: "Replace distribution center is required." 
  						  }, 
  					from_date : { 
  			 				required: "From date is required."  
  						  }, 
  					to_date : { 
  			 				required: "To date is required." 
  						  } 
		}
	}); 
});
</script>
  
