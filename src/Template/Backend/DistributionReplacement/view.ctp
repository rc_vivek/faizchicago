 <div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/distribution_replacement/index" class="btn btn-primary"  data-placement='top' title= 'Back' data-toggle="tooltip"  id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
</div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		  <h3> <?= __('View Distribution Replacment') ?></h3>
		   <table id="distributionReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
		       <tr>
		            <th><?= __('Id') ?></th>
		            <td><?= $this->Number->format($distributionReplacement->id) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Distribution Id') ?></th>
		            <td><?= $distributionReplacement->distribution_center->name?></td>
		        </tr>
		        <tr>
		            <th><?= __('Replace Distribution Id') ?></th>
		            <td><?= $distributionReplacement->distribution_center1->name ?></td>
		        </tr>
		        <tr>
		            <th><?= __('From Date') ?></th>
		            <td><?= h($distributionReplacement->from_date->format('l Y/m/d')) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('To Date') ?></th>
		            <td><?= h($distributionReplacement->to_date->format('l Y/m/d')) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Created') ?></th>
		            <td><?= h($distributionReplacement->created->format('l Y/m/d')) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Modified') ?></th>
		            <td><?= h($distributionReplacement->modified->format('l Y/m/d')) ?></td>
		        </tr>
		        <tr>
						            <th><?= __('Status') ?></th>
						            <td><?php switch ($distributionReplacement->status) {
			          							case '0': echo 'Inactive';break;
			          							case '1': echo 'Active'; break;
			          							 
			          					}
			         				 ?></td>
			  </tr>
						       
		    </table>
		</div>
	</div>
</div>

  