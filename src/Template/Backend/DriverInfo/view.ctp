<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Driver info') ?></h3>
		   <table id="driverInfo" class="table table-striped table-bordered" cellspacing="0" width="100%">
		       <tr>
		            <th><?= __('User') ?></th>
		            <td><?= $driverInfo->has('user') ? $this->Html->link($driverInfo->user->id, ['controller' => 'Users', 'action' => 'view', $driverInfo->user->id]) : '' ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Insurance Provider') ?></th>
		            <td><?= h($driverInfo->insurance_provider) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Insurance Policy No') ?></th>
		            <td><?= h($driverInfo->insurance_policy_no) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('License Number') ?></th>
		            <td><?= h($driverInfo->license_number) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Vehicle Make') ?></th>
		            <td><?= h($driverInfo->vehicle_make) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Vehicle Model') ?></th>
		            <td><?= h($driverInfo->vehicle_model) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Id') ?></th>
		            <td><?= $this->Number->format($driverInfo->id) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Insurance Expiry Date') ?></th>
		            <td><?= h($driverInfo->insurance_expiry_date) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('License Expiry Date') ?></th>
		            <td><?= h($driverInfo->license_expiry_date) ?></td>
		        </tr>
		        <tr>
		            <th><?= __('Vehicle Year') ?></th>
		            <td><?= h($driverInfo->vehicle_year) ?></td>
		        </tr>
		    </table>
		</div>
	</div>
</div>	
  
 