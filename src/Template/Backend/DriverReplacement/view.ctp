<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
			     <h3> <?= __('View Driver Replacement') ?></h3>
			   <table id="driverReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <tr>
			            <th><?= __('User') ?></th>
			            <td><?= $driverReplacement->has('user') ? $this->Html->link($driverReplacement->user->id, ['controller' => 'Users', 'action' => 'view', $driverReplacement->user->id]) : '' ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Id') ?></th>
			            <td><?= $this->Number->format($driverReplacement->id) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Driver Id') ?></th>
			            <td><?= $this->Number->format($driverReplacement->driver_id) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('From Date') ?></th>
			            <td><?= h($driverReplacement->from_date) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('To Date') ?></th>
			            <td><?= h($driverReplacement->to_date) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Created') ?></th>
			            <td><?= h($driverReplacement->created) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Modified') ?></th>
			            <td><?= h($driverReplacement->modified) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Status') ?></th>
			            <td><?= $this->Text->autoParagraph(h($driverReplacement->status));?></td>
			        </tr>
			    </table>
			</div>
		</div>
	</div?		

  
 
  