<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fateha->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fateha->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fateha'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fateha form large-9 medium-8 columns content">
    <?= $this->Form->create($fateha) ?>
    <fieldset>
        <legend><?= __('Edit Fateha') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('items');
            echo $this->Form->input('fateha_names');
            echo $this->Form->input('fateha_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
