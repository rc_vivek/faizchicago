 <?php $this->assign('title', 'Fateha/Salwaat');?> 
<!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
	              <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">All  Fateha/Salwaat</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			        </div>
			        </div>
	                   
	                </div><!-- /.box-header --> 
              	<div class="box-body table-responsive">
                	 <table id="allfateha" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr  class="skyblue-bg">
				            	<th><?= 'Customer Name' ?></th>
								<th><?= 'Fateha/Salwaat Date' ?></th>
								<th><?= 'Item' ?></th>
								<th><?= 'Fateha/Salwaat Names' ?></th>
								<th><?= 'Created' ?></th>
								<th><?= 'Actions' ?></th>
				             </tr>
			        	</thead>
			        <tbody>
					 <?php foreach ($fatehaList as $fateha): ?>
			            <tr>
			            	<td><?= $fateha->user->full_name ?></td>
			                <td><?= $fateha->fateha_date->format('l m/d/Y') ?></td>
			                <td><?= ($fateha->items == 'f'? 'Fruits':'Sweets')?></td>
			                <td><?= $fateha->fateha_names ?></td>
			                <td><?= $fateha->created->format('m/d/Y') ?></td>
			                
			                <td class="actions">
			                    <ul class="action">
			                    	<!-- <li><?php //$this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit',  $fateha->id], ['escape' => false]) ?></li>-->
			                    	 <li><?php echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'view',  $fateha->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $fateha->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?',  $fateha->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                    </ul>                             
			                                                 
			                </td>
			            </tr>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  
      
   

  <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allfateha').DataTable({
	   	 "bAutoWidth": false , 
	   	"bSortable": false,
         "aoColumnDefs": [
         { "sWidth": "20%",  "aTargets": [0], "bVisible": true,"bSortable": false,},
         { "sWidth": "20%", "aTargets": [1],"bSortable": false,},
         { "sWidth": "10%", "aTargets": [2],"bSortable": false,},
         { "sWidth": "25%", "aTargets": [3],"bSortable": false,},
         { "sWidth": "10%", "aTargets": [4],"bSortable": false,},
         { "sWidth": "10%", "aTargets": [5],"bSortable": false,},
        ]
	   	 });
	} );
</script>
   