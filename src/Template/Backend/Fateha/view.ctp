 <?php $this->assign('title', 'View Fateha/Salwaat');?> 
<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
    	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'fateha', 'action' => 'index']);?>
	</div>
</div><!--  box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
		    <h3> <?= __('View Fateha/Salwaat') ?></h3> 
		    <table id="miqat" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <tr>
					 <th><?= __('Name') ?></th>
					 <td><?=  $fateha->has('user') ? $this->Html->link($fateha->user->full_name, ['controller' => 'Users', 'action' => 'view', $fateha->user->id]) : ''  ?></td>
				</tr> 
				 
			    <tr>
			        <th><?= __('Fateha Date') ?></th>
			        <td><?= h($fateha->fateha_date->format('l Y/m/d')) ?></td>
			    </tr>
			     <tr>
			        <th><?= __('Fateha Items') ?></th>
			        <td><?=  ($fateha->items == 'f'? 'Fruits':'Sweets')?></td>
			    </tr>
			     <tr>
			        <th><?= __('Fateha Names') ?></th>
			        <td><?= $this->Text->autoParagraph(h($fateha->fateha_names)); ?></td>
			    </tr>
			  <tr>
			        <th><?= __('Created') ?></th>
			        <td><?= h($fateha->created->format('l Y/m/d')) ?></td>
			    </tr>
			</table>					           
		</div>
	</div>
</div>
  
 
 