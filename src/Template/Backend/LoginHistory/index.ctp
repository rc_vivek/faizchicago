<div class="container">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4 col-md-12 col-xs-12">
           <h2><?= __('Login History') ?></h2>
        </div>
        <div class="col-lg-8 col-md-12 col-xs-12">
           <!-- <a class="btn btn-primary center-block pull-right" data-toggle="tooltip" href="add">
            <i aria-hidden="true" class="fa fa-plus tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Add New User</span></a>-->
              <?= $this->Html->link(__('List Caterer'), ['controller' => 'Caterer', 'action' => 'index']) ?>
              <?= $this->Html->link(__('New Thaali'), ['action' => 'add']) ?>
        </div>
      </div>
    </div>
  </div>
      <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                   
                </div><!-- /.box-header --> 
                
                <div class="box-body table-responsive">
                	 <table id="allLoginHistory" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr>
				                <th><?= 'id' ?></th>
				                <th><?= 'user id' ?></th>
				                <th><?= 'created' ?></th>
				                <th><?= 'ip address' ?></th>
				                <th><?= 'user agent' ?></th>
				                <th class="actions"><?= __('Actions') ?></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php foreach ($loginHistory as $loginHistory): ?>
				            <tr>
				                <td><?= $this->Number->format($loginHistory->id) ?></td>
				                <td><?= $loginHistory->has('user') ? $this->Html->link($loginHistory->user->id, ['controller' => 'Users', 'action' => 'view', $loginHistory->user->id]) : '' ?></td>
				                <td><?= h($loginHistory->created) ?></td>
				                <td><?= h($loginHistory->ip_address) ?></td>
				                <td><?= $this->Number->format($loginHistory->user_agent) ?></td>
				                <td class="actions">
				                    <?= $this->Html->link(__('View'), ['action' => 'view', $loginHistory->id]) ?>
				                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $loginHistory->id]) ?>
				                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $loginHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loginHistory->id)]) ?>
				                </td>
				            </tr>
				            <?php endforeach; ?>
				        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

 <script>  
     $(document).ready(function() {
   	 $('#allLoginHistory').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 5 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</
	 