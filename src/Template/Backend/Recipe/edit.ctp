<?php $this->assign('title', 'Edit Recipe');?>
  <div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                
    </div>
            </div><!-- /.box-header -->  
		<div class="">
         
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							 <?= $this->Form->create($recipe) ?>
							    <fieldset>
							        <legend><?= __('Edit Recipe') ?></legend>
							         <?php  echo $this->Form->input('recipe_name', [ 'class'=> 'form-control']);  ?>
							         <?php  echo $this->Form->input('recipe_description', [ 'class'=> 'form-control']);  ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 <script>
  $(document).ready(function() {
	  $("#recipe").validate({
	     rules: {
	    	 recipe_name: {required:true},
	    	 recipe_description: {required:true}
       },
        messages: {
        	recipe_name: {  required: "Recipe Name is required."  },
  			recipe_description: { required: "Recipe Description is required." },			  
  		 }
	}); 
});
</script>
   
   