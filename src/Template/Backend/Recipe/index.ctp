<?php $this->assign('title', 'Recipe List');?> 
<!-- Main content -->
        <section class="content">
         <div class="row">
	                 
			         <div class="col-lg-2 col-md-6 col-xs-6 pull-right btn-group">
			           <?= $this->Html->link('Add Recipe', ['controller' => 'recipe', 'action' => 'add'], ['class' => 'center-block pull-right']);?>
			           <?= $this->Html->link('Send Recipe', ['controller' => 'recipe', 'action' => 'sendRecipe'], ['class' => 'center-block pull-left']);?>
			        </div>
		 </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
	              <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                 <h2 class="text-left" style="font-size: 18px; font-weight: 600;">All Recipes</h2>
	       			</div>
			        <div class="col-lg-4 col-md-12 col-xs-12 pull-right">
			        </div>
			        </div>
	                   
	                </div><!-- /.box-header --> 
              	<div class="box-body table-responsive">
                	 <table id="allRecipes" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
				            <tr  class="skyblue-bg">
				            	<th><?= 'id' ?></th>
								<th><?= 'Name' ?></th>
								<th><?= 'Description' ?></th>
				                <th><?= __('Actions') ?></th>
				            </tr>
				            </tr>
			        	</thead>
			        <tbody>
					 <?php foreach ($recipesList as $recipe): ?>
			            <tr>
			            	<td><?= $this->Number->format($recipe->id) ?></td>
			                <td><?= ucwords($recipe->recipe_name) ?></td>
			                 <td><?= $recipe->recipe_description ?></td>
			                <td class="actions">
			                    <ul class="action">
			                    	<li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil-square-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Edit', 'aria-hidden' => 'true']), ['action' => 'edit',  $recipe->id], ['escape' => false]) ?></li>
				                	     		<li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $recipe->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?',  $recipe->id)],
			                                                 ['class' => 'btn btn-mini']); ?></li>
			                    </ul>                             
			                                                 
			                </td>
			            </tr>
			            <?php endforeach; ?>
			        </tbody>
			    </table>
   			</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  
      
   

  <script>  
     $(document).ready(function() {
   	 
   	 
   	  $('#allRecipes').DataTable({
	   	 "bAutoWidth": false , 
	   	  "order": [[ 1, "asc" ]],
         "aoColumnDefs": [
         { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
         { "sWidth": "20%", "aTargets": [1]},
         { "sWidth": "70%", "aTargets": [2]},
         { "sWidth": "10%", "aTargets": [3], "bSearchable": false, "bSortable": false},
        ]
	   	 });
	} );
</script>
  
   