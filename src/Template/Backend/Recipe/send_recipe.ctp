<?php $this->assign('title', 'Send Recipe to Caterer');?>
	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'recipe', 'action' => 'index']);?>
    </div>
            </div><!-- /.box-header -->  
		<div class="">
         
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create('recipe', ['id' => 'recipe', 'name' => 'recipe']) ?>
							    <fieldset>
							        <legend><?= __('Send Recipe') ?></legend>
							         <?php   
							         echo $this->Form->input('recipe_name', ['options' => $recipe, 'class'=> 'form-control', 'multiple' => 'multiple']); 
							          echo $this->Form->input('caterer_id', ['options' => $caterer, 'class'=> 'form-control']); ?>
							    </fieldset>
							    <?= $this->Form->button(__('Send Recipe')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 <script>
  $(document).ready(function() {
	  $("#recipe").validate({
	     rules: {
	    	 "recipe_name[]": {required:true},
	    	 caterer_id: {required:true}
       },
        messages: {
        	"recipe_name[]": {  required: "Please select at least one Recipe Name." },
        	caterer_id: { required: "Caterer Name is required." },			  
  		 }
	}); 
});
</script>
   
   
