<?php
/**
 * This help you you view the report and download the file
 * @returns App\Controller\Backend\ReportController.php
 * 
 */ 
?>
<?php $this->assign('title', 'Contact Us Feeds Report');?>
<section class="content-header">
	<h2 class="text-left" style="font-size: 18px; font-weight: 600;">Contact Us Feeds Report</h2>
</section>
<section class="content">
	 <div class="box">
			<div class="row">
				<div class="col-xs-12">
					<div class="Tpreview-box" style="padding:9px;">
						<form name="contactfeeds" id="contactfeeds" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'report','action'=>'contactfeeds','_full' => true )); ?>" method="POST">
							<div class="input-group form-group">
								<span class="input-group-addon" id="basic-addon1">
								<i class="fa fa-calendar"></i>
								</span> 
								<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
							</div>
							<div class="input-group form-group">
								<span class="input-group-addon" id="basic-addon1">
								<i	class="fa fa-calendar"></i>
								</span>
								<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
							</div>				
							<div class="form-group">
								<label class="" for="">&nbsp;</label>
								<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
							</div>
						</form>
					</div>
				</div>
			</div>		
		</div>	
		<?php
		/**
		* Display User details based on the Selected date filters
		*/ 
		?>
		<div class="box">
			<div class="row">
				<div class="col-xs-12">
						<div class="box-body table-responsive">
						<table id="feeds" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr class="skyblue-bg">
									<th>Id</th>
									<th>Date</th>
									<th>Full Name</th>
									<th>Message</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($contactFeeds as $user):?>
								<tr>
									<td><?= h($user['id']) ?></td>
									<td><?= h($user['created']->format('m/d/Y')) ?></td>
									<td><?= h(ucwords($user ['Users'] ['first_name']." ".$user ['Users'] ['middle_name']." ".$user ['Users'] ['last_name'])) ?></td>
									<td><?= h(ucwords($user['message'])) ?></td>
								</tr>
								<?php endforeach;?>
							</tbody>
						</table>
				</div>
			</div><!-- /.col -->
		</div><!-- /.row-->
	</div> <!-- /.box-->
</section><!-- /.content -->
 
  
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});

	
	$(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		var pdfTitle = "Contact Feeds "+ fromDate +" To "+ toDate;
		
		//Date field validation start
		$("#contactfeeds").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"From date is required.",
				dtto:"To date is required."
			}
		}); 
		
		//ends "visible": false,
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#feeds').DataTable({
			"bAutoWidth": true ,
			"ordering": true,
			"dom": 'Bfrtip',
			"order": [[0, "desc"]],
			 "aoColumnDefs": [
			                  { "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false}
		], 
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Contact_Feeds".date("d-m-Y"); ?>',exportOptions:{columns: [1, 2, 3]}},
				
			],
		}); 
		
	});

	/*
	{extend: 'pdfHtml5',filename: '<?php echo "Contact_Feeds".date("d-m-Y"); ?>',
				 title: pdfTitle,
				 exportOptions:{columns: [1, 2, 3]}
				},
	*/
</script>