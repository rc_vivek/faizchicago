 
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<a href="/backend/role/index" class="btn btn-primary" data-placement='top' title= 'Back' data-toggle="tooltip" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
<div class="col-lg-8 col-md-8 col-xs-12">
	<div class="box box-primary col-md-offset-1">    
		<div class="box-body">
			    <h3> <?= __('View Role') ?></h3>
			    <table id="Role" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        
			        <tr>
			            <th><?= __('Id') ?></th>
			            <td><?= $this->Number->format($role->id) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Name') ?></th>
			            <td><?= h($role->name) ?></td>
			        </tr>
			        
			        <tr>
			            <th><?= __('Created') ?></th>
			            <td><?= h($role->created->format('l Y/m/d')) ?></td>
			        </tr>
			        <tr>
			            <th><?= __('Modified') ?></th>
			            <td><?= h($role->modified->format('l Y/m/d')) ?></td>
			        </tr>
			         <tr>
						            <th><?= __('Status') ?></th>
						            <td><?php switch ($role->status) {
			          						 
			          							case '1': echo 'Active'; break;
			          							case '0': echo 'Inactive';break;
			          					}
			         				 ?></td>
						        </tr>
			    </table>
			   
			     
			</div>
		</div>
	</div>
