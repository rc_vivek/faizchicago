<?php
/**
 *ALTER TABLE `thaali` ADD `thaali_note` VARCHAR(225) NOT NULL AFTER `menu_item`;
 *ALTER TABLE `thaali` ADD `thaali_state` INT(11) NOT NULL DEFAULT '0' COMMENT '\'0-scheduled\',\'1-delivered\',\'3-cancelled\'' AFTER `thaali_note`;
 *ALTER TABLE `thaali` CHANGE `thaali_note` `thaali_note` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
 */
?>
<?= $this->Html->css('fullcalendar.min.css') ?>
<?= $this->Html->script('moment.min.js') ?>
<?= $this->Html->script('fullcalendar.min.js') ?>
<?php $this->assign('title', 'Add Thali Note');?>
<section class="content">
   <div class="msg"></div>
	<div class="box">
			<div class="row">
			<div class="col-xs-12">
				<div class="box-header">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<h2 class="text-left" style="font-size: 18px; font-weight: 600;">Admin Thaali Notes</h2>
					</div>
				</div><!-- /.box-header --> 
			</div><!-- /.col -->
		</div><!-- /.row -->
	
		<div class="row">
			<div class="col-xs-12">
				<div class="col-lg-12 col-md-12 col-xs-12">
					<div id="events-calendar" class="jplist">
						<div id="calendar_thaali_notes"></div>
					</div>  
				 </div>
			</div>
		</div>
		<div class="row"><div class="col-xs-12"><div class="box-header"></div></div></div>
	</div>
</section> 
<div id="thaali_note_dialog" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">      
	 <form method="POST" id ="thaali_note_form" name="thaali_note_form">
			<div class='title'><b>Add Thaali Note</b> </div>
			<div class="modal-body">
				
				<input type="hidden" name="thaali_id" id="thaali_id" value=""/>
				<?= $this->Form->textarea('thaali_note', ['id'=>'thaali_note','rows' => '5', 'cols' => '30']);?>
			</div>
			<label id="thaali_note-error" class="error" for="thaali_note"></label>
			<div class="modal-footer">
				<button type="button" id="thaalinote_submit" class="btn btn-custom" >SUBMIT</button>
				<button type="button" class="btn btn-grey" data-dismiss="modal">CANCEL</button>
			</div>
    </form> 
    </div>

  </div>
</div>

 <script>
 if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	 var cView = 'listWeek';
 }
 else 
	 var cView = 'month';
 	 
	$(function() {
		$('#calendar_thaali_notes').fullCalendar({
			header: {
			left: 'title',
			center: '',
			right:'prev,next'
			},
			defaultView: cView,
			displayEventTime: false,
			defaultDate: '<?= date("Y-m-d");?>',
			editable: true,
		   eventRender: function (event, element)
			{
			   element.find('.fc-title').html(element.find('.fc-title').text());	
			   element.find('.fc-list-item-title').html(element.find('.fc-list-item-title').text());
			},
			eventLimit: true,
			events: {
				url : '<?php echo $this->Url->build(array('controller'=>'thaali','action'=>'getThaalicalendar','_full' => true )); ?>',
				type: 'POST', // Send post data
				error: function() {
					alert('There was an error while fetching events.');
				}
			},
			
		});
	});
	
	
	$("#thaali_note_form").validate({
        rules: {
			 thaali_note: {
				  required:true 
			 }
        },
        messages: {
            thaali_note:"Please enter the text to submit the form"
        }
    });

	
	 $("#thaalinote_submit").click(function( event ) {
		  $("#thaali_note").val();
		  if($("#thaali_note_form").valid()){ 
			  $("#thaalinote_submit").attr("data-dismiss","modal");
			  var formUrl = $(this).attr('action'); 
			  var post_data = $("#thaali_note_form").serialize();  
			  $.ajax({
					type : 'POST',
					url : '<?php echo $this->Url->build(array('controller'=>'thaali','action'=>'addThaaliNote','_full' => true )); ?>',
					data : post_data,
					success: function(response) {  
						$('#calendar_thaali_notes').fullCalendar( 'refetchEvents' );
						$(".msg").html("<div class='alert alert-success' role='alert'>Thaali note add successfully! </div>");
						 $("html, body").animate({ scrollTop: 0 }, "slow");
						$(".alert").delay(15000).fadeOut('slow');
				},
				error: function (xhr, textStatus, errorThrown)	{
					alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
				});
		  }
		});
	function addThaaliNote(id) {
		 $("#thaali_id").val();
		 $("#thaali_id").val(id);
		 $("#thaali_note").val('');
		 $("#thaali_note-error").text('');
	}
	function showThaaliNote(id,thaalinote) {
		 $("#thaali_id").val();
		 $("#thaali_note").val();
		 $("#thaali_id").val(id);
		 $("#thaali_note").val(thaalinote);
	}
</script>