<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Thaali Delivery'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Thaali'), ['controller' => 'Thaali', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Thaali'), ['controller' => 'Thaali', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Distribution Center'), ['controller' => 'DistributionCenter', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Distribution Center'), ['controller' => 'DistributionCenter', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="thaaliDelivery form large-9 medium-8 columns content">
    <?= $this->Form->create($thaaliDelivery) ?>
    <fieldset>
        <legend><?= __('Add Thaali Delivery') ?></legend>
        <?php
            echo $this->Form->input('user_id');
            echo $this->Form->input('driver_id', ['options' => $users]);
            echo $this->Form->input('thaali_id', ['options' => $thaali]);
            echo $this->Form->input('thaali_size');
            echo $this->Form->input('distribution_id', ['options' => $distributionCenter]);
            echo $this->Form->input('delivery_date');
            echo $this->Form->input('delivery_type');
            echo $this->Form->input('delivery_notes');
            echo $this->Form->input('order_status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
