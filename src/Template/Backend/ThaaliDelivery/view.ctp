<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Thaali Delivery'), ['action' => 'edit', $thaaliDelivery->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Thaali Delivery'), ['action' => 'delete', $thaaliDelivery->id], ['confirm' => __('Are you sure you want to delete # {0}?', $thaaliDelivery->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Thaali Delivery'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Thaali Delivery'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Thaali'), ['controller' => 'Thaali', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Thaali'), ['controller' => 'Thaali', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Distribution Center'), ['controller' => 'DistributionCenter', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Distribution Center'), ['controller' => 'DistributionCenter', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="thaaliDelivery view large-9 medium-8 columns content">
    <h3><?= h($thaaliDelivery->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $thaaliDelivery->has('user') ? $this->Html->link($thaaliDelivery->user->first_name, ['controller' => 'Users', 'action' => 'view', $thaaliDelivery->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Thaali') ?></th>
            <td><?= $thaaliDelivery->has('thaali') ? $this->Html->link($thaaliDelivery->thaali->name, ['controller' => 'Thaali', 'action' => 'view', $thaaliDelivery->thaali->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Distribution Center') ?></th>
            <td><?= $thaaliDelivery->has('distribution_center') ? $this->Html->link($thaaliDelivery->distribution_center->name, ['controller' => 'DistributionCenter', 'action' => 'view', $thaaliDelivery->distribution_center->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($thaaliDelivery->id) ?></td>
        </tr>
        <tr>
            <th><?= __('User Id') ?></th>
            <td><?= $this->Number->format($thaaliDelivery->user_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Delivery Date') ?></th>
            <td><?= h($thaaliDelivery->delivery_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($thaaliDelivery->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($thaaliDelivery->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Thaali Size') ?></h4>
        <?= $this->Text->autoParagraph(h($thaaliDelivery->thaali_size)); ?>
    </div>
    <div class="row">
        <h4><?= __('Delivery Type') ?></h4>
        <?= $this->Text->autoParagraph(h($thaaliDelivery->delivery_type)); ?>
    </div>
    <div class="row">
        <h4><?= __('Delivery Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($thaaliDelivery->delivery_notes)); ?>
    </div>
    <div class="row">
        <h4><?= __('Order Status') ?></h4>
        <?= $this->Text->autoParagraph(h($thaaliDelivery->order_status)); ?>
    </div>
</div>
