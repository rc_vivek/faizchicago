<?php $this->assign('title', 'Thaali Survey Report');?>
<section class="content-header">
	<h2 class="text-left" style="font-size: 18px; font-weight: 600;">Thaali Survey Report</h2>
</section>
<section class="content">
	 <div class="box">
			<div class="row">
				<div class="col-xs-12">
					<div class="Tpreview-box" style="padding:9px;">
						<form name="frmexplist" id="frmexplist" class="form-inline" action="<?php echo $this->Url->build(array('controller'=>'thaaliSurvey','action'=>'view','_full' => true )); ?>" method="POST">
			<div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i class="fa fa-calendar"></i>
				</span> 
				<?php echo $this->Form->input('dtfrom', ['class'=> 'form-control','placeholder'=>'From','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>				
		    <div class="input-group form-group">
				<span class="input-group-addon" id="basic-addon1">
					<i	class="fa fa-calendar"></i>
				</span>
				<?php echo $this->Form->input('dtto', ['class'=> 'form-control','placeholder'=>'To','aria-describedby'=>'basic-addon1','label' => false]);?>
			</div>
			<div class="form-group">
				<label class="" for="">&nbsp;</label>
					<button type="submit" class="btn btn-primary" id="viewbutton">View</button>
		    </div>

		</form>
					</div>
				</div>
			</div>		
		</div>	
		<?php
		/**
		* Display User details based on the Selected date filters
		*/ 
		?>
		<div class="box">
			<div class="row">
				<div class="col-xs-12">
						<div class="box-body table-responsive">
						<table id="allUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr class="skyblue-bg">
			    <th>Thaali Date</th>
				<th>User Name</th>
				<th>Thaali Date</th>
				<th>Menu Item</th>
				<th>How is the taste of the food?</th>
				<th>Are the spices?</th>
				<th>Is the oil?</th>
				<th>Average Rating</th>
				<th>Rating Created</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(count($survey)!= 0) {
					foreach ($survey as $surveyInfo): 
			?>
			 <tr>
			 <td><?php echo $surveyInfo['thaali_date'];?></td>
				<td><?= h(ucwords($surveyInfo['user_name'])) ?></td>
				<td><?= h( date("d/m/Y",strtotime($surveyInfo['thaali_date'])))?></td>
				<td><?= h(ucwords($surveyInfo['menu_item'])) ?></td>
				
				<td>
					<?php 
						switch ($surveyInfo['taste']) {
							case '100': echo 'Excellent';break;
							case '75': echo 'Good'; break;
							case '50': echo 'Average';break;
							case '25': echo 'Below Average'; break;
						
						}
					?> 
				</td>
				<td>
					<?php 
						switch ($surveyInfo['qty']) {
							case '75': echo 'More';break;
							case '50': echo 'Less'; break;
							case '100': echo 'Ok'; break;
						
						}
					?> 
				</td>
				<td>
					<?php 
							switch ($surveyInfo['oil']) {
								case '75': echo 'More';break;
								case '50': echo 'Less'; break;
								case '100': echo 'Ok'; break;
							
							}
						?>
				</td>
				 <td><?php echo  round(($surveyInfo['taste']+$surveyInfo['oil']+$surveyInfo['qty'])/3,2).'%' ?></td>
				<td><?= h( date("d/m/Y",strtotime($surveyInfo['created_date'])))?></td>
			</tr>
			<?php 
					endforeach;  
				}
			 ?>
		</tbody>
	</table>
				</div>
			</div><!-- /.col -->
		</div><!-- /.row-->
	</div> <!-- /.box-->
</section><!-- /.content -->
  
<script>

	//Date picker script
	
	$(function() {
		var dates = $("#dtfrom, #dtto")
	   .datepicker({
			maxDate:'0',
			dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) 
			{
				var option = this.id == "dtfrom" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}

		});
	});
  $(document).ready(function() {
		
		var fromDate = $("#dtfrom").val();
		var toDate = $("#dtto").val();
		
		if(fromDate == '' && toDate == ''){
			var currentDate = new Date();  
			$("#dtfrom, #dtto").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", currentDate);
		}
		
		//Date field validation start
		$("#frmexplist").validate({
			rules: {
				dtfrom: {
				// simple rule, converted to {required:true}
					required:true 
				}, 
				dtto: {
					required:true
				}  
			},
			messages: {
				dtfrom:"From date is required.",
				dtto:"To date is required."
			}
		}); 
		
		//ends
		
		// Data tables script for sorting and download the files in the .xls & .pdf

		$('#allUsers').DataTable({
			"bAutoWidth": true , 
			"dom": 'Bfrtip',
			"order": [[0, "desc"]],
			"aoColumnDefs": [
{ "sWidth": "0%",  "aTargets": [0], "bVisible": false,"bSearchable": false, "bSortable": false},
				 {  "aTargets": [1]},
				 {  "aTargets": [2]},
				 {  "aTargets": [3], "bSearchable": true, "bSortable": true},
				 {  "aTargets": [4], "bSearchable": true},
				 {  "aTargets": [5], "bSearchable": true},
				 {  "aTargets": [6], "bSearchable": true},
			],
			"buttons": [
				{extend: 'excelHtml5',filename: '<?php echo "Survey_Report".date("d-m-Y"); ?>',exportOptions:{columns: [1, 2, 3, 4, 5, 6]}},
				
			],
		});
		
	});

	/*
	{extend: 'pdfHtml5',filename: '<?php echo "Survey_Report".date("d-m-Y"); ?>',
				 title:'<?php echo "Survey Report"." ".date("d-m-Y");?> ',
				 exportOptions:{columns: [1, 2, 3, 4, 5, 6]}},
	*/
</script>