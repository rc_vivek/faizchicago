 
		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
                   		<?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userDriverMapping->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userDriverMapping->id)]
            )
        ?>
                   		<a href="users/index" class="btn btn-primary" id="addButton"><i class="fa fa-arrow-circle-o-left tip-bottom hidden-lg fa fa-1x"></i><span class="visible-lg"> Back</span></a>
			    </div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
							  <?= $this->Form->create($userDriverMapping) ?>
							    <fieldset>
							        <legend><?= __('Edit User Driver Mapping') ?></legend>
							        <?php
							            echo $this->Form->input('user_id');
							            echo $this->Form->input('driver_id', ['options' => $users]);
							        ?>
							    </fieldset>
							    <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>		
		</div>
 
 