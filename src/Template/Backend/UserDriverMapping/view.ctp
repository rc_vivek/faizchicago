<div class="box-header">
	<div class="col-md-12 text-right table-upper-row">
	</div>
</div><!-- /.box-header -->
<div class="col-lg-6 col-md-12 col-xs-12">
	<div class="box box-primary">    
		<div class="box-body"> 
           <h3> <?= __('View User Driver Info') ?></h3>
		   <table id="userDriverMapping" class="table table-striped table-bordered" cellspacing="0" width="100%">
				 <tr>
		            <th><?= __('Mapping Id') ?></th>
		            <td><?= $this->Number->format($userDriverMapping->id) ?></td>
		        </tr>		     
		         <tr>
		            <th><?= __('Driver Id') ?></th>
		            <td><?= $userDriverMapping->has('user') ? $this->Html->link($userDriverMapping->user->id, ['controller' => 'Users', 'action' => 'view', $userDriverMapping->user->id]) : '' ?></td>
		        </tr>
		       
		        <tr>
		            <th><?= __('User Id') ?></th>
		            <td><?= $this->Number->format($userDriverMapping->user_id) ?></td>
		        </tr>
		    </table>
		</div>
	</div>
</div>		
 <script>  
     $(document).ready(function() {
   	 $('#userDriverMapping').DataTable({
   	     
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            { width: '20%', targets: 3 }
        ],
        fixedColumns: true
   	 });
   	 
	} );
</script>

 