<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Payment'), ['action' => 'edit', $userPayment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Payment'), ['action' => 'delete', $userPayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userPayment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Payment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Payment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userPayment view large-9 medium-8 columns content">
    <h3><?= h($userPayment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $userPayment->has('user') ? $this->Html->link($userPayment->user->first_name, ['controller' => 'Users', 'action' => 'view', $userPayment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($userPayment->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Amount') ?></th>
            <td><?= $this->Number->format($userPayment->amount) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($userPayment->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($userPayment->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Payment Type') ?></h4>
        <?= $this->Text->autoParagraph(h($userPayment->payment_type)); ?>
    </div>
    <div class="row">
        <h4><?= __('Payment Status') ?></h4>
        <?= $this->Text->autoParagraph(h($userPayment->payment_status)); ?>
    </div>
</div>
