<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Thaali Info'), ['action' => 'edit', $userThaaliInfo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Thaali Info'), ['action' => 'delete', $userThaaliInfo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userThaaliInfo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Thaali Info'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Thaali Info'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userThaaliInfo view large-9 medium-8 columns content">
    <h3><?= h($userThaaliInfo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $userThaaliInfo->has('user') ? $this->Html->link($userThaaliInfo->user->first_name, ['controller' => 'Users', 'action' => 'view', $userThaaliInfo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Active Days') ?></th>
            <td><?= h($userThaaliInfo->active_days) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($userThaaliInfo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($userThaaliInfo->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($userThaaliInfo->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Thaali Size') ?></h4>
        <?= $this->Text->autoParagraph(h($userThaaliInfo->thaali_size)); ?>
    </div>
    <div class="row">
        <h4><?= __('Delivery Method') ?></h4>
        <?= $this->Text->autoParagraph(h($userThaaliInfo->delivery_method)); ?>
    </div>
    <div class="row">
        <h4><?= __('Driver Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($userThaaliInfo->driver_notes)); ?>
    </div>
</div>
