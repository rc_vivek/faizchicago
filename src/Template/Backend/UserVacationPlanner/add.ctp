<?php $this->assign('title', 'Add User Vacation');?>  


		<div class="">
        	<div class="box-header">
            	<div class="col-md-12 text-right table-upper-row">
  						<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'view', $userId]);?>
					</div>
            </div><!-- /.box-header -->
             <div class="row">
			  <div class="col-md-6">
					<div class="box box-primary col-md-offset-1">    
						<div class="box-body">
						<legend><?= __('Add User Vacation') ?></legend>
						<div class="pad-4">
          				 <div class="alert alert-info">
          				  <?php echo "<strong>Info!</strong> Adding / modifing  vacation will be allowed from tomorrow onwards.";?>
          				 </div>
        			</div>
							  <?= $this->Form->create($userVacationPlanner, ['id' => 'vacation', 'name' => 'vacation']) ?> 
							  
							     <?php
								            echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]);
								            echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text']);
											echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text']);
											echo $this->Form->input('title', ['options' => $reason,'class'=> 'form-control', 'label' => 'Reason']);
											echo $this->Form->input('driver_notes',['class'=> 'form-control', 'type' => 'hidden']);
										?>
								  <?= $this->Form->button(__('Submit')) ?>
							    <?= $this->Form->end() ?>
						</div>		
					</div>
				</div>
			</div>
			 <div class="row">
		                 <div class="col-lg-8 col-md-12 col-xs-12">
		                  <h2 class="text-left" style="font-size: 18px; font-weight: 600;">	Vacation List</h2>
		       			</div>
		       		</div>	
			   			<div class="box-body table-responsive">
			                	 <table id="driverReplacement" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>
							            <tr class="skyblue-bg">
							             	<th><?= 'Name' ?></th>
							             	<th><?= 'Start Date' ?></th>
							                <th><?= 'End Date' ?></th>
							                <th>Actions</th>
							            </tr>
							        </thead>
							        <tbody>
							            <?php foreach ($userVacationPlannerList as $userVacationPlanner):?>
							            
							            <tr>
							            	<td><?= h(ucwords($userVacationPlanner->user->full_name)) ?></td>
							                <td><?= h($userVacationPlanner->start_date->format('m/d/Y')) ?></td>
							                <td><?= h($userVacationPlanner->end_date->format('m/d/Y')) ?></td>
							                <td>
							                <?php if(date('Y-m-d') <= $userVacationPlanner->start_date->format('Y-m-d')  ) {  ?>
							                <ul class="action"><li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => 'fa fa-times-circle-o', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "",
			                                                 ['action' => 'delete', $userVacationPlanner->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $userVacationPlanner->id)],
			                                              ['class' => 'btn btn-mini']); ?>
			                               
			                               </li></ul> <?php } ?>  
			                               </td>
							            </tr>
							            <?php endforeach; ?>    
							        </tbody>
						    </table>
			                </div><!-- /.box-body -->		
		</div>
		
		
		
		
  <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
                                        minDate:'+1',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1d",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#vacation").validate({
	     rules: {
	    	 	driver_notes: {required:true},
	         	title: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			title: { 
  			 				required: "Title Name is required."
  						  },
  					 driver_notes: { 
  			 				required: "Driver notes is required." 
  						  }, 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
});
</script>
  