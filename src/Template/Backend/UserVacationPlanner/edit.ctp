<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userVacationPlanner->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userVacationPlanner->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Vacation Planner'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userVacationPlanner form large-9 medium-8 columns content">
    <?= $this->Form->create($userVacationPlanner) ?>
    <fieldset>
        <legend><?= __('Edit User Vacation Planner') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('title');
            echo $this->Form->input('driver_notes');
            echo $this->Form->input('start_date');
            echo $this->Form->input('end_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
