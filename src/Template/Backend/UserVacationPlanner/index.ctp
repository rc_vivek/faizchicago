<?php $this->assign('title', 'ManageUser Vacation');?>  
 <!-- Main content -->
        <section class="content">
          <div class="row">
		  
            <div class="col-xs-12">
              <div class="box">
              	 <div class="box-header">
	                  <div class="row">
	                 <div class="col-lg-8 col-md-12 col-xs-12">
	                  <h2 class="text-left" style="font-size: 18px; font-weight: 600;">Add Vacation period</h2>
	                  <div class="alert alert-info">
          				  <?php echo "<strong>Info!</strong> Adding / modifing  vacation will be allowed from tomorrow onwards.";?>
          				 </div>
        				 
	       			</div>
			        <div class="col-lg-12 col-md-12 col-xs-12">
			         <?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'Users', 'action' => 'view', $userId], [ 'class' => 'pull-right']);?>
			         </div>
			        </div>
			        <div class='row'>
			         <?= $this->Form->create($userVacationPlanner, ['id' => 'vacation', 'name' => 'vacation', 'class' => '']) ?>
							  <?php  echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id, 'class' => 'form-inline']);  
							  		 echo $this->Form->input('driver_notes',['class'=> 'form-control', 'type' => 'hidden']);
							  ?>
							  <div class="form-group col-md-3"> 
							    <label>From Date</label> 
							  	<div class="input-group date">  
							  	 <span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-calendar"></i>
								</span> 
							  	    <?php echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:84%']);?>
							    	<!--span class="add-on" style="padding-left:25px"><?php //echo $this->Html->image('calendar.png'); ?></span-->
							 	</div>
							 	<label id="start-date-error" class="error" for="start-date"></label>
							 </div>   
							 <div class="form-group col-md-3">  
							  <label>To Date</label>  
							  	<div class="input-group date">  
							  	 <span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-calendar"></i>
								</span> 
									<?php echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:84%']); ?>
									<!--span class="add-on" style="padding-left:25px"><?php //echo $this->Html->image('calendar.png'); ?></span-->
							 	</div><label id="end-date-error" class="error" for="end-date"></label>
							 </div> 
							 <div class="form-group col-md-3">  
							  <label>Reason</label> 
							  	<div class="input-group date">
								<?php echo $this->Form->input('title', ['options' => $reason,'class'=> 'form-control', 'label' => false,'style'=>'width:84%']); ?>
									<!--span class="add-on" style="padding-left:25px"><?php //echo $this->Html->image('calendar.png'); ?></span-->
							 	</div>
							 </div>   
							  <div class="form-group col-md-3">  
							  <label>&nbsp;</label>  
							  <button type="submit" class="btn btn-primary">Submit</button>
							</div>	  
			        </div>
	                   
	               
   			 
		              <div class="row">
		                 <div class="col-lg-8 col-md-12 col-xs-12">
		                  <h2 class="text-left" style="font-size: 18px; font-weight: 600;">User Vacation List</h2>
		       			</div>
		       		</div>	
			   			<div class="box-body table-responsive">
			                	 <table id="vacationList" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>
							            <tr class="skyblue-bg">
							             	<th><?= 'Name' ?></th>
							             	<th><?= 'Date' ?></th>
							                <!-- <th><?= 'End Date' ?></th>-->
							                <th>Actions</th>
							            </tr>
							        </thead>
							        
							        <tbody>
							            <?php foreach ($userVacationPlannerList as $userVacationPlanner):?>
							            
							            <tr>
							            	<td><?= h(ucwords($userVacationPlanner->user->full_name)) ?></td>
							                <td><?= h($userVacationPlanner->start_date->format('Y-m-d')) ?></td>
							               <!--  <td><?php //h($userVacationPlanner->end_date->format('m/d/Y')) ?></td> -->
							                <td>
							                <?php if(date('Y-m-d') <= $userVacationPlanner->start_date->format('Y-m-d')  ) {  ?>
							                <ul class="action"><li><?= $this->Form->postLink(
			                                                 $this->Html->tag('i', '', ['class' => '', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Delete',]). "Delete",
			                                                 ['action' => 'delete', $userVacationPlanner->id],
			  												 ['escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $userVacationPlanner->id, $userVacationPlanner->user_id)],
			                                              ['class' => 'btn btn-mini']); ?>
			                               
			                               </li></ul> <?php } ?>  
			                               </td>
							            </tr>
							            <?php endforeach; ?>    
							        </tbody>
						    </table>
			             </div><!-- /.box-header --> 
   			</div> 	
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  

		
  <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
                                        minDate:'+1',
                                  // 	maxDate:'+0d',
                                  dateFormat: 'yy-mm-dd',
					defaultDate: "+1d",
					changeMonth: true,
					numberOfMonths: 1,
					onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#vacation").validate({
	     rules: {
	    	 	//driver_notes: {required:true},
	         	title: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			title: { 
  			 				required: "Title Name is required."
  						  },
  					 /*driver_notes: { 
  			 				required: "Driver notes is required." 
  						  }, */
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 
	 
	  $('#vacationList').DataTable({
		   	 
	   	  "bAutoWidth": true , 
	   	  
          "order": [[ 1, "desc" ]],
          
        });
	       
		 
});
</script>
  