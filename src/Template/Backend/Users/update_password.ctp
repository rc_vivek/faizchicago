<?php $this->assign('title', 'Change Password'); ?>
<div class="">
	<div class="box-header">
		<div class="col-md-12 text-right table-upper-row">
        	<?= $this->Html->link('Back',['prefix' => 'backend', 'controller' => 'dashboard', 'action' => 'index']);?>
		</div>
	</div><!-- /.box-header -->
    <div class="row">
		<div class="col-md-6">
			<div class="box box-primary col-md-offset-1">    
				<div class="box-body">
					<?= $this->Form->create($user, ['id' => 'user', 'name' => 'user']) ?> 
						<legend><?= __('Change Password') ?></legend>
							    <?php
							            echo $this->Form->input('Password', ['type' => 'password', 'id' =>'password', 'name' => 'password', 'class'=> 'form-control']);
							            echo $this->Form->input('Confirm Password', ['type' => 'password', 'id' =>'confirmPassword', 'name' => 'confirmPassword', 'class'=> 'form-control']);
							    ?>
						 <?= $this->Form->button(__('Submit')) ?>
					<?= $this->Form->end() ?>
				</div>		
			</div>
		</div>
	</div>		
</div>

<script>
 $(document).ready(function() {
	  $("#user").validate({
	     rules: {
         password: {
              required:true,
              minlength:5
         }, 
	     confirmPassword: {
              required:true,
              qualTo: "#password"
         }  
     },
        messages: {
           password:"Password is required.",
       	   confirmPassword: 
       	   	{ 
       	   	  required: "Confirm password is required.",
       	   	  qualTo  : "Password & confirm password should be same"
       	   	} 
		}
	}); 
});
</script>