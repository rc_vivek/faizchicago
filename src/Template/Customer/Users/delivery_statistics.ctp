<?php $this->assign('title', 'Delivery Statistics');?> 
<div class="main-content drivers">
	<div class="row">
		<div class="col-md-6 col-xs-12 col-sm-4"><h1>Delivery Statistics</h1></div>
	</div>		
<hr class='m-hr'/>
 
		<div class="row">
		    <div class="col-md-11 col-sm-11 col-xs-11 table-responsive">
		    		 
									<strong>Overall Summary</strong><br /><br />
									<table class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="active">
                                                <th>Status</th>
                                                <th>Today</th>
                                                <th>This Week</th>
                                                <th>This Month</th>
                                                <th>This Year</th>
                                                <th>All Time</th>
                                            </tr>
                                        </thead>
          <tbody>
                                            <tr>
                                                <td>Household Tiffins</td>
                                                <td><?= $todaysDelivery;?> </td>
                                                <td><?= $thisWeekDelivery;?></td>
                                                <td><?= $thisMonthDelivery;?></td>
                                                <td><?= $thisYearDelivery;?></td>
                                                <td><?= $allTimeDelivery;?></td>
                                            </tr>
                                        </tbody>          
         </table>
									
							  		<strong>Monthly Totals for <?= date("Y"); ?></strong><br /><br />
									<table class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="active">          
                                                <th>Status</th>
                                                <?php foreach ($monthsDelivery as $key => $value):  ?>
                                                <th><?= date('M', mktime(0,0,0,$key))  ?></th>                                            
                                                <?php endforeach; ?> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>Household Tiffins</td>
                                            <?php 

                                                foreach ($monthsDelivery as $months):
                                                    echo "<td>".$months."</td>";      
                                                endforeach; 
                                            ?>

                                            </tr>
                                        </tbody>
         </table>
								</div>
							</div>
  </div>
 
	 