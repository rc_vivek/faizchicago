<?php $this->assign('title', 'Edit Profile');?> 
<div class="main-content edit-profile">
	<h1>Edit Profile</h1>
	<!-- <div class="row">
	<div class="col-md-11 col-sm-11 col-xs-11 grey-container">
		<form role="form" class="form-inline">
		
		<div>
		    <label for="name">Please enter Ejamaat ID below if you wish to autopopulate user information</label>  
		  </div>
		    <input type="text" class="form-control" id="id">
		  <button type="submit" class="btn btn-populate">Auto Populate</button>
		
		</form>
	</div>
</div> -->
<div class="row">
	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#userInfo" aria-controls="userInfo" role="tab" data-toggle="tab">Basic Information</a></li>
	    <?php if ($user->user_role != 3 ) { ?>
	    	<li role="presentation"><a href="#thaali" aria-controls="thaali" role="tab" data-toggle="tab">Thali Information</a></li>
	    <?php } ?>	
	  </ul>
	
	  <!-- Tab panes -->
	<?= $this->Form->create($user, ['id' => 'user', 'name' => 'user'])?>
		<?php echo $this->Form->input('id', [ 'class'=> 'form-control']);
	    	  echo $this->Form->input('ejamaatid', [ 'class'=> 'form-control','disabled', 'type' =>'hidden']); 
	    	  echo $this->Form->hidden('password',[ 'class'=> 'form-control']);
	    ?> 
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="userInfo">
		<div class="row">
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php  echo $this->Form->input('first_name', [ 'class'=> 'form-control']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php   echo $this->Form->input('middle_name', [ 'class'=> 'form-control']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php echo $this->Form->input('last_name', [ 'class'=> 'form-control']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php  echo $this->Form->input('email_address', [ 'class'=> 'form-control']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php   echo $this->Form->input('secondary_email_address', [ 'class'=> 'form-control']); ?>
				<small>Seperate email addresses with a ','	</small>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php echo $this->Form->input('home_phone', [ 'class'=> 'form-control']); ?>
					<small>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</small>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']); ?>
				<small>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</small>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12 min-h">
				<?php  echo $this->Form->input('mobile_carrier', ['options' => $mobileCarriers, 'empty' => 'Select Carrier', 'default'=>$mobile_carrier, 'class'=> 'form-control']);?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12 col-sm-6 col-xs-12 min-h">
				<?php  echo $this->Form->input('address', [ 'class'=> 'form-control', 'type' => 'text']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12 min-h">
				<?php   echo $this->Form->input('city', [ 'class'=> 'form-control']); ?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php echo $this->Form->input('user_state', ['options' => $states, 'default'=>$state_id, 'label' => 'States', 'class'=> 'form-control']);?>
			</div>
			<div class="form-group col-md-4 col-sm-6 col-xs-12">
				<?php  echo $this->Form->input('zipcode', [ 'class'=> 'form-control']); ?>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12 col-sm-12 col-xs-12">
				<button type="submit" class="btn btn-custom">SAVE</button>
				<?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'home'], ['class' => 'btn btn-grey']);?>
			</div>
		</div>


	    
	 </div>  
	      <?php if ($user->user_role != 3 ) { 
	      	$activeDays = explode(',', $active_days);
	      	echo $this->Form->hidden('thaali_id', ['default'=> $thaali_id, 'class'=> 'form-control']);
	      ?>
	    <div role="tabpanel" class="tab-pane" id="thaali">
	    <div class="row">
	    <div class="form-group col-md-3  col-sm-6 col-xs-12">
	    <?php echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize, 'default'=> $thaali_size,'class'=> 'form-control']);?>
	     </div>
	      <div class="form-group col-md-3  col-sm-6 col-xs-12">
	    <?php echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,  'default'=>$delivery_method, 'class'=> 'form-control']); ?>
	  </div>
	   <div class="form-group col-md-3  col-sm-6 col-xs-12">
	   <label>&nbsp;</label>
	   <button type="button" class="btn-info btn-small driversArea" data-toggle="modal" data-target="#coverageAreas">Drivers and Coverage Areas</button>
	  </div>
	  <div>
	  
	  <div id="coverageAreas" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Drivers and Coverage Areas</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">   
        <p style="font-size: 11px">
        Please see list of driver name and area that they cover below.If you do not see your area listed then please contact <b>chicagomawaid@gmail.com</b> and you 
		will be directed to the correct driver for your area.
		</br>Kindly contact your respected driver and discuss further details about delivery. Thaali delivery is not coordinated by Faiz al Mawaid al Burhaniyah.
        </p>       
  <table class="table" style="font-size: 11px">
    <thead>
      <tr>
        <th>Driver Name</th>
        <th>Contact No</th>
        <th>Areas</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Anwar Bhai Tapal</td>
        <td>630-518-6377</td>
        <td>Downers Grove, Darien, Lemont, Westmont,</br> Hinsdale, BurRidge</td>
      </tr>
      <tr>
        <td>Farida Bhen Kapadia</td>
        <td>630-962-0492</td>
        <td>Naperville, Aurora</td>
      </tr>
      <tr>
        <td>Abdullah Bhai Ghatilla</td>
        <td>630-312-9078</td>
        <td>Bolingbrook</td>
      </tr>
      <tr>
       <td>Mustansir Bhai Cash</td>
        <td>786-707-0722</td>
        <td>Chicago, Downtown, Buffalo Grove, Long Grove, </br>Roselle, Mount Prostpect</td>
      </tr>
    </tbody>
  </table>
  </div>
      </div>
     
    </div>

  </div>
</div>
	  </div>
	  </div>
	   <div class="row">
	  <?php echo '<div class="drivers  col-md-3" >';
	  		echo "<label style='color: #3A5371;font-size: 1.2em;font-weight: normal'>Driver</label>"; 
			echo $this->Form->input('drivers', ['options'=>$driverlist, 'default'=>$driverId, 'empty'=>'Select Driver','class'=>'form-control','placeholder'=>'Driver', 'label' => false]);
			echo '</div>';	
	  ?>
	 <div class="form-group col-md-3  col-sm-6 col-xs-12">
	    <?php  echo $this->Form->input('driver_notes', ['default'=>$driver_notes,  'class'=> 'form-control', 'label' => 'Delivery Notes']); ?>
	  </div>
	  </div>
	  <div class="row">
	  <?php if ($user->user_role != 3) { ?> 
	  <div class="form-group col-md-3 col-sm-6 col-xs-12">
  		<?php 
  		echo '<div class="delivery-sms-notification">';
  		echo $this->Form->input('delivery_sms_notification', ['default'=>$delivery_sms_notification, 'type'=>'checkbox', 'options'=> 'Yes', 'label' => 'Send Delivery SMS Notification']); 
 	   echo "</div>";
 	   ?>
 	   <?php } else { ?>
 	   <?php echo $this->Form->input('delivery_sms_notification', ['default'=>'0', 'type'=>'hidden']); ?>  
 	   <?php } ?>
 	    </div>   
 	    </div>
	    <div class="row">
		  <div class="form-group col-md-3  col-sm-6 col-xs-12">
		  <?php echo $this->Form->input('active_days', array('type'=>'select', 'multiple'=>'checkbox', 'options'=>$days, 'name' => 'active_days', 'default'=>$activeDays));?>
		  <label id="active_days[]-error" class="error" for="active_days[]"></label>
		  </div>
		  
		  </div>
	<div class="row">
	  <div class="form-group col-md-12  col-sm-12 col-xs-12">
	  <button type="submit" class="btn btn-custom">SAVE</button>
	
	   <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'home'], ['class' => 'btn btn-grey']);?>	
	</div>
	</div>
	    </div>
	    
	    <?php } ?>
	     
	  </div>
	</div>
	</div>
</div>


	
	 <?= $this->Form->end() ?>
  <script>
 $(document).ready(function() {
	  $("#user").validate({
	     rules: {
	         ejamaatid: {
	             required:true,
	             number:true
	         },
         first_name: {
             required:true,
             alpha:true,
             minlength:1
         },
         middle_name: {
       //      required:true,
             alpha:true
         },
         last_name: {
             required:true,
             alpha:true
         }, 
         email_address: {
             required:true,
             email:true
         },
        secondary_email_address: {
        	 multiemail:true
         }, 
          mobile_phone: {
             required:true,
             phoneUS:true
         },
         zipcode: {
             required:true,
             zipcodeUS: true
         }, 
          address: {
              required:true
         },
         
		 "active_days[]" : {required:true, minlength:1},
        /* license_number: {
              required:true
         },
          license_expiry_date: {
              required:true
         },
          vehicle_make: {
              required:true
         },
          
	       vehicle_model: {
              required:true
         },
          vehicle_year: {
              required:true
         },
          insurance_provider: {
              required:true
         },
         insurance_policy_no : {
          required:true
         },
         insurance_expiry_date : {
          required:true
         },
         */
          thaali_size: {
              required:true
         },
          delivery_method: {
              required:true
         },
          driver_notes: {
              required:true
         },
         drivers : {
        	 required: function(element){
                 return $("#delivery-method").val() == 2;
             }
           
     },
	     },
        messages: {
        ejamaatid: { 
  			 required: "Ejamaatid is required.",
  			 number:"Accepts number only."
  		},
  		first_name: { 
  			 required: "First name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		middle_name: { 
  			// required: "Middle name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		last_name: { 
  			 required: "Last name is required.",
  			 alpha:"Accepts alphapets only."
  		},
  		email_address: { 
  			 required: "Email address is required.",
  			 email:"Enter valid email address."
  		},
  		/*secondary_email_address: { 
 			multiemail:"please use a comma to separate multiple email addresses."
 		},*/
  		mobile_phone: { 
  			 required: "Mobile phone is required.",
  			 phoneUS:"Enter a valid Mobile number"
  		},
  		zipcode: { 
			 required: "Zipcode is required.",
			 zipcodeUS:"Enter a valid Zipcode"
		}, 
  		address : { 
  			 required: "Address is required." 
  		},
  		
  		  thaali_size: {
              required: "Thaali size is required." 
         },
          delivery_method: {
               required: "Delivery method is required." 
         },
          driver_notes: {
              required: "Driver notes is required." 
         },
          thaali_size: {
              required: "Thaali size is required." 
         },
         drivers : {
    			required: "Drivers is required." 
    		},
          driver_notes: {
              required: "Driver notes is required." 
         }  ,
		 "active_days[]" : { required: "Choose at least one day.",  minlength:"Choose at least one day."}
         
		}
	}); 
	
	
  
 
	
	if($("#insurance_provider" ).val() != '') {
 		$('#driver-info').css('display', 'block');
	}
	
	$(function() {
		
			var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
									minDate:'-0y',
							  // 	maxDate:'+0d',
							  dateFormat: 'yy-mm-dd',
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onSelect: function( selectedDate ) {
					var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
				
			});
		});
	});

 

var deliveryMethod = $( '#delivery-method' ).val(); 
if (deliveryMethod == 1) {
	$('.drivers').hide();
	$('.driversArea').hide();
	$('.delivery-sms-notification').hide();
 
}

$( '#delivery-method' ).change(function() {
	var deliveryMethod = $( this ).val(); 
	if (deliveryMethod == 1){
		$('.drivers').hide();
		$('.driversArea').hide();
		$('.delivery-sms-notification').hide();
	}
	else { 
		$('.drivers').show();
		$('.driversArea').show();
		$('.delivery-sms-notification').show();
		$('#coverageAreas').modal('show');
	}
		
});
</script>

 
