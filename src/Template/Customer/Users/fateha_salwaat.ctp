<?php $this->assign('title', 'Fateha/Salwaat');?> 
<div class="main-content">
<h1>Fateha/Salwaat</h1>
<hr class='m-hr'/>
<div class="row">
<div class="col-md-5 vacation-container">
 
  										
				<div class="row">							
<?= $this->Form->create('', ['id' => 'fateha', 'name' => 'fateha']) ?>									
	
	    <div class="form-group">
	     <?php
	      echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]); 
	      
	      echo "<div class='input'>";
	      echo $this->Form->label('Items');
	      echo "&nbsp;&nbsp;";
	      echo "<input type='radio' name='items' id='items-0' value = '0' checked = 'checked'> Sweets ";
	      echo "<input type='radio' name='items' id='items-1' value = '0'> Fruits";
	      /*echo $this->Form->radio('items', [
	      		['value' => '0', 'text' => 'Sweets  ', 'class'=> 'fateha',  'checked' => 'checked'],
	      		['value' => '1', 'text' => 'Fruits  ', 'class'=> 'fateha'  ] 
	      		 
	      ]);*/
	      echo '</div>';
	   
		  echo $this->Form->input('fateha_names', ['type' => 'textarea', 'class'=> 'form-control', 'label' => 'Fateha/Salwaat Names']);
		 // echo $this->Form->input('fateha_date', ['class' => 'datepicker form-control', 'type' => 'text', 'label' => 'Fateha/Salwaat date']);
		 ?>
		  
						  <label>Fateha/Salwaat date</label> 
						  	<div class="input-group date">
							  	<span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-calendar"></i>
								</span> 
								<?php echo $this->Form->input('fateha_date', ['class' => 'datepicker form-control', 'type' => 'text', 'label' => false]); ?>
							</div>
							 <label id="fateha-date-error" class="error" for="fateha-date"></label>
						 </div>   
		 <?php 
		  echo $this->Form->input('status', ['class' => 'form-control', 'type' => 'hidden', 'value' => '0']);
        ?>
		</div>
	 
	  <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-custom']);?>
	  <?= $this->Form->end() ?>
	</div>
	
</div>
</div>  
<script>
$(function() {
	
	var dates = $("#fateha-date").datepicker({
	minDate:'+0',
		 
		dateFormat: 'yy-mm-dd',
		 
		changeMonth: true,
		numberOfMonths: 1,
		 onSelect: function( selectedDate ) {
			var option = this.id == "fateha-date" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
		
	});
});
	 $(document).ready(function() {
	  $("#fateha").validate({
	     rules: {
	    	 fateha_names: {required:true},
	    	 fateha_date :  {required:true},
	     },
        messages: {
        	fateha_names: { 
  			 	 required: "Fateha/Salwaat names is required."
  			}, 
	     fateha_date: { 
			 	 required: "Fateha/Salwaat date is required."
			} 
		}
	}); 
});
	
	 
</script>