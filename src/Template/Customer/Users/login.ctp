<?php $this->assign('title', 'Login'); ?>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="login-bg">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 login-logo text-center">
                     <?php echo $this->Html->image('logo-big.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
                    </div>
                  </div>
                  <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-12 col-xs-12 login-content">
                          <div class="inner-container row">

                            <div class="text-center">
                              <div class="login-top">
                                 <?php echo $this->Html->image('mem-icon.png', ['alt' => 'Faizchicago', 'class' => '']); ?>
                                <h4>MEMBER LOGIN</h4>
                                 <?= $this->Flash->render() ?>
                              </div>
                              <?= $this->Form->create('Users', ['id' => 'login', 'name' => 'login', 'class' => 'col-md-12 col-sm-12 col-xs-12 text-center']) ?>
                              <div class="form-group">
                                <input type="text" id="ejamaatid" class="form-control" name="ejamaatid" placeholder="Ejamaat ID" autocomplete="off"/>
                              </div>
                             <input type="password" id="password" class="form-control" name="password"  placeholder="Password" autocomplete="off"/>
                              <?= $this->Form->submit('Login', array('class'=> 'btn btn-orange')); ?>  <br/><br/>
                              <?= $this->Html->link('Forgot your password?',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'forgotPassword']);?><br/>
                              <?= $this->Html->link("REGISTER NOW",['prefix' => 'customer', 'controller' => 'Users', 'action' => 'registration'],['class' => 'btn btn-dashboard nh']);?>
					       	  </form>
                            </div>
                       
                           </div>                     
                    </div>

                    <div class="col-md-offset-1 col-lg-8 col-md-8 col-sm-12 col-xs-12 login-content-right">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="5000">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                          <div class="item active">
                           <?php echo $this->Html->image('slider-1.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                          <div class="item">
                             <?php echo $this->Html->image('slider-2.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                           <div class="item">
                             <?php echo $this->Html->image('slider-3.jpg', ['alt' => 'Faizchicago', 'class' => '']); ?>
                          </div>
                        </div>

                        
                      </div>
                      
<div class='calendar'>
           
          <div class="col-md-12 col-sm-12 col-xs-12 menu-header">
           	<div class='row'>   
            	<div class="col-md-12 col-sm-12Ccol-xs-4 head text-right date">
            		<a title = 'Previous Week' href="javascript:void(0)" onclick="getThaali('<?= $prevWeek;?>')"><</a> &nbsp;&nbsp;
            		<a title = 'Next Week'  href="javascript:void(0)" onclick="getThaali('<?= $nextWeek;?>')">></a> &nbsp;&nbsp; &nbsp;&nbsp;
            	</div>
            </div>   
            <div class='row'>    
           		<div class="col-md-6 col-sm-6 col-xs-4 head ">THAALI MENU</div>
                <div class="col-md-6 col-sm-6 col-xs-8 text-right date"><?= $start. " to ". $end?> &nbsp;&nbsp; &nbsp;&nbsp;</div>
            </div>
  		  </div> 
           <div class="row">
        <?php   $wdates = array();
         		$wdate =  date('Y-m-d', strtotime($start)) ;
         		array_push($wdates,$wdate);
                    for($i=0;$i<4;$i++) {
                    		 $wdate =  date('Y-m-d', strtotime($wdate. ' + 1 days'));
                    		// date_format($wdate,"Y-m-d");
                    		 array_push($wdates,$wdate);
					 }
		?>
           <?php if (count($thisWeekCalendar) > 0) { ?>  <div class="menu-section col-md-12 text-center">
           <?php foreach($wdates as $wdate) { ?>
           
              <?php 
							$i = 0;$status ="0";
							$offset = '';
						    
							//date_format($wdate,"Y-m-d");//echo "<pre>";print_r($thisWeekCalendar);exit;
							foreach ($thisWeekCalendar as $thaali):
							if ($i == 0)
								$offset = "col-md-offset-1";
							else $offset = '';
							
						if($wdate ==$thaali['date'] ) {	$status ="1";
							
			 ?>
                <div class="menu-box col-sm-12">
	                <h4><?= date('l m/d/Y', strtotime($thaali['date']))?></h4>
	  				<div class="<?= ($thaali['type'] == 'Miqat'? 'menu-err' : '')?> menu-name">
		              	<?= ucwords($thaali['title'])?>
		              	<?php if ($thaali['type'] == 'Miqat') { ?>
		              	<?= ucwords($thaali['details'])?>
		              	<?php } ?>
	                </div>
		            <div class="caterer-info">
		              <?php if ($thaali['type'] != 'Miqat') { ?>
		              	<?= ($thaali['type'] != 'Miqat'? 'Caterer:' : '')?>  <?= ucwords($thaali['details'])?>
		              <?php } ?>
		            </div>
                </div>
            	<?php } ?>
            	 
	               
		        <?php 
            	 	$i++; endforeach; 
            	 	if($status == 0){
            	 		?>
            	 		<div class="menu-box col-sm-12">
            	 		<h4><?= date('l m/d/Y', strtotime($wdate))?></h4>
            	 			  				<div class="menu-err menu-name">
            	 				              	 No Thaali
            	 			                </div>
            	 	     </div>		                
            	 	<?php }  	                
            	 	}
				 
		?>

             </div>
               <?php } else { ?>
               <div class="menu-section col-md-12 text-center">
   					<div class="menu-box col-sm-12">
 						<h4>No Thaali scheduled for this week.</h4>
               		</div>
   			</div>
            
               <?php } ?>
                    </div>


      </div>
 
                  </div>

                </div>

</div>

              </div>

            </div>

             </div>
           </div>
       
</div>  
 
<script>
 $(document).ready(function() {
	  $("#login").validate({
	     rules: {
	    	 ejamaatid: {
             // simple rule, converted to {required:true}
              required:true 
         }, 
	     password: {
              required:true
         }  
     },
        messages: {
           ejamaatid:"Ejamaat ID is required.",
       	   password:"Password is required."
		}
	}); 
});

	function getThaali(startDate) {
		 
		 var formUrl = $(this).attr('action'); 
		  
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getThaaliMenu','_full' => true )); ?>',
				data : {"startDate":startDate},
				  
			     success: function(response) {  
			    	 $('.calendar').html(response);
			    	 
		    },
		    
		    error: function (xhr, textStatus, errorThrown)	{
          	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		 
	}
</script>
 