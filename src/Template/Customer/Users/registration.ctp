<?php 
/**
 * User Registration
 */
 ?>
 <?php $this->assign('title', 'Registration');?>
<nav class="navbar navbar-default hidden-lg hidden-md">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
       
      <?php echo $this->Html->link(
             $this->Html->image('logo-1.png', ['alt' => 'Faizchicago', 'class' => 'navbar-brand']),
             ['prefix' => 'customer', 'controller' => 'Users', 'action' => 'home'],
             ['escapeTitle' => false, 'title' => 'Faizchicago']
            );
        ?>     
    </div>
 
  </div><!-- /.container-fluid -->
</nav> 
<div class="main-content user-reg">
<h1>Registration</h1>
<div class="row">
<div class="col-md-8 grey-container">
 <?= $this->Form->create($user, ['id' => 'user', 'name' => 'user',  'class'=> 'row']) ?>

<div class="form-group col-md-6 col-sm-6 col-xs-12">
     <?php  echo $this->Form->input('ejamaatid', ['type' => 'text',  'class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('first_name', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
   <?php echo $this->Form->input('middle_name', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('last_name', [ 'class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
   <?php echo $this->Form->input('email_address', ['type' => 'email', 'class'=> 'form-control', 'label' => 'Email']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">  
    <?php echo $this->Form->input('secondary_email_address', [ 'class'=> 'form-control', 'label' => 'Secondary Email']);
		 echo "<span>Seperate email addresses with a ','	</span>";
	?>
  </div>
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('password', ['type' => 'password','class'=> 'form-control']);?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('confirm_password', ['type' => 'password','class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
   	<?php echo $this->Form->input('home_phone', [ 'class'=> 'form-control']);?>
  	<div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>
   </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('mobile_phone', [ 'class'=> 'form-control']);?>
   <div>Ex. x(xxx)-xxx-xxxx or xxx xxx xxxx or xxx-xxx-xxxx	</div>
  </div>

  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('mobile_carrier', ['options' => $mobileCarriers, 'empty' => 'Select Carrier', 'class'=> 'form-control']);?>
  </div>
 <div class="form-group col-md-6 col-sm-6 col-xs-12">
   <?php echo $this->Form->input('address', [ 'type' => 'text', 'class'=> 'form-control']);?>
  </div>
 <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php  echo $this->Form->input('city', [ 'class'=> 'form-control']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
   <?php 	echo $this->Form->input('user_state', ['options' => $states,'class'=> 'form-control', 'label' => 'State']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('zipcode', [ 'class'=> 'form-control']); ?>
  </div>
   <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('thaali_size', [ 'options' => $thaaliSize,'class'=> 'form-control', 'label' => 'Default Thaali Size']); ?>
  </div>
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <?php echo $this->Form->input('delivery_method', ['options' => $deliveryMethod,'class'=> 'form-control']); ?>
  </div>
  
	
  <div class="form-group col-md-6 col-sm-6 col-xs-12">
   <?php echo $this->Form->input('driver_notes', ['class'=> 'form-control', 'label' => 'Delivery Notes']); ?>
  </div>

 
		<div class="form-group col-md-6 col-sm-6 col-xs-12 drivers"> 
		  <?php echo '<div class="">';
			  		echo "<label style='color: #3A5371;font-size: 1.2em;font-weight: normal'>Driver</label>"; 
					echo $this->Form->input('drivers', ['options'=>$driverlist, 'empty'=>'Select Driver','class'=>'form-control','placeholder'=>'Driver', 'label' => false]);
					echo '</div>';	
		  ?>
		</div> 
		  <div class="form-group col-md-6 col-sm-6 col-xs-12 driversArea1">
		  <div><label>&nbsp;</label>  <label>&nbsp;</label></div>
		  <div><button type="button" class="btn-info btn-small driversArea" data-toggle="modal" data-target="#coverageAreas">Drivers and Coverage Areas</button></div>
		 </div>
		 <div class="form-group col-md-6 col-sm-6 col-xs-12">
		  <?php echo $this->Form->input('active_days', array('type'=>'select', 'multiple'=>'checkbox', 'options'=>$days, 'name' => 'active_days'));?>
		  <label id="active_days[]-error" class="error" for="active_days[]"></label>
		 </div> 
		 <div class="form-group col-md-6 col-sm-6 col-xs-12 delivery-sms-notification">
		  <?php echo $this->Form->input('delivery_sms_notification', ['type'=>'checkbox', 'options'=> 'Yes', 'label' => 'Send Delivery SMS Notification']); ?>
		 </div>
		   <div class="form-group col-md-6 col-sm-6 col-xs-12">
		  <?php echo $this->Form->input('ispwdupdated', array('type'=>'hidden', 'value' => '1'));?>
		   </div>
		  <div class="form-group col-md-12 col-sm-12 col-xs-12">
		  <button type="submit" class="btn btn-custom">SUBMIT</button>
		   <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'login'], ['class' => 'btn btn-grey']);?>	  
		</div>

<?= $this->Form->end() ?>
</div>
</div>
</div>
<div>
	  
	  <div id="coverageAreas" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Drivers and Coverage Areas</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">   
        <p style="font-size: 11px">
        Please see list of driver name and area that they cover below.If you do not see your area listed then please contact <b>chicagomawaid@gmail.com</b> and you 
		will be directed to the correct driver for your area.
		</br>Kindly contact your respected driver and discuss further details about delivery. Thaali delivery is not coordinated by Faiz al Mawaid al Burhaniyah.
        </p>       
  <table class="table" style="font-size: 11px">
    <thead>
      <tr>
        <th>Driver Name</th>
        <th>Contact No</th>
        <th>Areas</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Anwar Bhai Tapal</td>
        <td>630-518-6377</td>
        <td>Downers Grove, Darien, Lemont, Westmont,</br> Hinsdale, BurRidge</td>
      </tr>
      <tr>
        <td>Farida Bhen Kapadia</td>
        <td>630-962-0492</td>
        <td>Naperville, Aurora</td>
      </tr>
      <tr>
        <td>Abdullah Bhai Ghatilla</td>
        <td>630-312-9078</td>
        <td>Bolingbrook</td>
      </tr>
      <tr>
       <td>Mustansir Bhai Cash</td>
        <td>786-707-0722</td>
        <td>Chicago, Downtown, Buffalo Grove, Long Grove, </br>Roselle, Mount Prostpect</td>
      </tr>
    </tbody>
  </table>
  </div>
      </div>
     
    </div>

  </div>
</div>
	  </div>
 
		
 <script>
	 $(document).ready(function() {
		  $("#user").validate({
			 rules: {
				 ejamaatid: {
					 required:true,
					 number:true
				 },
				  
			 first_name: {
				 required:true,
				 alpha:true
			 },
			middle_name: {
			//	 required:true,
				 alpha:true
			 },
			 last_name: {
				 required:true,
				 alpha:true
			 }, 
			 email_address: {
				 required:true,
				 email:true
			 },
			  secondary_email_address: {
	        	 multiemail:true
	         }, 
			 home_phone: {
	             required:true,
	             phoneUS: true
	         }, 
	          mobile_phone: {
	             required:true,
	             phoneUS: true
	         }, 
	         mobile_carrier: {
	             required:true             
	         },
			 zipcode: {
				  required:true,
				  zipcodeUS:true
			 },
			 password: {
				  required:true,
				  minlength:5
			 },
			 confirm_password: {
				  required:true,
				  equalTo: "#password"
			 },
			  address: {
				  required:true
			 },
			 thaali_size: {
				  required:true
			 },
			  delivery_method: {
				  required:true
			 },
			 
	         drivers : {
	        	 required: function(element){
	                 return $("#delivery-method").val() == 2;
	             }
	           
	     },
			/*  driver_notes: {
				  required:true
			 } ,*/
			 "active_days[]" : {required:true, minlength:1}
		 },
			messages: {
				ejamaatid: { 
					 required: "Ejamaatid is required.",
					 number:"Accepts number only."
				},
				first_name: { 
					 required: "First name is required.",
					 alpha:"Accepts alphapets only."
				},
				/*middle_name: { 
					 required: "Middle name is required.",
					 alpha:"Accepts alphapets only."
				},*/
				last_name: { 
					 required: "Last name is required.",
					 alpha:"Accepts alphapets only."
				},
				email_address: { 
					 required: "Email address is required.",
					 email:"Enter valid email address."
				},
				/* secondary_email_address: { 
		 			multiemail:"Please use a comma to separate multiple email addresses."
		 		}, */
				home_phone: { 
		 			 required: "Home phone is required.",
		 			 phoneUS:"Enter a valid Home number"
		 		},
		  		mobile_phone: { 
		  			 required: "Mobile phone is required.",
		  			 phoneUS:"Enter a valid Mobile number"
		  		},
		  		mobile_carrier: {
		             required:"Mobile carrier is required."
		         },
				zipcode: { 
					 required: "Zipcode is required.",
					 zipcodeUS:"Enter a valid Zipcode"
				}, 
				password: { 
					 required: "Password is required."
				},
				confirm_password: { 
					 required: "Confirm Password is required."			 
				}, 		
				address : { 
					 required: "Address is required." 
				}, 
				thaali_size: {
					  required: "Thaali size is required." 
				 },
				  delivery_method: {
					   required: "Delivery method is required." 
				 },
				/*  driver_notes: {
					  required: "Driver notes is required." 
				 },*/
				  thaali_size: {
					  required: "Thaali size is required." 
				 },
				 drivers : {
		    			required: "Drivers is required." 
		    		},
				 "active_days[]" : { required: "Choose at least one day.",  minlength:"Choose at least one day."}
			}
		}); 
	});

	$( "#show-driver-info" ).click(function() {
		$('#driver-info').css('display', 'block');
	 });
	$(function() {

		var dates = $("#insurance-expiry-date, #license-expiry-date").datepicker({
								minDate:'-0y',
						  // 	maxDate:'+0d',
						  dateFormat: 'yy-mm-dd',
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				var option = this.id == "insurance_expiry_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
			
		});
	});

	var deliveryMethod = $( '#delivery-method' ).val(); 
	if (deliveryMethod == 1) {
		$('.drivers').hide();
		$('.driversArea').hide();
		$('.driversArea1').hide();
		$('.delivery-sms-notification').hide();
	 
	}
	
	$( '#delivery-method' ).change(function() {
		var deliveryMethod = $( this ).val(); 
		if (deliveryMethod == 1){
			$('.drivers').hide();
			$('.driversArea').hide();
			$('.driversArea1').hide();
			$('.delivery-sms-notification').hide();
		}
		else { 
			$('.drivers').show();
			$('.driversArea').show();
			$('.driversArea1').show();
			$('.delivery-sms-notification').show();
			$('#coverageAreas').modal('show');
		}
			
	});

	
</script>
 
 
<style>
#driver-info {display:none}
</style> 

 
