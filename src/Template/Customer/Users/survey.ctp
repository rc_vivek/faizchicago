<?php
//Survey for Thali
//ALTER TABLE `thaali` ADD `survey_id` INT NOT NULL DEFAULT '0' AFTER `modified`, ADD `review_status` INT NOT NULL DEFAULT '0' AFTER `survey_id`;


/* CREATE TABLE IF NOT EXISTS `thaali_survey` ( `id` int(11) NOT NULL AUTO_INCREMENT, 
`user_id` int(11) NOT NULL, `thaali_id` int(11) NOT NULL, `thaali_taste` enum('0','1') NOT NULL COMMENT '0-No,1-Yes', 
`thaali_qty` enum('1','2','3') NOT NULL COMMENT '1-More,2-Less,3-Ok', `rating` int(11) NOT NULL, `created` datetime NOT NULL, 
`modified` datetime NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1  */

?>

	<div class="main-content menu">
	  <div class="msg"></div>
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-4"><h1>Survey for Thaali</h1></div>
				<div class="text-right col-md-6 col-xs-12 col-sm-8">
			 		<span class="text-green">Your Default Size is <?= $thaali_size?></span> 
				</div>
		 </div>
		<hr class='m-hr'/>
		<div class="row">
			<div class="col-md-12">
				<div id="events-calendar" class="box jplist">
					<div id="review_calendar_survey"></div>
				</div>  
			</div>
		</div>
		<br/>
	
	<div id="review_dialog" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">      
				<div class="modal-body">
					<h3>Add Your Review</h3>
					<hr/>
					<?php //= $this->Form->create('', ['id' => 'survey', 'name' => 'survey', 'url' => ['controller' => 'Users', 'action' => 'surveySubmit']]); ?>
					<form method="POST" id ="survey" name="survey">
                        <input type="hidden" name="thaali_id" id="thaali_id"/>				
						<div class="form-group">
							<label>How is the taste of the food?</label> <br />
							<label class="radio-inline"><input name='taste' type="radio" value=100 />Excellent</label>
							<label class="radio-inline"><input name='taste' type="radio" value=75 />Good</label> 
							<label class="radio-inline"><input name='taste' type="radio" value=50 />Average</label> 
							<label class="radio-inline"><input name='taste' type="radio" value=25/>Below Average</label>
							<br />
							<div class="error_message_holder"></div>							
							<hr/>
						</div>
						<div class="form-group">
							<label>Are the spices?</label> <br />
							<label class="radio-inline"><input name='happy' type="radio" value=50 />Less</label>
							<label class="radio-inline"><input name='happy' type="radio" value=75 />More</label>
							<label class="radio-inline"><input name='happy' type="radio" value=100 />Ok</label> 
							<br />
							<div class="error_message_holder"></div>
							<hr/>
						</div>
						<div class="form-group">
							<label>Is the oil?</label> <br />
							<label class="radio-inline"><input name='is_oil' type="radio" value=50 />Less</label>
							<label class="radio-inline"><input name='is_oil' type="radio" value=75 />More</label>
							<label class="radio-inline"><input name='is_oil' type="radio" value=100 />Ok</label>
							<br />
							<div class="error_message_holder"></div>							
							<hr/>
						</div>
						<!--div class="form-group">
							<label>Your overall rating for this thaali</label><br />
							<input type="hidden" name="rating_value" class="rating" data-filled="fa fa-circle" data-empty="fa fa-circle-o "/>
						</div-->
						<div class="modal-footer">
							<button type="button" id="survey_submit" class="btn btn-custom">SUBMIT</button>
							<button type="button" id="survey_cancle" class="btn btn-grey" data-dismiss="modal">CANCEL</button>
						</div>
				 </form> 
				</div>
			</div>
		</div>
	</div>
	</div>
 <script>
 if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	 var cView = 'listWeek';
 }
 else 
	 var cView = 'month';
 	 
	$(document).ready(function() {
		if($('#review_calendar_survey').length) {
			var calendar = $('#review_calendar_survey').fullCalendar({
				header: {
				left: 'title',
				center: '',
				right:'prev,next'
				},
				defaultView: cView,
				displayEventTime: false,
				defaultDate: '<?= date("Y-m-d")?>',
				//editable: true,
				 disableDragging: true,
				/*columnFormat: {
					  month: 'dddd',  
					  day: 'MMM dd' 
					  },*/
			   eventRender: function (event, element)
				{
					element.find('.fc-title').html(element.find('.fc-title').text());
					element.find('.fc-list-item-title').html(element.find('.fc-list-item-title').text());
				},
				eventLimit: true,
				events: {
					url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'getSurveyinfo','_full' => true )); ?>',
					type: 'POST', // Send post data
					error: function() {
						alert('There was an error while fetching events.');
					}
				}
			});
		}
     
	$("#survey").validate({
		framework: 'bootstrap',
        excluded: ':disabled',
        rules: {
			 happy: {
				 // simple rule, converted to {required:true}
				  required:true 
			 }, 
			 taste: {
				  required:true
			 },
			 is_oil: {
				  required:true
			 }
        },
        messages: {
            is_oil:"Please Select Option",
			happy: "Please Select Option",
			taste: "Please Select Option"

        },
		errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertBefore(element.closest('.form-group').find('.error_message_holder'));
            } else {
                error.insertAfter(element);
            }
        }
    });

	
	$("#survey_submit").on('click',function( event ) {
		 // event.preventDefault();
		 if($("#survey").valid()){
		  var formUrl = $(this).attr('action'); 
		  var post_data = $("#survey").serialize();  
		  $.ajax({
			    type : 'POST',
			    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'surveySubmit','_full' => true )); ?>',
				data : post_data,
			    success: function(response) { 
                    $('#review_dialog').modal('hide');				
			    	$(".msg").html(response);
			    	$('#review_calendar_survey').fullCalendar( 'refetchEvents' );
			    	$("html, body").animate({ scrollTop: 0 }, "slow");
			    	$(".alert").delay(15000).fadeOut('slow');

		    },
		    error: function (xhr, textStatus, errorThrown)	{
             	alert("Error: " + (errorThrown ? errorThrown : xhr.status));}   
			});
		 }else{
			$('#review_dialog').modal('show');
		 }
		});
	 });	
	function thaaliReview(id) {
		$("#thaali_id").val();
		$("#thaali_id").val(id);
	}

 </script>