<?php $this->assign('title', 'Vacation Planner');?> 
<div class="main-content dashboard">
 <div class="msg"></div>
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-4"><h1>Vacation Planner</h1>
			<p class="error">* Adding/modifying vacation information will be considered valid only if the event is scheduled from <?= $min?>.</p>
			</div>
			<div class="text-right col-md-6 col-xs-12 col-sm-8"></div>
		 </div>

<hr class='m-hr'/>
<div class="row">

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="row">
			<div class="col-md-8 info-text"><h3>Add Vacation</h3></div>
			<div class="col-md-4 text-right"></div>
		</div> 
		<div class="row">
		 	<div class="col-md-12 col-sm-12 col-xs-12">
				 <?= $this->Form->create($userVacationPlanner, ['id' => 'vacation', 'name' => 'vacation']) ?> 											
					<div class="row">
					    <?= $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]);?>
						 <div class="col-md-4 form-group "> 
						    <label>From Date</label> 
						  	<div class="input-group date">  
							  	<span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-calendar"></i>
								</span> 
						  	    <?php echo $this->Form->input('start_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:83%']);?>
						    </div>
						   <label id="start-date-error" class="error" for="start-date"></label>
						 </div>   
						 <div class="col-md-4 form-group">  
						  <label>To Date</label> 
						  	<div class="input-group date">
							  	<span class="input-group-addon" id="basic-addon1">
									<i class="fa fa-calendar"></i>
								</span> 
								<?php echo $this->Form->input('end_date', ['class'=>'datepicker form-control','type'=> 'text', 'label' => false,'style'=>'width:83%']); ?>
							</div>
							 <label id="end-date-error" class="error" for="end-date"></label>
						 </div>   
						<div class="col-md-7 form-group"> 
						 <?php echo $this->Form->input('title', ['options' => $reason,'class'=> 'form-control', 'label' => 'Reason']); ?>
						 </div>
						<div class="col-md-12 form-group"> 
						 <?php echo $this->Form->input('driver_notes',['class'=> 'form-control', 'type' => 'hidden']); ?>
						 </div>
					</div>
					  <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-custom']);?>
					  <?= $this->Html->link('Cancel',['prefix' => 'customer', 'controller' => 'Users', 'action' => 'home'], ['class' => 'btn btn-grey']);?>	  
					  <?= $this->Form->end() ?>
					  <br/>
				</div>
	  		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12 ">
		<div class="row">
			<div class="col-md-8 info-text"><h3>Upcoming Vacations</h3></div>
			<div class="col-md-4 text-right"></div>
		</div> 
		<div class="row">
		 	<div class="col-md-12 col-sm-12 col-xs-12">
	 			<table id="vacationList" class="table table-striped table-bordered" cellspacing="0">
					 <thead>
						<tr class="skyblue-bg">
						<th><?= 'Date' ?></th>
						<!-- <th><?= 'End Date' ?></th>-->
						<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($userVacationPlannerList as $userVacationPlanner):?>
							<tr id="<?= $userVacationPlanner->id;?>">
								<td><?= h($userVacationPlanner->start_date->format('Y-m-d')) ?></td>
								     <!--  <td><?php //h($userVacationPlanner->end_date->format('m/d/Y')) ?></td> -->
								      <td>
 <?php if(date('Y-m-d') <= $userVacationPlanner->start_date->format('Y-m-d') &&  $userVacationPlanner->start_date->format('Y-m-d') >= date('Y-m-d', strtotime($min))) {  ?>
								         <?php echo "<a href='javascript:void(0)' class='btn btn-primary' onclick ='deleteVacation(". $userVacationPlanner->id.")' >Delete</a>";?>
				                         <?php } else { ?>
				                         <button type="button" class="btn btn-info disabled">Delete</button> 
				                         <?php } ?>  
				                       </td>
							</tr>
						<?php endforeach; ?>    
					</tbody>
				</table>
		 	</div>
		</div> 
	</div>
</div>

</div>
 <script>
  
  $(function() {
			
				var dates = $("#start-date, #end-date").datepicker({
				minDate:'+7',
					//maxDate:'+0d',
					dateFormat: 'yy-mm-dd',
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 1,
					 onSelect: function( selectedDate ) {
						var option = this.id == "start-date" ? "minDate" : "maxDate",
							instance = $( this ).data( "datepicker" ),
							date = $.datepicker.parseDate(
								instance.settings.dateFormat ||
								$.datepicker._defaults.dateFormat,
								selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
					
				});
			});
 
 
 
	 $(document).ready(function() {
	  $("#vacation").validate({
	     rules: {
	    	 	/*driver_notes: {required:true},*/
	         	title: {required:true},
         		start_date: {required:true,date: true},
         		end_date: {required:true, date:true}
        },
        messages: {
        			title: { 
  			 				required: "Title Name is required."
  						  },
  					 
  					start_date : { 
  			 				required: "Start date is required."  
  						  }, 
  					end_date : { 
  			 				required: "End date is required." 
  						  } 
		}
	}); 

		
	  $('#vacationList').DataTable({
		   	 
	   	  "bAutoWidth": true , 
	   	  "lengthChange": false,
	   	  'iDisplayLength': 5,
	   	  "pagingType": "simple",
	   	  "bFilter": false,
          "order": [[ 0, "desc" ]],
          columnDefs: [
   	   	            { "sWidth": "10%",  "aTargets": [0],  "bSearchable": false, "bSortable": false},
   	   	            { "sWidth": "10%", "aTargets": [1],  "bSearchable": true, "bSortable": false },
   	   	   ],
           
        });
});

	 
		function deleteVacation(id) {
			bootbox.confirm("Are you sure to cancel this vacation?", function(result)  {
				if (result) {
				  var table = $('#vacationList').DataTable();
				  $.ajax({
					    type : 'POST',
					    url : '<?php echo $this->Url->build(array('controller'=>'users','action'=>'deleteVacation','_full' => true )); ?>',
						data : {"id":id},
					     success: function(response) {  
					    	 $("#"+id).remove();
					    	  bootbox.alert('Vacation has been deleted successfully.');
					    	// $("html, body").animate({ scrollTop: 0 }, "slow");
					    	// $(".msg").html(response).fadeIn('slow');
					    	
					    	// $(".msg").delay(15000).fadeOut('slow');
					    	
					    	 //table.row('.selected').remove().draw( false );
				    },
				    error: function (xhr, textStatus, errorThrown)	{
		           //alert("Error: " + (errorThrown ? errorThrown : xhr.status));
				    	 bootbox.alert('Vacation planner could not be deleted. Please, try again.');
				           }   
				    	 
					});
				}
			});			
	 }
</script>