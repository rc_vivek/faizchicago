
<?php
 $session = $this->request->session();
 $user_data = $session->read('Auth.User');
 if($loggedIn && !empty($user_data)) { ?>
	<div class="navbar-collapse collapse main-nav ">
		  <div class="container">
		    <ul class="nav navbar-nav pull-right">
		       <!--    <li><?= $this->Html->link('My Account', ['prefix' => 'customer','controller' => 'users', 'action' => 'view',$user_data['id']]);?></li>
		           <li><?= $this->Html->link('LogOut', ['prefix' => 'customer','controller' => 'users', 'action' => 'logout']);?></li> -->
		    </ul>
		  </div>
	</div>
<?php } else { ?>
	<div class="navbar-collapse collapse main-nav">
		  <div class="container">
		    <ul class="nav navbar-nav">
		           <li><?= $this->Html->link('Login', ['prefix' => 'customer','controller' => 'users', 'action' => 'login']);?></li>
		           <li><?= $this->Html->link('Sign Up', ['prefix' => 'customer','controller' => 'users', 'action' => 'registration']);?></li>
		    </ul>
		  </div>
	</div>
<?php } ?> 