<?php
namespace App\Test\TestCase\Controller\Backend;

use App\Controller\Backend\UserDistributionMappingController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Backend\UserDistributionMappingController Test Case
 */
class UserDistributionMappingControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_distribution_mapping',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.user_driver_mapping',
        'app.role',
        'app.distribution_center'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
