<?php
namespace App\Test\TestCase\Controller\Backend;

use App\Controller\Backend\UserVacationPlannerController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Backend\UserVacationPlannerController Test Case
 */
class UserVacationPlannerControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_vacation_planner',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
