<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributionCenterTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributionCenterTable Test Case
 */
class DistributionCenterTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributionCenterTable
     */
    public $DistributionCenter;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distribution_center'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributionCenter') ? [] : ['className' => 'App\Model\Table\DistributionCenterTable'];
        $this->DistributionCenter = TableRegistry::get('DistributionCenter', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributionCenter);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
