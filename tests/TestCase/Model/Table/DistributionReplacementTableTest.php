<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributionReplacementTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributionReplacementTable Test Case
 */
class DistributionReplacementTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributionReplacementTable
     */
    public $DistributionReplacement;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distribution_replacement',
        'app.distribution_center'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributionReplacement') ? [] : ['className' => 'App\Model\Table\DistributionReplacementTable'];
        $this->DistributionReplacement = TableRegistry::get('DistributionReplacement', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributionReplacement);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
