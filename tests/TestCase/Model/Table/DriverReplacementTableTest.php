<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DriverReplacementTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DriverReplacementTable Test Case
 */
class DriverReplacementTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DriverReplacementTable
     */
    public $DriverReplacement;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.driver_replacement',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.user_distribution_mapping',
        'app.distribution_center',
        'app.user_driver_mapping',
        'app.role'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DriverReplacement') ? [] : ['className' => 'App\Model\Table\DriverReplacementTable'];
        $this->DriverReplacement = TableRegistry::get('DriverReplacement', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DriverReplacement);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
