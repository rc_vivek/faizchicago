<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FatehaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FatehaTable Test Case
 */
class FatehaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FatehaTable
     */
    public $Fateha;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fateha',
        'app.users',
        'app.contact_feeds',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_payment',
        'app.user_thaali_info',
        'app.user_vacation_planner'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Fateha') ? [] : ['className' => 'App\Model\Table\FatehaTable'];
        $this->Fateha = TableRegistry::get('Fateha', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fateha);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
