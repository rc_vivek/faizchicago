<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ThaaliDeliveryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ThaaliDeliveryTable Test Case
 */
class ThaaliDeliveryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ThaaliDeliveryTable
     */
    public $ThaaliDelivery;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.thaali_delivery',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.user_distribution_mapping',
        'app.distribution_center',
        'app.user_driver_mapping',
        'app.user_thaali_info',
        'app.user_vacation_planner',
        'app.thaali',
        'app.caterer'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ThaaliDelivery') ? [] : ['className' => 'App\Model\Table\ThaaliDeliveryTable'];
        $this->ThaaliDelivery = TableRegistry::get('ThaaliDelivery', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ThaaliDelivery);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
