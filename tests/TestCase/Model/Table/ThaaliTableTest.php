<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ThaaliTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ThaaliTable Test Case
 */
class ThaaliTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ThaaliTable
     */
    public $Thaali;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.thaali',
        'app.caterer'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Thaali') ? [] : ['className' => 'App\Model\Table\ThaaliTable'];
        $this->Thaali = TableRegistry::get('Thaali', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Thaali);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
