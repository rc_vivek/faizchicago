<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserDistributionMappingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserDistributionMappingTable Test Case
 */
class UserDistributionMappingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserDistributionMappingTable
     */
    public $UserDistributionMapping;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_distribution_mapping',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.user_driver_mapping',
        'app.role',
        'app.distribution_center'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserDistributionMapping') ? [] : ['className' => 'App\Model\Table\UserDistributionMappingTable'];
        $this->UserDistributionMapping = TableRegistry::get('UserDistributionMapping', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserDistributionMapping);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
