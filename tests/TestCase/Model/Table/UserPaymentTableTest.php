<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserPaymentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserPaymentTable Test Case
 */
class UserPaymentTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\UserPaymentTable     */
    public $UserPayment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_payment',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info',
        'app.user_vacation_planner',
        'app.role',
        'app.states'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserPayment') ? [] : ['className' => 'App\Model\Table\UserPaymentTable'];        $this->UserPayment = TableRegistry::get('UserPayment', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserPayment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
