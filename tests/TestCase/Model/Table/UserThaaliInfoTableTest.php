<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserThaaliInfoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserThaaliInfoTable Test Case
 */
class UserThaaliInfoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserThaaliInfoTable
     */
    public $UserThaaliInfo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_thaali_info',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.user_distribution_mapping',
        'app.distribution_center',
        'app.user_driver_mapping',
        'app.role'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserThaaliInfo') ? [] : ['className' => 'App\Model\Table\UserThaaliInfoTable'];
        $this->UserThaaliInfo = TableRegistry::get('UserThaaliInfo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserThaaliInfo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
