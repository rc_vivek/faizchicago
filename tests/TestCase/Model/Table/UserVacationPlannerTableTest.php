<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserVacationPlannerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserVacationPlannerTable Test Case
 */
class UserVacationPlannerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserVacationPlannerTable
     */
    public $UserVacationPlanner;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_vacation_planner',
        'app.users',
        'app.driver_info',
        'app.login_history',
        'app.thaali_delivery',
        'app.thaali',
        'app.caterer',
        'app.distribution_center',
        'app.user_distribution_mapping',
        'app.user_driver_mapping',
        'app.user_thaali_info'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserVacationPlanner') ? [] : ['className' => 'App\Model\Table\UserVacationPlannerTable'];
        $this->UserVacationPlanner = TableRegistry::get('UserVacationPlanner', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserVacationPlanner);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
